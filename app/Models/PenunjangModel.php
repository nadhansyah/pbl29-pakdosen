<?php

namespace App\Models;

use CodeIgniter\Model;

class PenunjangModel extends Model
{
    protected $db;
    public function __construct()
    {
        $this->db = \Config\Database::connect();
    }

    function get_data()
    {
        $data = $this->db->query('select * from penunjang where nik_dosen = ');
        return $data->getResultArray();
    }

    function insertdata($data)
    {
        return $this->db->table('penunjang')->insert($data);
    }

    function get_databyid($id)
    {
        $data = $this->db->query("select * from penunjang where id_penunjang = '" . $id . "'");
        return $data->getRow();
    }

    function updatedata($id, $data)
    {
        return $this->db->table('penunjang')->update($data, array('id_penunjang' => $id));
    }

    function hapusdata($id)
    {
        return $this->db->table('penunjang')->delete(array('id_penunjang' => $id));
    }
}
