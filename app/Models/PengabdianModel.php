<?php

namespace App\Models;

use CodeIgniter\Model;

class PengabdianModel extends Model
{
    protected $db;
    public function __construct()
    {
        $this->db = \Config\Database::connect();
    }

    function get_data()
    {
        $data = $this->db->query('select * from pengabdian where id_usulan = ');
        return $data->getResultArray();
    }

    function insertdata($data)
    {
        return $this->db->table('pengabdian')->insert($data);
    }

    function get_databyid($id)
    {
        $data = $this->db->query("select * from pengabdian where id_pengabdian = '" . $id . "'");
        return $data->getRow();
    }

    function updatedata($id, $data)
    {
        return $this->db->table('pengabdian')->update($data, array('id_pengabdian' => $id));
    }

    function hapusdata($id)
    {
        return $this->db->table('pengabdian')->delete(array('id_pengabdian' => $id));
    }
}
