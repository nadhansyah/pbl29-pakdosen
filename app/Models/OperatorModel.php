<?php

namespace App\Models;

use CodeIgniter\Model;

class OperatorModel extends Model
{
    protected $db;
    public function __construct()
    {
        $this->db = \Config\Database::connect();
    }

    function get_data()
    {
        $data = $this->db->query('select * from operator');
        return $data->getResultArray();
    }

    function insertdata($data)
    {
        return $this->db->table('operator')->insert($data);
    }

    function get_databyid($id)
    {
        $data = $this->db->query("select * from operator where nik_operator = '" . $id . "'");
        return $data->getRow();
    }

    function updatedata($id, $data)
    {
        return $this->db->table('operator')->update($data, array('nik_operator' => $id));
    }

    function hapusdata($id)
    {
        return $this->db->table('operator')->delete(array('nik_operator' => $id));
    }

    // Profile
    function get_profile()
    {
        $data = $this->db->query('select * from operator');
        return $data->getRow();
    }

    function updateprofile($id, $data)
    {
        return $this->db->table('operator')->update($data, array('nik_operator' => $id));
    }

    // login
    function loginprocessopt($nik_operator, $password)
    {
        return $this->db->query('select nik_operator, nama from operator where nik_operator="' . $nik_operator . '" and password="' . $password . '"')->getRow();
    }

    //change pass
    function get_passopt($nik)
    {
        $data = $this->db->query("SELECT nik_operator, password FROM operator WHERE nik_operator ='" . $nik . "'");
        return $data->getRow();
    }

    function passprocess($nik_operator, $password)
    {
        return $this->db->query('select nik_operator, nama from operator where nik_operator="' . $nik_operator . '" and password="' . $password . '"')->getRow();
    }

    function updatepass($nik, $data)
    {
        $data = $this->db->query('UPDATE operator SET password ="' . $data . '" WHERE nik_operator = "' . $nik . '"');
        return $data;
    }
}
