<?php

namespace App\Models;

use CodeIgniter\Model;

class VerifpelpendidikanModel extends Model
{
    protected $db;
    public function __construct()
    {
        $this->db = \Config\Database::connect();
    }

    function get_data()
    {
        $data = $this->db->query('select * from pel_pendidikan where nik_dosen = ');
        return $data->getResultArray();
    }

    function get_databyid($id)
    {
        $data = $this->db->query("select * from pel_pendidikan_reviewer where id_pel_pendidikan = '" . $id . "'");
        return $data->getRow();
    }

    function updatedata($id, $data)
    {
        return $this->db->table('pel_pendidikan_reviewer')->update($data, array('id_pel_pendidikan' => $id));
    }
    function insertdata($data)
    {
        return $this->db->table('pel_pendidikan_reviewer')->insert($data);
    }
}
