<?php

namespace App\Models;

use CodeIgniter\Model;

class PenelitianModel extends Model
{
    protected $db;
    public function __construct()
    {
        $this->db = \Config\Database::connect();
    }

    function get_data()
    {
        $data = $this->db->query('select * from penelitian where nik_dosen = ');
        return $data->getResultArray();
    }

    function insertdata($data)
    {
        return $this->db->table('penelitian')->insert($data);
    }

    function get_databyid($id)
    {
        $data = $this->db->query("select * from penelitian where id_penelitian = '" . $id . "'");
        return $data->getRow();
    }

    function updatedata($id, $data)
    {
        return $this->db->table('penelitian')->update($data, array('id_penelitian' => $id));
    }

    function hapusdata($id)
    {
        return $this->db->table('penelitian')->delete(array('id_penelitian' => $id));
    }
}
