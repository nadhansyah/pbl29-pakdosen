<?php

namespace App\Models;

use CodeIgniter\Model;

class VerifusulanModel extends Model
{
    protected $db;
    public function __construct()
    {
        $this->db = \Config\Database::connect();
    }

    function get_data()
    {
        $data = $this->db->query("SELECT ur.nik_reviewer_1, ur.nik_reviewer_2, u.id_usulan, u.nik_dosen, u.jenis_usulan FROM usulan_reviewer as ur
        LEFT JOIN usulan as u ON u.id_usulan = ur.id_usulan");
        return $data->getResultArray();
    }

    // function insertdata($data)
    // {
    //     return $this->db->table('usulan')->insert($data);
    // }

    //form select option
    function get_ldsn()
    {
        $data = $this->db->query("select nik_dosen, nama from dosen");
        return $data->getResultArray();
    }

    function get_databyid($id)
    {
        $data = $this->db->query("select * from usulan where id_usulan = '" . $id . "'");
        return $data->getRow();
    }

    // function updatedata($id, $data)
    // {
    //     return $this->db->table('usulan')->update($data, array('id_usulan' => $id));
    // }

    // function hapusdata($id)
    // {
    //     return $this->db->table('usulan')->delete(array('id_usulan' => $id));
    // }

    // unpas
    // function getusulan($idusul)
    // {
    //     return $this->db->table('usulan')->where(['id_usulan' => $idusul]);
    // }
}
