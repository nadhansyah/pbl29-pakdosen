<?php

namespace App\Models;

use CodeIgniter\Model;

class UsulanModel extends Model
{
    protected $db;
    public function __construct()
    {
        $this->db = \Config\Database::connect();
    }

    function get_data()
    {
        $data = $this->db->query('select * from usulan');
        return $data->getResultArray();
    }

    function insertdata($data)
    {
        return $this->db->table('usulan')->insert($data);
    }

    //form select option
    function get_ldsn()
    {
        $data = $this->db->query("select nik_dosen, nama from dosen");
        return $data->getResultArray();
    }

    function get_databyid($id)
    {
        $data = $this->db->query("select * from usulan where id_usulan = '" . $id . "'");
        return $data->getRow();
    }

    function updatejenisusulan($id, $data)
    {

        $data = $this->db->query('UPDATE usulan SET jenis_usulan ="' . $data . '" WHERE id_usulan = "' . $id . '"');
        return $data;
    }

    function updatedata($id, $data)
    {
        return $this->db->table('usulan')->update($data, array('id_usulan' => $id));
    }

    function hapusdata($id)
    {
        return $this->db->table('usulan')->delete(array('id_usulan' => $id));
    }

    // unpas
    // function getusulan($idusul)
    // {
    //     return $this->db->table('usulan')->where(['id_usulan' => $idusul]);
    // }

    //Data ajuan opt - asistenahli&lektor
    function get_namadsn($nik)
    {
        $data = $this->db->query("select nama from dosen where nik_dosen ='" . $nik . "'");
        return $data->getRow();
    }

    //ajukan sekarang
    function ajukansekarang($id, $tgl, $status)
    {

        $data = $this->db->query('UPDATE usulan SET tgl_usulan ="' . $tgl . '", status_usulan ="' . $status . '" WHERE id_usulan = "' . $id . '"');
        return $data;
    }

    //validasi sekarang
    function validasisekarang($id, $tgl, $status)
    {

        $data = $this->db->query('UPDATE usulan SET tgl_validasi ="' . $tgl . '", status_usulan ="' . $status . '" WHERE id_usulan = "' . $id . '"');
        return $data;
    }

    //kumpul nilai sekarang
    function submitnilaisekarang($id, $status)
    {

        $data = $this->db->query('UPDATE usulan SET status_usulan ="' . $status . '" WHERE id_usulan = "' . $id . '"');
        return $data;
    }

    //update status verifikasi
    function verifikasisekarang($id, $status)
    {

        $data = $this->db->query('UPDATE usulan SET status_usulan ="' . $status . '" WHERE id_usulan = "' . $id . '"');
        return $data;
    }
}
