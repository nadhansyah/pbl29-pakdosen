<?php

namespace App\Models;

use CodeIgniter\Model;

class DosenModel extends Model
{
    protected $db;
    public function __construct()
    {
        $this->db = \Config\Database::connect();
    }

    function get_data()
    {
        $data = $this->db->query('select * from dosen');
        return $data->getResultArray();
    }

    function insertdata($data)
    {
        return $this->db->table('dosen')->insert($data);
    }

    function get_databyid($id)
    {
        $data = $this->db->query("select * from dosen where nik_dosen = '" . $id . "'");
        return $data->getRow();
    }

    function updatedata($id, $data)
    {
        return $this->db->table('dosen')->update($data, array('nik_dosen' => $id));
    }

    function hapusdata($id)
    {
        return $this->db->table('dosen')->delete(array('nik_dosen' => $id));
    }

    // login
    function loginprocessdsn($nik_dosen, $password)
    {
        return $this->db->query('select nik_dosen, nama, role from dosen where nik_dosen="' . $nik_dosen . '" and password="' . $password . '"')->getRow();
    }

    // Profile
    function get_profile()
    {
        $data = $this->db->query('select * from dosen');
        return $data->getRow();
    }

    function updateprofile($id, $data)
    {
        return $this->db->table('dosen')->update($data, array('nik_dosen' => $id));
    }

    //profiledsn - opt - rev
    function get_profiledsn($nik)
    {
        $data = $this->db->query("SELECT * FROM dosen WHERE nik_dosen ='" . $nik . "'");
        return $data->getRow();
    }

    //ubah pass
    function get_passdsn($nik)
    {
        $data = $this->db->query("SELECT nik_dosen, password FROM dosen WHERE nik_dosen ='" . $nik . "'");
        return $data->getRow();
    }

    function passprocess($nik_dosen, $password)
    {
        return $this->db->query('select nik_dosen, nama from dosen where nik_dosen="' . $nik_dosen . '" and password="' . $password . '"')->getRow();
    }

    function updatepass($nik, $data)
    {
        $data = $this->db->query('UPDATE dosen SET password ="' . $data . '" WHERE nik_dosen = "' . $nik . '"');
        return $data;
    }
}
