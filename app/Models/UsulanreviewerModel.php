<?php

namespace App\Models;

use CodeIgniter\Model;

class UsulanreviewerModel extends Model
{
    protected $db;
    public function __construct()
    {
        $this->db = \Config\Database::connect();
    }

    function get_data()
    {
        $data = $this->db->query('select * from usulan');
        return $data->getResultArray();
    }

    function insertdata($data)
    {
        return $this->db->table('usulan_reviewer')->insert($data);
    }

    //form select option
    function get_niknamadosen()
    {
        $data = $this->db->query("select nik_dosen, nama from dosen where role = 'Dosen'");
        return $data->getResultArray();
    }

    function get_niknamareviewer()
    {
        $data = $this->db->query("select nik_dosen, nama from dosen where role = 'Reviewer'");
        return $data->getResultArray();
    }

    // function get_nama($id)
    // {
    //     $data = $this->db->query("select nik_dosen, nama from dosen where nik_dosen = " . $id);
    //     return $data->getR();
    // }

    function get_databyid($id)
    {
        $data = $this->db->query("select * from usulan_reviewer where id_usulan = '" . $id . "'");
        return $data->getRow();
    }

    // function get_databyidrev($id)
    // {
    //     $data = $this->db->query("select * from usulan_reviewer where id_usulan_reviewer = '" . $id . "'");
    //     return $data->getRow();
    // }

    function updatedatausul($id, $data)
    {
        return $this->db->table('usulan')->update($data, array('id_usulan' => $id));
    }
    function updatedatarev($id, $data)
    {
        return $this->db->table('usulan_reviewer')->update($data, array('id_usulan_reviewer' => $id));
    }

    function hapusdata($id)
    {
        return $this->db->table('usulan')->delete(array('id_usulan' => $id));
    }

    // unpas
    // function getusulan($idusul)
    // {
    //     return $this->db->table('usulan')->where(['id_usulan' => $idusul]);
    // }


    //ajukan sekarang
    // function ajukansekarang($id, $tgl, $status)
    // {

    //     $data = $this->db->query('UPDATE usulan SET tgl_usulan ="' . $tgl . '", status_usulan ="' . $status . '" WHERE id_usulan = "' . $id . '"');
    //     return $data;
    // }

    // //validasi sekarang
    // function validasisekarang($id, $tgl, $status)
    // {

    //     $data = $this->db->query('UPDATE usulan SET tgl_validasi ="' . $tgl . '", status_usulan ="' . $status . '" WHERE id_usulan = "' . $id . '"');
    //     return $data;
    // }
}
