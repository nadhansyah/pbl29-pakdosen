<?php

namespace App\Models;

use CodeIgniter\Model;

class NilaipengabdianModel extends Model
{
    protected $db;
    public function __construct()
    {
        $this->db = \Config\Database::connect();
    }

    function get_data()
    {
        $data = $this->db->query('select * from pengabdian where nik_dosen = ');
        return $data->getResultArray();
    }

    function get_databyid($id)
    {
        $data = $this->db->query("select * from pengabdian_reviewer where id_pengabdian = '" . $id . "'");
        return $data->getRow();
    }

    function updatedata($id, $data)
    {
        return $this->db->table('pengabdian_reviewer')->update($data, array('id_pengabdian' => $id));
    }
}
