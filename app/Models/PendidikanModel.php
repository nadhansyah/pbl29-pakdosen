<?php

namespace App\Models;

use CodeIgniter\Model;

class PendidikanModel extends Model
{
    protected $db;
    public function __construct()
    {
        $this->db = \Config\Database::connect();
    }

    function get_data()
    {
        $data = $this->db->query('select * from pendidikan where id_usulan = ');
        return $data->getResultArray();
    }

    function insertdata($data)
    {
        return $this->db->table('pendidikan')->insert($data);
    }

    function get_databyid($id)
    {
        $data = $this->db->query("select * from pendidikan where id_pendidikan = '" . $id . "'");
        return $data->getRow();
    }

    function updatedata($id, $data)
    {
        return $this->db->table('pendidikan')->update($data, array('id_pendidikan' => $id));
    }

    function hapusdata($id)
    {
        return $this->db->table('pendidikan')->delete(array('id_pendidikan' => $id));
    }
    // rev
}
