<?php

namespace App\Models;

use CodeIgniter\Model;

class NilaipenelitianModel extends Model
{
    protected $db;
    public function __construct()
    {
        $this->db = \Config\Database::connect();
    }

    function get_data()
    {
        $data = $this->db->query('select * from penelitian where nik_dosen = ');
        return $data->getResultArray();
    }

    function get_databyid($id)
    {
        $data = $this->db->query("select * from penelitian_reviewer where id_penelitian = '" . $id . "'");
        return $data->getRow();
    }

    function updatedata($id, $data)
    {
        return $this->db->table('penelitian_reviewer')->update($data, array('id_penelitian' => $id));
    }
}
