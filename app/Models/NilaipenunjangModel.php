<?php

namespace App\Models;

use CodeIgniter\Model;

class NilaipenunjangModel extends Model
{
    protected $db;
    public function __construct()
    {
        $this->db = \Config\Database::connect();
    }

    function get_data()
    {
        $data = $this->db->query('select * from penunjang where nik_dosen = ');
        return $data->getResultArray();
    }

    function get_databyid($id)
    {
        $data = $this->db->query("select * from penunjang_reviewer where id_penunjang = '" . $id . "'");
        return $data->getRow();
    }

    function updatedata($id, $data)
    {
        return $this->db->table('penunjang_reviewer')->update($data, array('id_penunjang' => $id));
    }
}
