<?php

namespace App\Models;

use CodeIgniter\Model;

class NilaipendidikanModel extends Model
{
    protected $db;
    public function __construct()
    {
        $this->db = \Config\Database::connect();
    }

    function get_data()
    {
        $data = $this->db->query('select * from pendidikan where nik_dosen = ');
        return $data->getResultArray();
    }

    function get_databyid($id)
    {
        $data = $this->db->query("select * from pendidikan_reviewer where id_pendidikan = '" . $id . "'");
        return $data->getRow();
    }

    function updatedata($id, $data)
    {
        return $this->db->table('pendidikan_reviewer')->update($data, array('id_pendidikan' => $id));
    }
    function insertdata($data)
    {
        return $this->db->table('pendidikan_reviewer')->insert($data);
    }
}
