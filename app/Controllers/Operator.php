<?php

namespace App\Controllers;

use App\Models\OperatorModel;
use CodeIgniter\Controller;
use App\Libraries\Datatables;

class Operator extends BaseController
{
    protected $opt;
    protected $db;
    public function __construct()
    {
        $this->opt = new OperatorModel();
        $this->db = \Config\Database::connect();
    }

    public function index()
    {
        $data = array(
            'title' => 'Daftar Operator',
            'isi' => 'opt/listoperator',
            'css' => '
            <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/v/bs/jszip-2.5.0/dt-1.10.16/af-2.2.2/b-1.5.1/b-colvis-1.5.1/b-flash-1.5.1/b-html5-1.5.1/b-print-1.5.1/cr-1.4.1/fc-3.2.4/fh-3.1.3/kt-2.3.2/r-2.2.1/rg-1.0.2/rr-1.2.3/sc-1.4.4/sl-1.2.5/datatables.min.css"/>
            ',
            'js' => '
            <script type="text/javascript" src="https://cdn.datatables.net/v/bs/jszip-2.5.0/dt-1.10.16/af-2.2.2/b-1.5.1/b-colvis-1.5.1/b-flash-1.5.1/b-html5-1.5.1/b-print-1.5.1/cr-1.4.1/fc-3.2.4/fh-3.1.3/kt-2.3.2/r-2.2.1/rg-1.0.2/rr-1.2.3/sc-1.4.4/sl-1.2.5/datatables.min.js"></script>
            ',
        );
        echo view('layout/optwraper', $data);
    }

    public function ajaxloaddata()
    {
        $params['draw'] = $_REQUEST['draw'];
        $start = $_REQUEST['start'];
        $length = $_REQUEST['length'];
        $search_value = $_REQUEST['search']['value'];
        //$data=array();
        if (!empty($search_value)) {
            $total_count = $this->db->query("SELECT * from operator WHERE nik_operator like '%" . $search_value . "%' OR nama like '%" . $search_value . "%' OR email like '%" . $search_value . "%' OR no_hp like '%" . $search_value . "%'")->getResult();
            $data = $this->db->query("SELECT * from operator WHERE nik_operator like '%" . $search_value . "%' OR nama like '%" . $search_value . "%' OR email like '%" . $search_value . "%' OR no_hp like '%" . $search_value . "%' limit $start, $length")->getResult();
        } else {
            $total_count = $this->db->query("SELECT * from operator")->getResult();
            $data = $this->db->query("SELECT * from operator limit $start, $length")->getResult();
        }

        $data1 = array();
        foreach ($data as $ld) {
            $row = array(
                "nik_operator"      => $ld->nik_operator,
                "nama"    => $ld->nama,
                "email"     => $ld->email,
                "no_hp"       => $ld->no_hp,
                "action"       => "<a href='" . Base_url('operator/edit/' . $ld->nik_operator) . "'><button type='button' class='btn btn-warning btn-xs'><i class='fa fa-pencil'></i></button></a>",

            );
            $data1[] = $row;
        }

        $json_data = array(
            "data" => $data1,
            "recordsTotal" => count($total_count),
            "recordsFiltered" => count($total_count),
            "draw" => intval($params['draw']),
        );

        echo json_encode($json_data);
    }

    public function create()
    {
        $data = array(
            'title'     => 'Input Data Operator',
            'isi'       => 'opt/formoperator',
            'action'    => 'create_action',
            'button'    => 'Simpan',
            // 'idkamar'   => set_value('idkamar'),
            // 'kodekamar' => set_value('kodekamar'),
            // 'namatipe'  => set_value('namatipe'),
            // 'ukuran'    => set_value('ukuran'),
            'nik_operator'   => 'nik_operator',
            'nama' => 'nama',
            'password' => 'password',
            'email'  => 'email',
            'no_hp'    => 'no_hp'
        );
        echo view("layout/optwraper", $data);
    }

    public function create_action()
    {
        $data = array(
            'nik_operator' => !empty($this->request->getPost('nik_operator')) ? $this->request->getPost('nik_operator') : NULL,
            'nama' => !empty($this->request->getPost('nama')) ? $this->request->getPost('nama') : NULL,
            'password' => !empty($this->request->getPost('password')) ? $this->request->getPost('password') : NULL,
            'email' => !empty($this->request->getPost('email')) ? $this->request->getPost('email') : NULL,
            'no_hp' => !empty($this->request->getPost('no_hp')) ? $this->request->getPost('no_hp') : NULL,
        );
        $this->opt->insertdata($data);
        return redirect()->to(base_url('operator'));
    }

    public function edit($id)
    {
        $dataopt = $this->opt->get_databyid($id);
        $data = array(
            'title'         => 'Edit Data Operator',
            'isi'           => 'opt/formoperator',
            'action'        => base_url('operator/edit_action'),
            'button'        => 'Update',
            'js'            => '
            <script src="https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/7.18.0/sweetalert2.all.min.js"></script>
            <script src="https://cdnjs.cloudflare.com/ajax/libs/core-js/2.4.1/core.js"></script>
            <script>
                    // delete
                    $(function() {
                        $(\'.delete\').on(\'click\', function(){
                            var delete_url = $(this).attr(\'data-url\');
                            swal({
                                title: \'Are you sure?\',
                                text: \'You wont be able to revert this!\',
                                type: \'warning\',
                                showCancelButton: true,
                                confirmButtonColor: \'#3085d6\',
                                cancelButtonColor: \'#d33\',
                                confirmButtonText: \'Yes, delete it!\',
                                cancelButtonText: \'No, cancel!\',
                                confirmButtonClass: \'btn btn-success\',
                                cancelButtonClass: \'btn btn-danger\',
                                buttonsStyling: false,
                                reverseButtons: true
                            }).then((result) => {
                                if (result.value) {
                                    window.location.href = delete_url;
                                }
                            });
                        });
                    });
                </script>
            ',
            'nik_operator'  => $id,
            'nama'          => $dataopt->nama,
            'password'      => $dataopt->password,
            'email'         => $dataopt->email,
            'no_hp'         => $dataopt->no_hp,
        );
        echo view("layout/optwraper", $data);
    }

    public function edit_action()
    {
        $nik_operator = $this->request->getPost('nik_operator');
        $data = array(
            'nama' => !empty($this->request->getPost('nama')) ? $this->request->getPost('nama') : NULL,
            'password' => !empty($this->request->getPost('password')) ? $this->request->getPost('password') : NULL,
            'email' => !empty($this->request->getPost('email')) ? $this->request->getPost('email') : NULL,
            'no_hp' => !empty($this->request->getPost('no_hp')) ? $this->request->getPost('no_hp') : NULL,
        );
        $this->opt->updatedata($nik_operator, $data);
        return redirect()->to(base_url('operator'));
    }

    public function delete($id)
    {
        $this->opt->hapusdata($id);
        return redirect()->to(base_url('operator'));
    }

    // profile
    public function profile()
    {
        $dataopt = $this->opt->get_profile();
        $data = array(
            'title' => 'Profile',
            'action' => base_url('operator/profile_action'),
            'isi' => 'opt/profile',
            'nik_operator' => $dataopt->nik_operator,
            'nama' => $dataopt->nama,
            'email' => $dataopt->email,
            'no_hp' => $dataopt->no_hp
        );
        echo view('layout/optwraper', $data);
    }

    public function profile_action()
    {
        $nik_operator = $this->request->getPost('nik_operator');
        $data = array(
            'nama' => !empty($this->request->getPost('nama')) ? $this->request->getPost('nama') : NULL,
            'email' => !empty($this->request->getPost('email')) ? $this->request->getPost('email') : NULL,
            'no_hp' => !empty($this->request->getPost('no_hp')) ? $this->request->getPost('no_hp') : NULL,
        );
        $this->opt->updateprofile($nik_operator, $data,);
        return redirect()->to(base_url('operator/profile'));
    }

    // Login

    public function logout()
    {
        $session = session();
        $session->destroy();
        return redirect()->to('login');
    }

    //change password
    public function pass()
    {
        $data = array(
            'title' => 'Ubah Sandi',
            'action' => base_url('operator/pass_action'),
            'isi' => 'opt/formpass',
        );
        echo view('layout/optwraper', $data);
    }

    public function pass_action()
    {
        $session = session();
        $id = $session->nik_operator;

        $nik = $this->request->getPost('nik_operator');
        $passlama = $this->request->getPost('passlama');
        $passbaru = $this->request->getPost('passbaru');
        $valpassbaru = $this->request->getPost('valpassbaru');

        if ($this->opt->passprocess($nik, $passlama) && $passbaru == $valpassbaru && $nik == $id) {
            $data = $passbaru;

            $this->opt->updatepass($nik, $data);
        }
        return redirect()->to(base_url('operator/pass'));
    }
}
