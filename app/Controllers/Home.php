<?php

namespace App\Controllers;

class Home extends BaseController
{
    public function __construct()
    {
    }

    // Dashboard Dosen
    public function index()
    {
        $data = array(
            'title' => 'Home',
            'isi' => 'opt/home'
        );
        echo view('layout/optwraper', $data);
    }

    // Dashboard Operator
    public function dsn()
    {
        $data = array(
            'title' => 'Home',
            'isi' => 'dsn/home'
        );
        echo view('layout/dsnwraper', $data);
    }
}
