<?php

namespace App\Controllers;

use App\Models\PenunjangModel;
use App\Models\NilaipenunjangModel;
use CodeIgniter\Controller;
use App\Libraries\Datatables;
use App\Models\UsulanModel;

class Nilaipenunjang extends BaseController
{
    protected $penj;
    protected $npenj;
    protected $usul;
    protected $db;
    public function __construct()
    {
        $this->penj = new PenunjangModel();
        $this->npenj = new NilaipenunjangModel();
        $this->usul = new UsulanModel();
        $this->db = \Config\Database::connect();
    }

    public function index()
    {
        $data = array(
            'title' => 'Data Nilai Penunjang',
            'isi' => 'rev/listpenunjang',
            'css' => '
            <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/v/bs/jszip-2.5.0/dt-1.10.16/af-2.2.2/b-1.5.1/b-colvis-1.5.1/b-flash-1.5.1/b-html5-1.5.1/b-print-1.5.1/cr-1.4.1/fc-3.2.4/fh-3.1.3/kt-2.3.2/r-2.2.1/rg-1.0.2/rr-1.2.3/sc-1.4.4/sl-1.2.5/datatables.min.css"/>
            ',
            'js' => '
            <script type="text/javascript" src="https://cdn.datatables.net/v/bs/jszip-2.5.0/dt-1.10.16/af-2.2.2/b-1.5.1/b-colvis-1.5.1/b-flash-1.5.1/b-html5-1.5.1/b-print-1.5.1/cr-1.4.1/fc-3.2.4/fh-3.1.3/kt-2.3.2/r-2.2.1/rg-1.0.2/rr-1.2.3/sc-1.4.4/sl-1.2.5/datatables.min.js"></script>
            ',
            // 'bcreate'  => "<a href=" . Base_url('pendidikan/create/' . $ld->id_usulan) . "><button type='button' class='btn btn-success btn-sm'><i class='fa fa-plus'></i> Create</button></a>"
        );
        echo view('layout/dsnwraper', $data);
    }

    public function ajaxloaddata()
    {
        $session = session();
        $nik = $session->nik_dosen;
        $params['draw'] = $_REQUEST['draw'];
        $start = $_REQUEST['start'];
        $length = $_REQUEST['length'];
        $search_value = $_REQUEST['search']['value'];
        //$data=array();
        if (!empty($search_value)) {
            $total_count = $this->db->query("SELECT pj.id_penunjang, pj.jenis_pelaksanaan, pj.detail_pelaksanaan,
            pj.jumlah_volume_dosen, pjr.jumlah_volume_reviewer 
            FROM penunjang pj 
            LEFT JOIN penunjang_reviewer pjr 
            ON pjr.id_penunjang = pj.id_penunjang
            WHERE pj.jenis_pelaksanaan like '%" . $search_value . "%' OR pj.detail_pelaksanaan like '%" . $search_value . "%' OR pj.jumlah_volume_dosen like '%" . $search_value . "%' OR pjr.jumlah_volume_reviewer like '%" . $search_value . "%'")->getResult();
            $data = $this->db->query("SELECT pj.id_penunjang, pj.jenis_pelaksanaan, pj.detail_pelaksanaan,
            pj.jumlah_volume_dosen, pjr.jumlah_volume_reviewer 
            FROM penunjang pj 
            LEFT JOIN penunjang_reviewer pjr 
            ON pjr.id_penunjang = pj.id_penunjang
            WHERE pj.jenis_pelaksanaan like '%" . $search_value . "%' OR pj.detail_pelaksanaan like '%" . $search_value . "%' OR pj.jumlah_volume_dosen like '%" . $search_value . "%' OR pjr.jumlah_volume_reviewer like '%" . $search_value . "%' limit $start, $length")->getResult();
        } else {
            $total_count = $this->db->query("SELECT pj.id_penunjang, pj.jenis_pelaksanaan, pj.detail_pelaksanaan,
            pj.jumlah_volume_dosen, pjr.jumlah_volume_reviewer 
            FROM penunjang pj 
            LEFT JOIN penunjang_reviewer pjr 
            ON pjr.id_penunjang = pj.id_penunjang")->getResult();
            $data = $this->db->query("SELECT pj.id_penunjang, pj.jenis_pelaksanaan, pj.detail_pelaksanaan,
            pj.jumlah_volume_dosen, pjr.jumlah_volume_reviewer 
            FROM penunjang pj 
            LEFT JOIN penunjang_reviewer pjr 
            ON pjr.id_penunjang = pj.id_penunjang limit $start, $length")->getResult();
        }

        $data1 = array();
        foreach ($data as $ld) {
            $row = array(
                "id_penunjang"         => $ld->id_penunjang,
                "jenis_pelaksanaan"          => $ld->jenis_pelaksanaan,
                "detail_pelaksanaan"         => $ld->detail_pelaksanaan,
                "jumlah_volume_dosen"       => $ld->jumlah_volume_dosen,
                "jumlah_volume_reviewer"    => $ld->jumlah_volume_reviewer,
                "action"                    => "<a href='" . Base_url('nilaipenunjang/edit/' . $ld->id_penunjang) . "'><button type='button' class='btn btn-warning btn-xs'><i class='fa fa-pencil'></i></button></a>",
            );
            $data1[] = $row;
        }

        $json_data = array(
            "data"              => $data1,
            "recordsTotal"      => count($total_count),
            "recordsFiltered"   => count($total_count),
            "draw"              => intval($params['draw']),
        );

        echo json_encode($json_data);
    }

    public function edit($id)
    {
        $datapenj = $this->penj->get_databyid($id);
        $datanpenj = $this->npenj->get_databyid($id);
        // $datausul = $this->usul->get_databyid($id);
        $data = array(
            'title'         => 'Edit Data Nilai Penunjang',
            'isi'           => 'rev/formpenunjang',
            'action'        => base_url('nilaipenunjang/edit_action'),
            'button'        => 'Update',
            'js'            => '
            <script src="https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/7.18.0/sweetalert2.all.min.js"></script>
            <script src="https://cdnjs.cloudflare.com/ajax/libs/core-js/2.4.1/core.js"></script>',
            'id_penunjang'         => $id,
            'id_usulan'                 => $datapenj->id_usulan,
            'nik_dosen'                 => $datapenj->nik_dosen,
            'jenis_pelaksanaan'          => $datapenj->jenis_pelaksanaan,
            'detail_pelaksanaan'         => $datapenj->detail_pelaksanaan,
            'keterangan_pelaksanaan'     => $datapenj->keterangan_pelaksanaan,
            'tgl_penunjang'            => $datapenj->tgl_penunjang,
            'satuan_hasil'              => $datapenj->satuan_hasil,
            'jumlah_volume_dosen'       => $datapenj->jumlah_volume_dosen,
            'keterangan'                => $datapenj->keterangan,
            'bukti_file'                => $datapenj->bukti_file,
            'jumlah_volume_reviewer'    => !empty($datanpenj->jumlah_volume_reviewer) ? $datanpenj->jumlah_volume_reviewer : 0,
        );
        echo view("layout/dsnwraper", $data);
    }

    public function edit_action()
    {
        // $id_usulan = $this->request->getPost('id_usulan');
        $id_penunjang = $this->request->getPost('id_penunjang');
        $data = array(
            'jumlah_volume_reviewer' => !empty($this->request->getPost('jumlah_volume_reviewer')) ? $this->request->getPost('jumlah_volume_reviewer') : NULL,
        );
        $this->npenj->updatedata($id_penunjang, $data);
        return redirect()->to(base_url('nilaipenunjang'));
    }
}
