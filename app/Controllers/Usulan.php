<?php

namespace App\Controllers;

use App\Models\UsulanModel;
use CodeIgniter\Controller;
use App\Libraries\Datatables;

class Usulan extends BaseController
{
    protected $usul;
    protected $db;
    public function __construct()
    {
        $this->usul = new UsulanModel();
        $this->db = \Config\Database::connect();
    }

    public function index()
    {
        $data = array(
            'title' => 'Pengajuan Usulan',
            'isi'   => 'dsn/listusulan',
            'css'   => '
            <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/v/bs/jszip-2.5.0/dt-1.10.16/af-2.2.2/b-1.5.1/b-colvis-1.5.1/b-flash-1.5.1/b-html5-1.5.1/b-print-1.5.1/cr-1.4.1/fc-3.2.4/fh-3.1.3/kt-2.3.2/r-2.2.1/rg-1.0.2/rr-1.2.3/sc-1.4.4/sl-1.2.5/datatables.min.css"/>
            ',
            'js'    => '
            <script type="text/javascript" src="https://cdn.datatables.net/v/bs/jszip-2.5.0/dt-1.10.16/af-2.2.2/b-1.5.1/b-colvis-1.5.1/b-flash-1.5.1/b-html5-1.5.1/b-print-1.5.1/cr-1.4.1/fc-3.2.4/fh-3.1.3/kt-2.3.2/r-2.2.1/rg-1.0.2/rr-1.2.3/sc-1.4.4/sl-1.2.5/datatables.min.js"></script>
            ',
        );
        echo view('layout/dsnwraper', $data);
    }

    public function ajaxloaddata()
    {
        $session = session();
        $nik = $session->nik_dosen;
        $params['draw'] = $_REQUEST['draw'];
        $start          = $_REQUEST['start'];
        $length         = $_REQUEST['length'];
        $search_value   = $_REQUEST['search']['value'];
        //$data=array();
        if (!empty($search_value)) {
            $total_count = $this->db->query("SELECT * from usulan WHERE  nik_dosen = '" . $nik . " AND jenis_usulan like '%" . $search_value . "%' OR tgl_usulan like '%" . $search_value . "%' OR status_usulan like '%" . $search_value . "%'")->getResult();
            $data = $this->db->query("SELECT * from usulan WHERE  nik_dosen = '" . $nik . " AND jenis_usulan like '%" . $search_value . "%' OR tgl_usulan like '%" . $search_value . "%' OR status_usulan like '%" . $search_value . "%' limit $start, $length")->getResult();
        } else {
            $total_count = $this->db->query("SELECT * from usulan where nik_dosen = '" . $nik . "'")->getResult();
            $data = $this->db->query("SELECT * from usulan where nik_dosen = '" . $nik . "' limit $start, $length")->getResult();
        }

        $data1 = array();
        foreach ($data as $ld) {
            $row = array(
                "jenis_usulan"      => $ld->jenis_usulan,
                "tgl_usulan"        => $ld->tgl_usulan,
                "status_usulan"     => $ld->status_usulan,
                "dokumen"            => "<a href='" . Base_url('usulan/detail/' . $ld->id_usulan) . "'><button type='button' class='btn btn-info btn-xs'><i class='fa fa-info-circle'></i> Detail </button></a>",
                "action"            => "<a href='" . Base_url('usulan/edit/' . $ld->id_usulan) . "'><button type='button' class='btn btn-warning btn-xs'><i class='fa fa-pencil'></i></button></a>",

            );
            $data1[] = $row;
        }

        $json_data = array(
            "data"              => $data1,
            "recordsTotal"      => count($total_count),
            "recordsFiltered"   => count($total_count),
            "draw"              => intval($params['draw']),
        );

        echo json_encode($json_data);
    }

    public function create()
    {
        $data = array(
            'title'         => 'Input Data Ajuan',
            'isi'           => 'dsn/formusulan',
            'action'        => 'create_action',
            'button'        => 'Simpan',
            'id_usulan'     => 'id_usulan',
            'nik_dosen'     => 'nik_dosen',
            'jenis_usulan'  => 'jenis_usulan',
            // 'tgl_usulan'    => 'tgl_usulan',
            // 'status_usulan' => 'status_usulan'
        );
        echo view("layout/dsnwraper", $data);
    }

    public function create_action()
    {
        $session = session();
        $nik = $session->nik_dosen;
        $data = array(
            'nik_dosen'     => $nik,
            'jenis_usulan'  => !empty($this->request->getPost('jenis_usulan')) ? $this->request->getPost('jenis_usulan') : NULL,
            'tgl_usulan'    => !empty($this->request->getPost('tgl_usulan')) ? $this->request->getPost('tgl_usulan') : NULL,
            // 'status_usulan' => !empty($this->request->getPost('status_usulan')) ? $this->request->getPost('status_usulan') : NULL,
        );
        $this->usul->insertdata($data);
        return redirect()->to(base_url('usulan'));
    }

    public function edit($id)
    {
        $datausul = $this->usul->get_databyid($id);
        $data = array(
            'title'         => 'Edit Data Ajuan',
            'isi'           => 'dsn/formusulan',
            'action'        => base_url('usulan/edit_action/' . $id),
            'button'        => 'Update',
            'js'            => '
            <script src="https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/7.18.0/sweetalert2.all.min.js"></script>
            <script src="https://cdnjs.cloudflare.com/ajax/libs/core-js/2.4.1/core.js"></script>
            <script>
                    // delete
                    $(function() {
                        $(\'.delete\').on(\'click\', function(){
                            var delete_url = $(this).attr(\'data-url\');
                            swal({
                                title: \'Are you sure?\',
                                text: \'You wont be able to revert this!\',
                                type: \'warning\',
                                showCancelButton: true,
                                confirmButtonColor: \'#3085d6\',
                                cancelButtonColor: \'#d33\',
                                confirmButtonText: \'Yes, delete it!\',
                                cancelButtonText: \'No, cancel!\',
                                confirmButtonClass: \'btn btn-success\',
                                cancelButtonClass: \'btn btn-danger\',
                                buttonsStyling: false,
                                reverseButtons: true
                            }).then((result) => {
                                if (result.value) {
                                    window.location.href = delete_url;
                                }
                            });
                        });
                    });
                </script>
            ',
            'id_usulan'       => $id,
            'nik_dosen'     => $datausul->nik_dosen,
            'jenis_usulan'      => $datausul->jenis_usulan,
            // 'tgl_usulan'        => $datausul->tgl_usulan,
            // 'status_usulan'        => $datausul->status_usulan,
        );
        echo view("layout/dsnwraper", $data);
    }

    public function edit_action($id)
    {
        // $id_usulan = $this->request->getPost('id_usulan');
        $data = !empty($this->request->getPost('jenis_usulan')) ? $this->request->getPost('jenis_usulan') : NULL;
        // 'tgl_usulan' => !empty($this->request->getPost('tgl_usulan')) ? $this->request->getPost('tgl_usulan') : NULL,
        // 'status_usulan' => !empty($this->request->getPost('status_usulan')) ? $this->request->getPost('status_usulan') : NULL,
        $this->usul->updatejenisusulan($id, $data);

        return redirect()->to(base_url('usulan'));
    }

    public function delete($id)
    {
        $this->usul->hapusdata($id);
        return redirect()->to(base_url('usulan'));
    }

    public function detail($id)
    {
        $datausul = $this->usul->get_databyid($id);
        // if (empty('$session->idusulan')) {
        //     $session = session(); 
        //     $ses_idusulan = [
        //         'idusulan'      => $id,
        //     ];
        //     $session->set($ses_idusulan);
        // }
        $data = array(
            'title' => 'Detail Ajuan',
            'isi'   => 'dsn/detailajuan',
            'css'   => '
                <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/v/bs/jszip-2.5.0/dt-1.10.16/af-2.2.2/b-1.5.1/b-colvis-1.5.1/b-flash-1.5.1/b-html5-1.5.1/b-print-1.5.1/cr-1.4.1/fc-3.2.4/fh-3.1.3/kt-2.3.2/r-2.2.1/rg-1.0.2/rr-1.2.3/sc-1.4.4/sl-1.2.5/datatables.min.css"/>
                ',
            'js'            => '
                <script src="https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/7.18.0/sweetalert2.all.min.js"></script>
                <script src="https://cdnjs.cloudflare.com/ajax/libs/core-js/2.4.1/core.js"></script>
                <script>
                    $(function() {
                        $(\'.aju\').on(\'click\', function(){
                            var aju_url = $(this).attr(\'data-url\');
                            swal({
                                title: \'Yakin ingin mengajukan seluruh data sekarang?\',
                                text: \'Anda tidak dapat mengubah data lagi!\',
                                type: \'warning\',
                                showCancelButton: true,
                                confirmButtonColor: \'#3085d6\',
                                cancelButtonColor: \'#d33\',
                                confirmButtonText: \'Ya, yakin!\',
                                cancelButtonText: \'Tidak, kembali!\',
                                confirmButtonClass: \'btn btn-success\',
                                cancelButtonClass: \'btn btn-danger\',
                                buttonsStyling: false,
                                reverseButtons: true
                            }).then((result) => {
                                if (result.value) {
                                    window.location.href = aju_url;
                                }
                            });
                        });
                    });
                </script>',
            'id_usulan'         => $id,
            'nik_dosen'         => $datausul->nik_dosen,
            'jenis_usulan'      => $datausul->jenis_usulan,
        );
        echo view('layout/dsnwraper', $data);
    }


    // //ajukan sekarang
    // public function aju($id)
    // {
    //     $datausul = $this->usul->get_databyid($id);
    //     $data = array(
    //         'id_usulan'       => $id,
    //         'nik_dosen'     => $datausul->nik_dosen,
    //         'jenis_usulan'      => $datausul->jenis_usulan,
    //         'tgl_usulan'        => $datausul->tgl_usulan,
    //         'status_usulan'        => $datausul->status_usulan,
    //     );
    //     echo view("layout/dsnwraper", $data);
    // }

    public function aju($id)
    {
        // $datausul = $this->usul->get_databyid($id);
        $tgl = date('Y-m-d H:i:s');
        $status = 'Diajukan';

        $this->usul->ajukansekarang($id, $tgl, $status);

        return redirect()->to(base_url('usulan'));
    }

    //validasi ajuan - opt
    public function val($id)
    {
        $datausul = $this->usul->get_databyid($id);

        $tgl = date('Y-m-d H:i:s');
        $status = 'Tervalidasi';

        $this->usul->validasisekarang($id, $tgl, $status);

        if ($datausul->jenis_usulan == 'Asisten Ahli') {
            return redirect()->to(base_url('revassignment/create/' . $id));
        } else if ($datausul->jenis_usulan == 'Lektor') {
            return redirect()->to(base_url('revassignment/create/' . $id));
        }
        // echo $tgl;
        // echo $status;
        // echo $id;
    }

    //submit nilai reviewer
    public function sub($id)
    {
        $datausul = $this->usul->get_databyid($id);

        $tgl = date('Y-m-d H:i:s');
        $status = 'Sudah dinilai';

        $this->usul->submitnilaisekarang($id, $status);


        return redirect()->to(base_url('nilaiusulan'));
    }

    public function ver($id)
    {
        $datausul = $this->usul->get_databyid($id);

        $tgl = date('Y-m-d H:i:s');
        $status = 'Sudah dinilai';

        $this->usul->verifikasisekarang($id, $status);


        return redirect()->to(base_url('revassignment'));
    }
}
