<?php

namespace App\Controllers;

use App\Models\PengabdianModel;
use CodeIgniter\Controller;
use App\Libraries\Datatables;
use App\Models\UsulanModel;

class Pengabdian extends BaseController
{
    protected $peng;
    protected $usul;
    protected $db;
    public function __construct()
    {
        $this->peng = new PengabdianModel();
        $this->usul = new UsulanModel();
        $this->db = \Config\Database::connect();
    }

    public function index($id)
    {
        $data = array(
            'title' => 'Data Pengabdian kepada Masyarakat',
            'isi' => 'dsn/listpengabdian',
            'css' => '
            <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/v/bs/jszip-2.5.0/dt-1.10.16/af-2.2.2/b-1.5.1/b-colvis-1.5.1/b-flash-1.5.1/b-html5-1.5.1/b-print-1.5.1/cr-1.4.1/fc-3.2.4/fh-3.1.3/kt-2.3.2/r-2.2.1/rg-1.0.2/rr-1.2.3/sc-1.4.4/sl-1.2.5/datatables.min.css"/>
            ',
            'js' => '
            <script type="text/javascript" src="https://cdn.datatables.net/v/bs/jszip-2.5.0/dt-1.10.16/af-2.2.2/b-1.5.1/b-colvis-1.5.1/b-flash-1.5.1/b-html5-1.5.1/b-print-1.5.1/cr-1.4.1/fc-3.2.4/fh-3.1.3/kt-2.3.2/r-2.2.1/rg-1.0.2/rr-1.2.3/sc-1.4.4/sl-1.2.5/datatables.min.js"></script>
            ',
            'idusul' => $id,
        );
        echo view('layout/dsnwraper', $data);
    }

    public function ajaxloaddata($id)
    {
        $session = session();
        $nik = $session->nik_dosen;
        $params['draw'] = $_REQUEST['draw'];
        $start = $_REQUEST['start'];
        $length = $_REQUEST['length'];
        $search_value = $_REQUEST['search']['value'];
        //$data=array();
        if (!empty($search_value)) {
            $total_count = $this->db->query("SELECT * from pengabdian WHERE id_usulan = '" . $id . "' OR jenis_pelaksanaan like '%" . $search_value . "%' OR detail_pelaksanaan like '%" . $search_value . "%' OR jumlah_volume_dosen like '%" . $search_value . "%'")->getResult();
            $data = $this->db->query("SELECT * from pengabdian WHERE id_usulan = '" . $id . "' OR jenis_pelaksanaan like '%" . $search_value . "%' OR detail_pelaksanaan like '%" . $search_value . "%' OR jumlah_volume_dosen like '%" . $search_value . "%' limit $start, $length")->getResult();
        } else {
            $total_count = $this->db->query("SELECT * from pengabdian where id_usulan = '" . $id . "'")->getResult();
            $data = $this->db->query("SELECT * from pengabdian where id_usulan = '" . $id . "' limit $start, $length")->getResult();
        }

        $no = 1;
        $data1 = array();
        foreach ($data as $ld) {
            $row = array(

                "no"             => $no++,
                "id_usulan"             => $ld->id_usulan,
                "nik_dosen"             => $ld->nik_dosen, //utk cek aja. nnti dihapus
                "jenis_pelaksanaan"      => $ld->jenis_pelaksanaan,
                "detail_pelaksanaan"     => $ld->detail_pelaksanaan,
                "jumlah_volume_dosen"   => $ld->jumlah_volume_dosen,
                "action"                => "<a href='" . Base_url('pengabdian/edit/' . $ld->id_pengabdian) . "'><button type='button' class='btn btn-warning btn-xs'><i class='fa fa-pencil'></i></button></a>",
            );
            $data1[] = $row;
        }

        $json_data = array(
            "data"              => $data1,
            "recordsTotal"      => count($total_count),
            "recordsFiltered"   => count($total_count),
            "draw"              => intval($params['draw']),
        );

        echo json_encode($json_data);
    }

    // unpas
    // public function getusulan($idusul)
    // {
    //     $usulan = $this->usul->getUsul($idusul);
    //     return $usulan->id_usulan;
    // }

    public function create($idusul)
    {
        // $datausul = $this->usul->getUsulan($idusul);
        $data = array(
            'title'                 => 'Input Data Pengabdian kepada Masyarakat',
            'isi'                   => 'dsn/formpengabdian',
            'action'                => base_url('pengabdian/create_action/' . $idusul),
            'button'                => 'Simpan',
            'id_pengabdian'         => 'id_pengabdian',
            // 'id_usulan'             => "$datausul->id_usulan",
            'id_usulan'             => 'id_usulan',
            'nik_dosen'             => 'nik_dosen',
            'jenis_pelaksanaan'      => 'jenis_pelaksanaan',
            'detail_pelaksanaan'     => 'detail_pelaksanaan',
            'keterangan_pelaksanaan'     => 'keterangan_pelaksanaan',
            'tgl_pengabdian'        => 'tgl_pengabdian',
            'satuan_hasil'          => 'satuan_hasil',
            'jumlah_volume_dosen'   => 'jumlah_volume_dosen',
            'keterangan'            => 'keterangan',
            'bukti_file'            => 'bukti_file',
        );
        echo view("layout/dsnwraper", $data);
    }

    public function create_action($id)
    {
        $session = session();
        $nik = $session->nik_dosen;
        $data = array(
            'id_usulan'          => $id,
            'nik_dosen'          => $nik,
            'jenis_pelaksanaan'      => !empty($this->request->getPost('jenis_pelaksanaan')) ? $this->request->getPost('jenis_pelaksanaan') : NULL,
            'detail_pelaksanaan'         => !empty($this->request->getPost('detail_pelaksanaan')) ? $this->request->getPost('detail_pelaksanaan') : NULL,
            'keterangan_pelaksanaan'         => !empty($this->request->getPost('keterangan_pelaksanaan')) ? $this->request->getPost('keterangan_pelaksanaan') : NULL,
            'tgl_pengabdian'         => !empty($this->request->getPost('tgl_pengabdian')) ? $this->request->getPost('tgl_pengabdian') : NULL,
            'satuan_hasil'          => !empty($this->request->getPost('satuan_hasil')) ? $this->request->getPost('satuan_hasil') : NULL,
            'jumlah_volume_dosen'     => !empty($this->request->getPost('jumlah_volume_dosen')) ? $this->request->getPost('jumlah_volume_dosen') : NULL,
            'keterangan'          => !empty($this->request->getPost('keterangan')) ? $this->request->getPost('keterangan') : NULL,
            'bukti_file'      => !empty($this->request->getPost('bukti_file')) ? $this->request->getPost('bukti_file') : NULL,
        );
        $this->peng->insertdata($data);
        return redirect()->to(base_url('pengabdian/index/' . $id));
    }

    public function edit($id)
    {
        $datapeng = $this->peng->get_databyid($id);
        $idusul = $datapeng->id_usulan;
        // $datausul = $this->usul->get_databyid($id);
        $data = array(
            'title'         => 'Edit Data Pengabdian kepada Masyarakat',
            'isi'           => 'dsn/formpengabdian',
            'action'        => base_url('pengabdian/edit_action/' . $idusul . '/' . $id),
            'button'        => 'Update',
            'js'            => '
            <script src="https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/7.18.0/sweetalert2.all.min.js"></script>
            <script src="https://cdnjs.cloudflare.com/ajax/libs/core-js/2.4.1/core.js"></script>
            <script>
                    // delete
                    $(function() {
                        $(\'.delete\').on(\'click\', function(){
                            var delete_url = $(this).attr(\'data-url\');
                            swal({
                                title: \'Are you sure?\',
                                text: \'You wont be able to revert this!\',
                                type: \'warning\',
                                showCancelButton: true,
                                confirmButtonColor: \'#3085d6\',
                                cancelButtonColor: \'#d33\',
                                confirmButtonText: \'Yes, delete it!\',
                                cancelButtonText: \'No, cancel!\',
                                confirmButtonClass: \'btn btn-success\',
                                cancelButtonClass: \'btn btn-danger\',
                                buttonsStyling: false,
                                reverseButtons: true
                            }).then((result) => {
                                if (result.value) {
                                    window.location.href = delete_url;
                                }
                            });
                        });
                    });
                </script>
            ',
            'id_pengabdian'             => $id,
            'id_usulan'                 => $datapeng->id_usulan,
            'nik_dosen'                 => $datapeng->nik_dosen,
            'jenis_pelaksanaan'          => $datapeng->jenis_pelaksanaan,
            'detail_pelaksanaan'         => $datapeng->detail_pelaksanaan,
            'keterangan_pelaksanaan'     => $datapeng->keterangan_pelaksanaan,
            'tgl_pengabdian'            => $datapeng->tgl_pengabdian,
            'satuan_hasil'              => $datapeng->satuan_hasil,
            'jumlah_volume_dosen'       => $datapeng->jumlah_volume_dosen,
            'keterangan'                => $datapeng->keterangan,
            'bukti_file'                => $datapeng->bukti_file,
        );
        echo view("layout/dsnwraper", $data);
    }

    public function edit_action($idusul, $id)
    {
        $id_usulan = $idusul;
        // $id_usulan = $this->request->getPost('id_usulan');
        // $id_pengabdian = $this->request->getPost('id_pengabdian');
        $data = array(
            'jenis_pelaksanaan' => !empty($this->request->getPost('jenis_pelaksanaan')) ? $this->request->getPost('jenis_pelaksanaan') : NULL,
            'detail_pelaksanaan' => !empty($this->request->getPost('detail_pelaksanaan')) ? $this->request->getPost('detail_pelaksanaan') : NULL,
            'keterangan_pelaksanaan' => !empty($this->request->getPost('keterangan_pelaksanaan')) ? $this->request->getPost('keterangan_pelaksanaan') : NULL,
            'tgl_pengabdian' => !empty($this->request->getPost('tgl_pengabdian')) ? $this->request->getPost('tgl_pengabdian') : NULL,
            'satuan_hasil' => !empty($this->request->getPost('satuan_hasil')) ? $this->request->getPost('satuan_hasil') : NULL,
            'jumlah_volume_dosen' => !empty($this->request->getPost('jumlah_volume_dosen')) ? $this->request->getPost('jumlah_volume_dosen') : NULL,
            'keterangan' => !empty($this->request->getPost('keterangan')) ? $this->request->getPost('keterangan') : NULL,
            'bukti_file' => !empty($this->request->getPost('bukti_file')) ? $this->request->getPost('bukti_file') : NULL,
        );
        $this->peng->updatedata($id, $data);
        return redirect()->to(base_url('pengabdian/index/' . $id_usulan));
    }

    public function delete($idusul, $id)
    {
        $id_usulan = $idusul;
        $this->peng->hapusdata($id);
        return redirect()->to(base_url('pengabdian/index/' . $id_usulan));
    }
}
