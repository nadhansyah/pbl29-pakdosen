<?php

namespace App\Controllers;

use App\Models\DashboardModel;

class Dashboard extends BaseController
{

    protected $dsn;
    protected $usulan;
    protected $db;
    public function __construct()
    {
        $this->dash = new DashboardModel();
        $this->db = \Config\Database::connect();
    }

    // Dashboard operator
    public function index()
    {


        $data = array(
            'title' => 'Dashboard',
            'isi' => 'opt/dashboard',
            'jml_dosen' => $this->dash->getjmldosen(),
            'jml_reviewer' => $this->dash->getjmlreviewer(),
            'jml_operator' => $this->dash->getjmloperator(),
            'jml_usulan' => $this->dash->getjmlusulan(),
            // 'jml_dosen' => $this->dash->getjmldosen(),
        );
        echo view('layout/optwraper', $data);
    }

    // Dashboard Dosen
    public function dsn()
    {
        $data = array(
            'title' => 'Dashboard',
            'isi' => 'dsn/dashboard'
        );
        echo view('layout/dsnwraper', $data);
    }
}
