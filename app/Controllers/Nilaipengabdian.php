<?php

namespace App\Controllers;

use App\Models\PengabdianModel;
use App\Models\NilaipengabdianModel;
use CodeIgniter\Controller;
use App\Libraries\Datatables;
use App\Models\UsulanModel;

class Nilaipengabdian extends BaseController
{
    protected $peng;
    protected $npeng;
    protected $usul;
    protected $db;
    public function __construct()
    {
        $this->peng = new PengabdianModel();
        $this->npeng = new NilaipengabdianModel();
        $this->usul = new UsulanModel();
        $this->db = \Config\Database::connect();
    }

    public function index()
    {
        $data = array(
            'title' => 'Data Nilai Pengabdian',
            'isi' => 'rev/listpengabdian',
            'css' => '
            <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/v/bs/jszip-2.5.0/dt-1.10.16/af-2.2.2/b-1.5.1/b-colvis-1.5.1/b-flash-1.5.1/b-html5-1.5.1/b-print-1.5.1/cr-1.4.1/fc-3.2.4/fh-3.1.3/kt-2.3.2/r-2.2.1/rg-1.0.2/rr-1.2.3/sc-1.4.4/sl-1.2.5/datatables.min.css"/>
            ',
            'js' => '
            <script type="text/javascript" src="https://cdn.datatables.net/v/bs/jszip-2.5.0/dt-1.10.16/af-2.2.2/b-1.5.1/b-colvis-1.5.1/b-flash-1.5.1/b-html5-1.5.1/b-print-1.5.1/cr-1.4.1/fc-3.2.4/fh-3.1.3/kt-2.3.2/r-2.2.1/rg-1.0.2/rr-1.2.3/sc-1.4.4/sl-1.2.5/datatables.min.js"></script>
            ',
            // 'bcreate'  => "<a href=" . Base_url('pendidikan/create/' . $ld->id_usulan) . "><button type='button' class='btn btn-success btn-sm'><i class='fa fa-plus'></i> Create</button></a>"
        );
        echo view('layout/dsnwraper', $data);
    }

    public function ajaxloaddata()
    {
        $session = session();
        $nik = $session->nik_dosen;
        $params['draw'] = $_REQUEST['draw'];
        $start = $_REQUEST['start'];
        $length = $_REQUEST['length'];
        $search_value = $_REQUEST['search']['value'];
        //$data=array();
        if (!empty($search_value)) {
            $total_count = $this->db->query("SELECT pg.id_pengabdian, pg.jenis_pelaksanaan, pg.detail_pelaksanaan,
            pg.jumlah_volume_dosen, pgr.jumlah_volume_reviewer 
            FROM pengabdian pg 
            LEFT JOIN pengabdian_reviewer pgr 
            ON pgr.id_pengabdian = pg.id_pengabdian
            WHERE pg.jenis_pelaksanaan like '%" . $search_value . "%' OR pg.detail_pelaksanaan like '%" . $search_value . "%' OR pg.jumlah_volume_dosen like '%" . $search_value . "%' OR pgr.jumlah_volume_reviewer like '%" . $search_value . "%'")->getResult();
            $data = $this->db->query("SELECT pg.id_pengabdian, pg.jenis_pelaksanaan, pg.detail_pelaksanaan,
            pg.jumlah_volume_dosen, pgr.jumlah_volume_reviewer 
            FROM pengabdian pg 
            LEFT JOIN pengabdian_reviewer pgr 
            ON pgr.id_pengabdian = pg.id_pengabdian
            WHERE pg.jenis_pelaksanaan like '%" . $search_value . "%' OR pg.detail_pelaksanaan like '%" . $search_value . "%' OR pg.jumlah_volume_dosen like '%" . $search_value . "%' OR pgr.jumlah_volume_reviewer like '%" . $search_value . "%' limit $start, $length")->getResult();
        } else {
            $total_count = $this->db->query("SELECT pg.id_pengabdian, pg.jenis_pelaksanaan, pg.detail_pelaksanaan,
            pg.jumlah_volume_dosen, pgr.jumlah_volume_reviewer 
            FROM pengabdian pg 
            LEFT JOIN pengabdian_reviewer pgr 
            ON pgr.id_pengabdian = pg.id_pengabdian")->getResult();
            $data = $this->db->query("SELECT pg.id_pengabdian, pg.jenis_pelaksanaan, pg.detail_pelaksanaan,
            pg.jumlah_volume_dosen, pgr.jumlah_volume_reviewer 
            FROM pengabdian pg 
            LEFT JOIN pengabdian_reviewer pgr 
            ON pgr.id_pengabdian = pg.id_pengabdian limit $start, $length")->getResult();
        }

        $data1 = array();
        foreach ($data as $ld) {
            $row = array(
                "id_pengabdian"         => $ld->id_pengabdian,
                "jenis_pelaksanaan"          => $ld->jenis_pelaksanaan,
                "detail_pelaksanaan"         => $ld->detail_pelaksanaan,
                "jumlah_volume_dosen"       => $ld->jumlah_volume_dosen,
                "jumlah_volume_reviewer"    => $ld->jumlah_volume_reviewer,
                "action"                    => "<a href='" . Base_url('nilaipengabdian/edit/' . $ld->id_pengabdian) . "'><button type='button' class='btn btn-warning btn-xs'><i class='fa fa-pencil'></i></button></a>",
            );
            $data1[] = $row;
        }

        $json_data = array(
            "data"              => $data1,
            "recordsTotal"      => count($total_count),
            "recordsFiltered"   => count($total_count),
            "draw"              => intval($params['draw']),
        );

        echo json_encode($json_data);
    }

    public function edit($id)
    {
        $datapeng = $this->peng->get_databyid($id);
        $datanpeng = $this->npeng->get_databyid($id);
        // $datausul = $this->usul->get_databyid($id);
        $data = array(
            'title'         => 'Edit Data Nilai Pengabdian',
            'isi'           => 'rev/formpengabdian',
            'action'        => base_url('nilaipengabdian/edit_action'),
            'button'        => 'Update',
            'js'            => '
            <script src="https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/7.18.0/sweetalert2.all.min.js"></script>
            <script src="https://cdnjs.cloudflare.com/ajax/libs/core-js/2.4.1/core.js"></script>',
            'id_pengabdian'         => $id,
            'id_usulan'                 => $datapeng->id_usulan,
            'nik_dosen'                 => $datapeng->nik_dosen,
            'jenis_pelaksanaan'          => $datapeng->jenis_pelaksanaan,
            'detail_pelaksanaan'         => $datapeng->detail_pelaksanaan,
            'keterangan_pelaksanaan'     => $datapeng->keterangan_pelaksanaan,
            'tgl_pengabdian'            => $datapeng->tgl_pengabdian,
            'satuan_hasil'              => $datapeng->satuan_hasil,
            'jumlah_volume_dosen'       => $datapeng->jumlah_volume_dosen,
            'keterangan'                => $datapeng->keterangan,
            'bukti_file'                => $datapeng->bukti_file,
            'jumlah_volume_reviewer'    => !empty($datanpeng->jumlah_volume_reviewer) ? $datanpeng->jumlah_volume_reviewer : 0,
        );
        echo view("layout/dsnwraper", $data);
    }

    public function edit_action()
    {
        // $id_usulan = $this->request->getPost('id_usulan');
        $id_pengabdian = $this->request->getPost('id_pengabdian');
        $data = array(
            'jumlah_volume_reviewer' => !empty($this->request->getPost('jumlah_volume_reviewer')) ? $this->request->getPost('jumlah_volume_reviewer') : NULL,
        );
        $this->npeng->updatedata($id_pengabdian, $data);
        return redirect()->to(base_url('nilaipengabdian'));
    }
}
