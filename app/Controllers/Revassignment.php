<?php

namespace App\Controllers;

use App\Models\UsulanModel;
use App\Models\UsulanreviewerModel;
use CodeIgniter\Controller;
use App\Libraries\Datatables;

class Revassignment extends BaseController
{
    protected $usul;
    protected $usulrev;
    protected $db;
    public function __construct()
    {
        $this->usul = new UsulanModel();
        $this->usulrev = new UsulanreviewerModel();
        $this->db = \Config\Database::connect();
    }

    public function index()
    {
        $data = array(
            'title' => 'Data Penilaian Reviewer',
            'isi'   => 'opt/listrevassignment',
            'css'   => '
            <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/v/bs/jszip-2.5.0/dt-1.10.16/af-2.2.2/b-1.5.1/b-colvis-1.5.1/b-flash-1.5.1/b-html5-1.5.1/b-print-1.5.1/cr-1.4.1/fc-3.2.4/fh-3.1.3/kt-2.3.2/r-2.2.1/rg-1.0.2/rr-1.2.3/sc-1.4.4/sl-1.2.5/datatables.min.css"/>
            ',
            'js'    => '
            <script type="text/javascript" src="https://cdn.datatables.net/v/bs/jszip-2.5.0/dt-1.10.16/af-2.2.2/b-1.5.1/b-colvis-1.5.1/b-flash-1.5.1/b-html5-1.5.1/b-print-1.5.1/cr-1.4.1/fc-3.2.4/fh-3.1.3/kt-2.3.2/r-2.2.1/rg-1.0.2/rr-1.2.3/sc-1.4.4/sl-1.2.5/datatables.min.js"></script>
            ',
        );
        echo view('layout/optwraper', $data);
    }

    public function ajaxloaddata()
    {
        $params['draw'] = $_REQUEST['draw'];
        $start          = $_REQUEST['start'];
        $length         = $_REQUEST['length'];
        $search_value   = $_REQUEST['search']['value'];
        //$data=array();
        if (!empty($search_value)) {
            $total_count = $this->db->query("SELECT d.nik_dosen, d.nama, u.id_usulan, u.tgl_validasi, u.status_usulan, ur.id_usulan_reviewer, ur.nik_reviewer_1, ur.nik_reviewer_2
            FROM usulan u
            INNER JOIN dosen d
            ON d.nik_dosen = u.nik_dosen
            Left JOIN usulan_reviewer ur 
            ON u.id_usulan = ur.id_usulan 
            WHERE u.status_usulan = 'Tervalidasi' OR u.status_usulan = 'Penilaian Reviewer' OR u.status_usulan = 'Ditolak' OR u.status_usulan = 'Diterima' OR u.status_usulan = 'Diajukan ke Senat' AND u.nik_dosen like '%" . $search_value . "%' OR d.nama like '%" . $search_value . "%' OR u.tgl_validasi like '%" . $search_value . "%' OR u.status_usulan like '%" . $search_value . "%' OR ur.nik_reviewer_1 like '%" . $search_value . "%' OR ur.nik_reviewer_2 like '%" . $search_value . "%'")->getResult();
            $data = $this->db->query("SELECT d.nik_dosen, d.nama, u.id_usulan, u.tgl_validasi, u.status_usulan, ur.id_usulan_reviewer, ur.nik_reviewer_1, ur.nik_reviewer_2
            FROM usulan u
            INNER JOIN dosen d
            ON d.nik_dosen = u.nik_dosen
            Left JOIN usulan_reviewer ur 
            ON u.id_usulan = ur.id_usulan 
            WHERE u.status_usulan = 'Tervalidasi' OR u.status_usulan = 'Penilaian Reviewer' OR u.status_usulan = 'Ditolak' OR u.status_usulan = 'Diterima' OR u.status_usulan = 'Diajukan ke Senat' AND u.nik_dosen like '%" . $search_value . "%' OR d.nama like '%" . $search_value . "%' OR u.tgl_validasi like '%" . $search_value . "%' OR u.status_usulan like '%" . $search_value . "%' OR ur.nik_reviewer_1 like '%" . $search_value . "%' OR ur.nik_reviewer_2 like '%" . $search_value . "%' limit $start, $length")->getResult();
        } else {
            $total_count = $this->db->query("SELECT d.nik_dosen, d.nama, u.id_usulan, u.tgl_validasi, u.status_usulan, ur.id_usulan_reviewer, ur.nik_reviewer_1, ur.nik_reviewer_2
            FROM usulan u
            INNER JOIN dosen d
            ON d.nik_dosen = u.nik_dosen
            Left JOIN usulan_reviewer ur 
            ON u.id_usulan = ur.id_usulan 
            WHERE u.status_usulan = 'Tervalidasi' OR u.status_usulan = 'Penilaian Reviewer' OR u.status_usulan = 'Ditolak' OR u.status_usulan = 'Diterima' OR u.status_usulan = 'Diajukan ke Senat'")->getResult();
            $data = $this->db->query("SELECT d.nik_dosen, d.nama, u.id_usulan, u.tgl_validasi, u.status_usulan, ur.id_usulan_reviewer, ur.nik_reviewer_1, ur.nik_reviewer_2
            FROM usulan u
            INNER JOIN dosen d
            ON d.nik_dosen = u.nik_dosen
            Left JOIN usulan_reviewer ur 
            ON u.id_usulan = ur.id_usulan 
            WHERE u.status_usulan = 'Tervalidasi' OR u.status_usulan = 'Penilaian Reviewer' OR u.status_usulan = 'Ditolak' OR u.status_usulan = 'Diterima' OR u.status_usulan = 'Diajukan ke Senat' limit $start, $length")->getResult();
        }

        $data1 = array();
        $no = 1;
        foreach ($data as $ld) {
            $row = array(
                "no"                => $no++,
                "nik_dosen"         => $ld->nik_dosen,
                "nama"               => $ld->nama,
                "id_usulan"         => $ld->id_usulan,
                "nama"              => $ld->nama,
                "tgl_validasi"        => $ld->tgl_validasi,
                "status_usulan"     => $ld->status_usulan,
                "id_usulan_reviewer"     => $ld->id_usulan_reviewer,
                "nik_reviewer_1"     => $ld->nik_reviewer_1,
                "nik_reviewer_2"     => $ld->nik_reviewer_2,
                "dokumen"           => "<a href='" . Base_url('revassignment/detail/' . $ld->id_usulan . '/' . $ld->id_usulan_reviewer) . "'><button type='button' class='btn btn-info btn-xs'><i class='fa fa-info-circle'></i> Detail</button></a>",
                "action"            => "<a href='" . Base_url('revassignment/edit/' . $ld->id_usulan . '/' . $ld->id_usulan_reviewer) . "'><button type='button' class='btn btn-warning btn-xs'><i class='fa fa-pencil'></i></button></a>",

            );
            $data1[] = $row;
        }

        $json_data = array(
            "data"              => $data1,
            "recordsTotal"      => count($total_count),
            "recordsFiltered"   => count($total_count),
            "draw"              => intval($params['draw']),
        );

        echo json_encode($json_data);
    }

    public function create($id)
    {
        // $datausul = $this->usul->get_databyid($id);

        $data = array(
            'title'         => 'Input Data Ajuan',
            'isi'           => 'opt/formcreaterev',
            'action'        => base_url('revassignment/create_action/' . $id),
            'button'        => 'Simpan',
            'id_usulan'     => 'id_usulan',
            'id_usulan_reviewer' => 'id_usulan_reviewer',
            'listreviewer'     => $this->usulrev->get_niknamareviewer(),
            'nik_reviewer_1'  => 'nik_reviewer_1',
            'nik_reviewer_2'  => 'nik_reviewer_2',

        );
        echo view("layout/optwraper", $data);
    }

    public function create_action($id)
    {
        $data = array(
            'id_usulan'     => $id,
            'nik_reviewer_1'  => !empty($this->request->getPost('nik_reviewer_1')) ? $this->request->getPost('nik_reviewer_1') : NULL,
            'nik_reviewer_2'    => !empty($this->request->getPost('nik_reviewer_2')) ? $this->request->getPost('nik_reviewer_2') : NULL,
            // 'status_usulan' => !empty($this->request->getPost('status_usulan')) ? $this->request->getPost('status_usulan') : NULL,
        );
        $this->usulrev->insertdata($data);

        return redirect()->to(base_url('revassignment'));
    }


    public function edit($id, $id_rev)
    {
        $datausul = $this->usul->get_databyid($id);
        $datausulrev = $this->usulrev->get_databyid($id);

        // $id_rev = $datausulrev->id_usulan_reviewer;

        $data = array(
            'title'         => 'Edit Data Ajuan - Asisten Ahli',
            'isi'           => 'opt/formrevassignment',
            'action'        => base_url('revassignment/edit_action/' . $id . '/' . $id_rev),
            'button'        => 'Update',
            'js'            => '
            <script src="https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/7.18.0/sweetalert2.all.min.js"></script>
            <script src="https://cdnjs.cloudflare.com/ajax/libs/core-js/2.4.1/core.js"></script>
            <script>
                    // delete
                    $(function() {
                        $(\'.delete\').on(\'click\', function(){
                            var delete_url = $(this).attr(\'data-url\');
                            swal({
                                title: \'Are you sure?\',
                                text: \'You wont be able to revert this!\',
                                type: \'warning\',
                                showCancelButton: true,
                                confirmButtonColor: \'#3085d6\',
                                cancelButtonColor: \'#d33\',
                                confirmButtonText: \'Yes, delete it!\',
                                cancelButtonText: \'No, cancel!\',
                                confirmButtonClass: \'btn btn-success\',
                                cancelButtonClass: \'btn btn-danger\',
                                buttonsStyling: false,
                                reverseButtons: true
                            }).then((result) => {
                                if (result.value) {
                                    window.location.href = delete_url;
                                }
                            });
                        });
                    });
                </script>
            ',
            'id_usulan'         => $id,
            'listdosen'     => $this->usulrev->get_niknamadosen(),
            'nik_dosen'         => $datausul->nik_dosen,
            'tgl_validasi'      => $datausul->tgl_validasi,
            'status_usulan'      => $datausul->status_usulan,
            'listreviewer'     => $this->usulrev->get_niknamareviewer(),
            'id_usulan_reviewer'     => $datausulrev->id_usulan_reviewer,
            'nik_reviewer_1'     => $datausulrev->nik_reviewer_1,
            'nik_reviewer_2'     => $datausulrev->nik_reviewer_2,
        );
        echo view("layout/optwraper", $data);
    }



    public function edit_action($id, $id_rev)
    {
        // $id_usulan = $this->request->getPost('id_usulan');
        $data1 = array(
            'status_usulan' => !empty($this->request->getPost('status_usulan')) ? $this->request->getPost('status_usulan') : NULL,
        );
        $data2 = array(
            'nik_reviewer_1'     => !empty($this->request->getPost('nik_reviewer_1')) ? $this->request->getPost('nik_reviewer_1') : NULL,
            'nik_reviewer_2'  => !empty($this->request->getPost('nik_reviewer_2')) ? $this->request->getPost('nik_reviewer_2') : NULL,
        );
        $this->usulrev->updatedatausul($id, $data1);
        $this->usulrev->updatedatarev($id_rev, $data2);
        return redirect()->to(base_url('revassignment'));
    }

    public function delete($id)
    {
        $this->usulrev->hapusdata($id);
        return redirect()->to(base_url('revassignment'));
    }

    public function detail($id, $id_rev)
    {
        $datausul = $this->usul->get_databyid($id);
        $datausulrev = $this->usulrev->get_databyid($id);

        // $nik1 = $datausulrev->nik_reviewer_1;

        $data = array(
            'title' => 'Detail Nilai Reviewer',
            'isi'   => 'opt/detailrevassignment',
            'css'   => '
                <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/v/bs/jszip-2.5.0/dt-1.10.16/af-2.2.2/b-1.5.1/b-colvis-1.5.1/b-flash-1.5.1/b-html5-1.5.1/b-print-1.5.1/cr-1.4.1/fc-3.2.4/fh-3.1.3/kt-2.3.2/r-2.2.1/rg-1.0.2/rr-1.2.3/sc-1.4.4/sl-1.2.5/datatables.min.css"/>
                ',
            'js'    => '
            <script src="https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/7.18.0/sweetalert2.all.min.js"></script>
            <script src="https://cdnjs.cloudflare.com/ajax/libs/core-js/2.4.1/core.js"></script>
            <script>
                $(function() {
                    $(\'.ver\').on(\'click\', function(){
                        var ver_url = $(this).attr(\'data-url\');
                        swal({
                            title: \'Yakin anda telah memverifikasi seluruh data Pengajuan dan Penilaian?\',
                            text: \'Anda tidak dapat mengubah data lagi!\',
                            type: \'warning\',
                            showCancelButton: true,
                            confirmButtonColor: \'#3085d6\',
                            cancelButtonColor: \'#d33\',
                            confirmButtonText: \'Ya, yakin!\',
                            cancelButtonText: \'Tidak, kembali!\',
                            confirmButtonClass: \'btn btn-success\',
                            cancelButtonClass: \'btn btn-danger\',
                            buttonsStyling: false,
                            reverseButtons: true
                        }).then((result) => {
                            if (result.value) {
                                window.location.href = ver_url;
                            }
                        });
                    });
                });
            </script>',
            'id_usulan'           => $id,
            'nik_dosen'            => $datausul->nik_dosen,
            'id_usulan_reviewer'  => $id_rev,
            'nik_reviewer_1'       => $datausulrev->nik_reviewer_1,
            'nik_reviewer_2'       => $datausulrev->nik_reviewer_2,
            // 'listnama'       => $this->usulrev->get_nama($nik1),


        );
        echo view('layout/optwraper', $data);
    }
}
