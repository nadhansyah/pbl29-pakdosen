<?php

namespace App\Controllers;

use App\Models\UsulanModel;
use CodeIgniter\Controller;
use App\Libraries\Datatables;

class Lektor extends BaseController
{
    protected $l;
    protected $db;
    public function __construct()
    {
        $this->l = new UsulanModel();
        $this->db = \Config\Database::connect();
    }

    public function index()
    {
        $data = array(
            'title' => 'Data Ajuan - Lektor',
            'isi'   => 'opt/listlektor',
            'css'   => '
            <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/v/bs/jszip-2.5.0/dt-1.10.16/af-2.2.2/b-1.5.1/b-colvis-1.5.1/b-flash-1.5.1/b-html5-1.5.1/b-print-1.5.1/cr-1.4.1/fc-3.2.4/fh-3.1.3/kt-2.3.2/r-2.2.1/rg-1.0.2/rr-1.2.3/sc-1.4.4/sl-1.2.5/datatables.min.css"/>
            ',
            'js'    => '
            <script type="text/javascript" src="https://cdn.datatables.net/v/bs/jszip-2.5.0/dt-1.10.16/af-2.2.2/b-1.5.1/b-colvis-1.5.1/b-flash-1.5.1/b-html5-1.5.1/b-print-1.5.1/cr-1.4.1/fc-3.2.4/fh-3.1.3/kt-2.3.2/r-2.2.1/rg-1.0.2/rr-1.2.3/sc-1.4.4/sl-1.2.5/datatables.min.js"></script>
            ',
        );
        echo view('layout/optwraper', $data);
    }

    public function ajaxloaddata()
    {
        $params['draw'] = $_REQUEST['draw'];
        $start          = $_REQUEST['start'];
        $length         = $_REQUEST['length'];
        $search_value   = $_REQUEST['search']['value'];
        //$data=array();
        if (!empty($search_value)) {
            $total_count = $this->db->query("SELECT u.id_usulan, u.nik_dosen, d.nama, u.tgl_usulan, u.status_usulan 
            FROM usulan u
            INNER JOIN dosen d 
            ON u.nik_dosen = d.nik_dosen 
            WHERE u.jenis_usulan = 'Lektor' AND u.status_usulan != '' AND u.status_usulan != 'Tervalidasi' AND u.status_usulan != 'Penilaian Reviewer' AND u.nik_dosen like '%" . $search_value . "%' OR d.nama like '%" . $search_value . "%' OR u.tgl_usulan like '%" . $search_value . "%' OR u.status_usulan like '%" . $search_value . "%'")->getResult();
            $data = $this->db->query("SELECT u.id_usulan, u.nik_dosen, d.nama, u.tgl_usulan, u.status_usulan 
            FROM usulan u
            INNER JOIN dosen d 
            ON u.nik_dosen = d.nik_dosen 
            WHERE u.jenis_usulan = 'Lektor' AND u.status_usulan != '' AND u.status_usulan != 'Tervalidasi' AND u.status_usulan != 'Penilaian Reviewer' AND u.nik_dosen like '%" . $search_value . "%' OR d.nama like '%" . $search_value . "%' OR u.tgl_usulan like '%" . $search_value . "%' OR u.status_usulan like '%" . $search_value . "%' limit $start, $length")->getResult();
        } else {
            $total_count = $this->db->query("SELECT u.id_usulan, u.nik_dosen, d.nama, u.tgl_usulan, u.status_usulan 
            FROM usulan u
            INNER JOIN dosen d 
            ON u.nik_dosen = d.nik_dosen 
            WHERE u.jenis_usulan = 'Lektor' AND u.status_usulan != '' AND u.status_usulan != 'Tervalidasi' AND u.status_usulan != 'Penilaian Reviewer'")->getResult();
            $data = $this->db->query("SELECT u.id_usulan, u.nik_dosen, d.nama, u.tgl_usulan, u.status_usulan 
            FROM usulan u
            INNER JOIN dosen d 
            ON u.nik_dosen = d.nik_dosen 
            WHERE u.jenis_usulan = 'Lektor' AND u.status_usulan != '' AND u.status_usulan != 'Tervalidasi' AND u.status_usulan != 'Penilaian Reviewer' limit $start, $length")->getResult();
        }

        $data1 = array();
        $no = 1;
        foreach ($data as $ld) {
            $row = array(
                "no"                => $no++,
                "id_usulan"         => $ld->id_usulan,
                "nik_dosen"         => $ld->nik_dosen,
                "nama"              => $ld->nama,
                "tgl_usulan"        => $ld->tgl_usulan,
                "status_usulan"     => $ld->status_usulan,
                "dokumen"           => "<a href='" . Base_url('lektor/detail/' . $ld->id_usulan) . "'><button type='button' class='btn btn-info btn-xs'><i class='fa fa-info-circle'></i> Detail</button></a>",
                "action"            => "<a href='" . Base_url('lektor/edit/' . $ld->id_usulan) . "'><button type='button' class='btn btn-warning btn-xs'><i class='fa fa-pencil'></i></button></a>",

            );
            $data1[] = $row;
        }

        $json_data = array(
            "data"              => $data1,
            "recordsTotal"      => count($total_count),
            "recordsFiltered"   => count($total_count),
            "draw"              => intval($params['draw']),
        );

        echo json_encode($json_data);
    }

    public function create()
    {
        $data = array(
            'title'         => 'Input Data Ajuan - Lektor',
            'isi'           => 'opt/formlektor',
            'action'        => 'create_action',
            'button'        => 'Simpan',
            'id_usulan'     => 'id_usulan',
            'listdosen'     => $this->l->get_ldsn(),
            'nik_dosen'     => 'nik_dosen',
            'jenis_usulan'  => 'jenis_usulan',
            // 'tgl_usulan'    => 'tgl_usulan',
            'status_usulan' => 'status_usulan'
        );
        echo view("layout/optwraper", $data);
    }

    public function create_action()
    {
        $data = array(
            'nik_dosen'     => !empty($this->request->getPost('nik_dosen')) ? $this->request->getPost('nik_dosen') : NULL,
            'jenis_usulan'  => !empty($this->request->getPost('jenis_usulan')) ? $this->request->getPost('jenis_usulan') : NULL,
            'tgl_usulan'    => !empty($this->request->getPost('tgl_usulan')) ? $this->request->getPost('tgl_usulan') : NULL,
            'status_usulan' => !empty($this->request->getPost('status_usulan')) ? $this->request->getPost('status_usulan') : NULL,
        );
        $this->l->insertdata($data);
        return redirect()->to(base_url('lektor'));
    }

    public function edit($id)
    {
        $datal = $this->l->get_databyid($id);
        $data = array(
            'title'         => 'Edit Data Ajuan - Lektor',
            'isi'           => 'opt/formlektor',
            'action'        => base_url('lektor/edit_action/' . $id),
            'button'        => 'Update',
            'js'            => '
            <script src="https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/7.18.0/sweetalert2.all.min.js"></script>
            <script src="https://cdnjs.cloudflare.com/ajax/libs/core-js/2.4.1/core.js"></script>
            <script>
                    // delete
                    $(function() {
                        $(\'.delete\').on(\'click\', function(){
                            var delete_url = $(this).attr(\'data-url\');
                            swal({
                                title: \'Are you sure?\',
                                text: \'You wont be able to revert this!\',
                                type: \'warning\',
                                showCancelButton: true,
                                confirmButtonColor: \'#3085d6\',
                                cancelButtonColor: \'#d33\',
                                confirmButtonText: \'Yes, delete it!\',
                                cancelButtonText: \'No, cancel!\',
                                confirmButtonClass: \'btn btn-success\',
                                cancelButtonClass: \'btn btn-danger\',
                                buttonsStyling: false,
                                reverseButtons: true
                            }).then((result) => {
                                if (result.value) {
                                    window.location.href = delete_url;
                                }
                            });
                        });
                    });
                </script>
            ',
            'id_usulan'       => $id,
            'listdosen'     => $this->l->get_ldsn(),
            'nik_dosen'     => $datal->nik_dosen,
            'jenis_usulan'      => $datal->jenis_usulan,
            // 'tgl_usulan'        => $datal->tgl_usulan,
            'status_usulan'        => $datal->status_usulan,
        );
        echo view("layout/optwraper", $data);
    }

    public function edit_action($id)
    {
        // $id_usulan = $this->request->getPost('id_usulan');
        $data = array(
            // 'nik_dosen' => !empty($this->request->getPost('nik_dosen')) ? $this->request->getPost('nik_dosen') : NULL,
            // 'jenis_usulan' => !empty($this->request->getPost('jenis_usulan')) ? $this->request->getPost('jenis_usulan') : NULL,
            // 'tgl_usulan' => !empty($this->request->getPost('tgl_usulan')) ? $this->request->getPost('tgl_usulan') : NULL,
            'status_usulan' => !empty($this->request->getPost('status_usulan')) ? $this->request->getPost('status_usulan') : NULL,
        );
        $this->l->updatedata($id, $data);
        return redirect()->to(base_url('lektor'));
    }

    public function delete($id)
    {
        $this->l->hapusdata($id);
        return redirect()->to(base_url('lektor'));
    }

    public function detail($id)
    {
        $datausul = $this->l->get_databyid($id);
        // return redirect()->to(base_url('detail'));
        // echo view("layout/dsnwraper", $data);

        $data = array(
            'title' => 'Detail Ajuan',
            'isi'   => 'opt/detailajuan',
            'css'   => '
                <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/v/bs/jszip-2.5.0/dt-1.10.16/af-2.2.2/b-1.5.1/b-colvis-1.5.1/b-flash-1.5.1/b-html5-1.5.1/b-print-1.5.1/cr-1.4.1/fc-3.2.4/fh-3.1.3/kt-2.3.2/r-2.2.1/rg-1.0.2/rr-1.2.3/sc-1.4.4/sl-1.2.5/datatables.min.css"/>
                ',
            'js'    => '
            <script src="https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/7.18.0/sweetalert2.all.min.js"></script>
            <script src="https://cdnjs.cloudflare.com/ajax/libs/core-js/2.4.1/core.js"></script>
            <script>
                $(function() {
                    $(\'.val\').on(\'click\', function(){
                        var val_url = $(this).attr(\'data-url\');
                        swal({
                            title: \'Yakin seluruh data ajuan telah tervalidasi?\',
                            text: \'Anda tidak dapat mengubah data lagi!\',
                            type: \'warning\',
                            showCancelButton: true,
                            confirmButtonColor: \'#3085d6\',
                            cancelButtonColor: \'#d33\',
                            confirmButtonText: \'Ya, yakin!\',
                            cancelButtonText: \'Tidak, kembali!\',
                            confirmButtonClass: \'btn btn-success\',
                            cancelButtonClass: \'btn btn-danger\',
                            buttonsStyling: false,
                            reverseButtons: true
                        }).then((result) => {
                            if (result.value) {
                                window.location.href = val_url;
                            }
                        });
                    });
                });
            </script>',
            'id_usulan'         => $id,
            'nik_dosen'         => $datausul->nik_dosen,
        );
        echo view('layout/optwraper', $data);
    }
}
