<?php

namespace App\Controllers;

use App\Models\PenelitianModel;
use App\Models\NilaipenelitianModel;
use CodeIgniter\Controller;
use App\Libraries\Datatables;
use App\Models\UsulanModel;

class Nilaipenelitian extends BaseController
{
    protected $penl;
    protected $npenl;
    protected $usul;
    protected $db;
    public function __construct()
    {
        $this->penl = new PenelitianModel();
        $this->npenl = new NilaipenelitianModel();
        $this->usul = new UsulanModel();
        $this->db = \Config\Database::connect();
    }

    public function index()
    {
        $data = array(
            'title' => 'Data Nilai Penelitian',
            'isi' => 'rev/listpenelitian',
            'css' => '
            <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/v/bs/jszip-2.5.0/dt-1.10.16/af-2.2.2/b-1.5.1/b-colvis-1.5.1/b-flash-1.5.1/b-html5-1.5.1/b-print-1.5.1/cr-1.4.1/fc-3.2.4/fh-3.1.3/kt-2.3.2/r-2.2.1/rg-1.0.2/rr-1.2.3/sc-1.4.4/sl-1.2.5/datatables.min.css"/>
            ',
            'js' => '
            <script type="text/javascript" src="https://cdn.datatables.net/v/bs/jszip-2.5.0/dt-1.10.16/af-2.2.2/b-1.5.1/b-colvis-1.5.1/b-flash-1.5.1/b-html5-1.5.1/b-print-1.5.1/cr-1.4.1/fc-3.2.4/fh-3.1.3/kt-2.3.2/r-2.2.1/rg-1.0.2/rr-1.2.3/sc-1.4.4/sl-1.2.5/datatables.min.js"></script>
            ',
            // 'bcreate'  => "<a href=" . Base_url('pendidikan/create/' . $ld->id_usulan) . "><button type='button' class='btn btn-success btn-sm'><i class='fa fa-plus'></i> Create</button></a>"
        );
        echo view('layout/dsnwraper', $data);
    }

    public function ajaxloaddata()
    {
        $session = session();
        $nik = $session->nik_dosen;
        $params['draw'] = $_REQUEST['draw'];
        $start = $_REQUEST['start'];
        $length = $_REQUEST['length'];
        $search_value = $_REQUEST['search']['value'];
        //$data=array();
        if (!empty($search_value)) {
            $total_count = $this->db->query("SELECT pl.id_penelitian, pl.jenis_pelaksanaan, pl.jenis_hasil_pelaksanaan, pl.detail_pelaksanaan,
            pl.jumlah_volume_dosen, plr.jumlah_volume_reviewer 
            FROM penelitian pl 
            LEFT JOIN penelitian_reviewer plr 
            ON plr.id_penelitian = pl.id_penelitian
            WHERE pl.jenis_pelaksanaan like '%" . $search_value . "%' OR pl.jenis_hasil_pelaksanaan like '%" . $search_value . "%' OR pl.detail_pelaksanaan like '%" . $search_value . "%' OR pl.jumlah_volume_dosen like '%" . $search_value . "%' OR plr.jumlah_volume_reviewer like '%" . $search_value . "%'")->getResult();
            $data = $this->db->query("SELECT pl.id_penelitian, pl.jenis_pelaksanaan, pl.jenis_hasil_pelaksanaan, pl.detail_pelaksanaan,
            pl.jumlah_volume_dosen, plr.jumlah_volume_reviewer 
            FROM penelitian pl 
            LEFT JOIN penelitian_reviewer plr 
            ON plr.id_penelitian = pl.id_penelitian
            WHERE pl.jenis_pelaksanaan like '%" . $search_value . "%' OR pl.jenis_pelaksanaan like '%" . $search_value . "%' OR pl.detail_pelaksanaan like '%" . $search_value . "%' OR pl.jumlah_volume_dosen like '%" . $search_value . "%' OR plr.jumlah_volume_reviewer like '%" . $search_value . "%' limit $start, $length")->getResult();
        } else {
            $total_count = $this->db->query("SELECT pl.id_penelitian, pl.jenis_pelaksanaan, pl.jenis_hasil_pelaksanaan, pl.detail_pelaksanaan,
            pl.jumlah_volume_dosen, plr.jumlah_volume_reviewer 
            FROM penelitian pl 
            LEFT JOIN penelitian_reviewer plr 
            ON plr.id_penelitian = pl.id_penelitian")->getResult();
            $data = $this->db->query("SELECT pl.id_penelitian, pl.jenis_pelaksanaan, pl.jenis_hasil_pelaksanaan, pl.detail_pelaksanaan,
            pl.jumlah_volume_dosen, plr.jumlah_volume_reviewer 
            FROM penelitian pl 
            LEFT JOIN penelitian_reviewer plr 
            ON plr.id_penelitian = pl.id_penelitian limit $start, $length")->getResult();
        }

        $data1 = array();
        foreach ($data as $ld) {
            $row = array(
                "id_penelitian"         => $ld->id_penelitian,
                "jenis_pelaksanaan"          => $ld->jenis_pelaksanaan,
                "jenis_hasil_pelaksanaan"   => $ld->jenis_hasil_pelaksanaan,
                "detail_pelaksanaan"         => $ld->detail_pelaksanaan,
                "jumlah_volume_dosen"       => $ld->jumlah_volume_dosen,
                "jumlah_volume_reviewer"    => $ld->jumlah_volume_reviewer,
                "action"                    => "<a href='" . Base_url('nilaipenelitian/edit/' . $ld->id_penelitian) . "'><button type='button' class='btn btn-warning btn-xs'><i class='fa fa-pencil'></i></button></a>",
            );
            $data1[] = $row;
        }

        $json_data = array(
            "data"              => $data1,
            "recordsTotal"      => count($total_count),
            "recordsFiltered"   => count($total_count),
            "draw"              => intval($params['draw']),
        );

        echo json_encode($json_data);
    }

    public function edit($id)
    {
        $datapenl = $this->penl->get_databyid($id);
        $datanpenl = $this->npenl->get_databyid($id);
        // $datausul = $this->usul->get_databyid($id);
        $data = array(
            'title'         => 'Edit Data Nilai Penelitian',
            'isi'           => 'rev/formpenelitian',
            'action'        => base_url('nilaipenelitian/edit_action'),
            'button'        => 'Update',
            'js'            => '
            <script src="https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/7.18.0/sweetalert2.all.min.js"></script>
            <script src="https://cdnjs.cloudflare.com/ajax/libs/core-js/2.4.1/core.js"></script>',
            'id_penelitian'            => $id,
            'id_usulan'                 => $datapenl->id_usulan,
            'nik_dosen'                 => $datapenl->nik_dosen,
            'jenis_pelaksanaan'          => $datapenl->jenis_pelaksanaan,
            'jenis_hasil_pelaksanaan'    => $datapenl->jenis_hasil_pelaksanaan,
            'detail_pelaksanaan'         => $datapenl->detail_pelaksanaan,
            'keterangan_pelaksanaan'     => $datapenl->keterangan_pelaksanaan,
            'tgl_penelitian'            => $datapenl->tgl_penelitian,
            'satuan_hasil'              => $datapenl->satuan_hasil,
            'jumlah_volume_dosen'       => $datapenl->jumlah_volume_dosen,
            'keterangan'                => $datapenl->keterangan,
            'bukti_file'                => $datapenl->bukti_file,
            'jumlah_volume_reviewer'    => !empty($datanpenl->jumlah_volume_reviewer) ? $datanpenl->jumlah_volume_reviewer : 0,
        );
        echo view("layout/dsnwraper", $data);
    }

    public function edit_action()
    {
        // $id_usulan = $this->request->getPost('id_usulan');
        $id_penelitian = $this->request->getPost('id_penelitian');
        $data = array(
            'jumlah_volume_reviewer' => !empty($this->request->getPost('jumlah_volume_reviewer')) ? $this->request->getPost('jumlah_volume_reviewer') : NULL,
        );
        $this->npenl->updatedata($id_penelitian, $data);
        return redirect()->to(base_url('nilaipenelitian'));
    }
}
