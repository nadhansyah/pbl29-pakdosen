<?php

namespace App\Controllers;

use App\Models\DosenModel;
use CodeIgniter\Controller;
use App\Libraries\Datatables;

class Dsn extends BaseController
{
    protected $dsn;
    protected $db;
    public function __construct()
    {
        $this->dsn = new DosenModel();
        $this->db = \Config\Database::connect();
    }

    public function index()
    {
        $data = array(
            'title' => 'Daftar Dosen',
            'isi' => 'dsn/listdosen',
            'css' => '
            <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/v/bs/jszip-2.5.0/dt-1.10.16/af-2.2.2/b-1.5.1/b-colvis-1.5.1/b-flash-1.5.1/b-html5-1.5.1/b-print-1.5.1/cr-1.4.1/fc-3.2.4/fh-3.1.3/kt-2.3.2/r-2.2.1/rg-1.0.2/rr-1.2.3/sc-1.4.4/sl-1.2.5/datatables.min.css"/>
            ',
            'js' => '
            <script type="text/javascript" src="https://cdn.datatables.net/v/bs/jszip-2.5.0/dt-1.10.16/af-2.2.2/b-1.5.1/b-colvis-1.5.1/b-flash-1.5.1/b-html5-1.5.1/b-print-1.5.1/cr-1.4.1/fc-3.2.4/fh-3.1.3/kt-2.3.2/r-2.2.1/rg-1.0.2/rr-1.2.3/sc-1.4.4/sl-1.2.5/datatables.min.js"></script>
            ',
        );
        echo view('layout/dsnwraper', $data);
    }

    public function ajaxloaddata()
    {
        $params['draw'] = $_REQUEST['draw'];
        $start = $_REQUEST['start'];
        $length = $_REQUEST['length'];
        $search_value = $_REQUEST['search']['value'];
        //$data=array();
        if (!empty($search_value)) {
            $total_count = $this->db->query("SELECT * from dosen WHERE nik_dosen like '%" . $search_value . "%' OR nama like '%" . $search_value . "%' OR email like '%" . $search_value . "%' OR no_hp like '%" . $search_value . "%' OR role like '%" . $search_value . "%'")->getResult();
            $data = $this->db->query("SELECT * from dosen WHERE nik_dosen like '%" . $search_value . "%' OR nama like '%" . $search_value . "%' OR email like '%" . $search_value . "%' OR no_hp like '%" . $search_value . "%' OR role like '%" . $search_value . "%' limit $start, $length")->getResult();
        } else {
            $total_count = $this->db->query("SELECT * from dosen")->getResult();
            $data = $this->db->query("SELECT * from dosen limit $start, $length")->getResult();
        }

        $data1 = array();
        foreach ($data as $ld) {
            $row = array(
                "nik_dosen"     => $ld->nik_dosen,
                "nama"          => $ld->nama,
                "email"         => $ld->email,
                "no_hp"         => $ld->no_hp,
                "role"          => $ld->role,

            );
            $data1[] = $row;
        }

        $json_data = array(
            "data"              => $data1,
            "recordsTotal"      => count($total_count),
            "recordsFiltered"   => count($total_count),
            "draw"              => intval($params['draw']),
        );

        echo json_encode($json_data);
    }


    // Logout
    public function logout()
    {
        $session = session();
        $session->destroy();
        return redirect()->to('login');
    }

    //profile dosen - opt - rev
    public function profile($nik)
    {
        $datadsn = $this->dsn->get_profiledsn($nik);
        $data = array(
            'title' => 'Profile',
            'isi' => 'opt/profilepengaju',
            'nik_dosen' => $datadsn->nik_dosen,
            'nama' => $datadsn->nama,
            'email' => $datadsn->email,
            'no_hp' => $datadsn->no_hp,
            'tempat_lahir' => $datadsn->tempat_lahir,
            'tgl_lahir' => $datadsn->tgl_lahir,
            'jenis_kelamin' => $datadsn->jenis_kelamin,
            'jurusan' => $datadsn->jurusan,
            'prodi' => $datadsn->prodi,
            'jenjang_pendidikan' => $datadsn->jenjang_pendidikan,
            'jabatan_fungsional' => $datadsn->jabatan_fungsional,
            'bidang_ilmu' => $datadsn->bidang_ilmu,
            'foto_profile' => $datadsn->foto_profile,
            'nidn' => $datadsn->nidn,
            'nip' => $datadsn->nip,
            'pangkat_golongan' => $datadsn->pangkat_golongan,
            'masa_gol_lama' => $datadsn->masa_gol_lama,
            'masa_gol_baru' => $datadsn->masa_gol_baru,

        );
        echo view('layout/optwraper', $data);
    }
}
