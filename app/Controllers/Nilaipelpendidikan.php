<?php

namespace App\Controllers;

use App\Models\PelpendidikanModel;
use App\Models\NilaipelpendidikanModel;
use CodeIgniter\Controller;
use App\Libraries\Datatables;
use App\Models\UsulanModel;

class Nilaipelpendidikan extends BaseController
{
    protected $pelpend;
    protected $npelpend;
    protected $usul;
    protected $db;
    public function __construct()
    {
        $this->pelpend = new PelpendidikanModel();
        $this->npelpend = new NilaipelpendidikanModel();
        $this->usul = new UsulanModel();
        $this->db = \Config\Database::connect();
    }

    public function index($id)
    {
        $data = array(
            'title' => 'Data Nilai Pelaksanaan Pendidikan',
            'isi' => 'rev/listpelpendidikan',
            'css' => '
            <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/v/bs/jszip-2.5.0/dt-1.10.16/af-2.2.2/b-1.5.1/b-colvis-1.5.1/b-flash-1.5.1/b-html5-1.5.1/b-print-1.5.1/cr-1.4.1/fc-3.2.4/fh-3.1.3/kt-2.3.2/r-2.2.1/rg-1.0.2/rr-1.2.3/sc-1.4.4/sl-1.2.5/datatables.min.css"/>
            ',
            'js' => '
            <script type="text/javascript" src="https://cdn.datatables.net/v/bs/jszip-2.5.0/dt-1.10.16/af-2.2.2/b-1.5.1/b-colvis-1.5.1/b-flash-1.5.1/b-html5-1.5.1/b-print-1.5.1/cr-1.4.1/fc-3.2.4/fh-3.1.3/kt-2.3.2/r-2.2.1/rg-1.0.2/rr-1.2.3/sc-1.4.4/sl-1.2.5/datatables.min.js"></script>
            ',
            'idusul' => $id,
            // 'bcreate'  => "<a href=" . Base_url('pendidikan/create/' . $ld->id_usulan) . "><button type='button' class='btn btn-success btn-sm'><i class='fa fa-plus'></i> Create</button></a>"
        );
        echo view('layout/dsnwraper', $data);
    }

    public function ajaxloaddata($id)
    {
        $session = session();
        $nik = $session->nik_dosen;
        $params['draw'] = $_REQUEST['draw'];
        $start = $_REQUEST['start'];
        $length = $_REQUEST['length'];
        $search_value = $_REQUEST['search']['value'];
        //$data=array();
        if (!empty($search_value)) {
            $total_count = $this->db->query("SELECT pp.id_pel_pendidikan, pp.jenis_pelaksanaan, pp.detail_pelaksanaan,
            pp.jumlah_volume_dosen, ppr.jumlah_volume_reviewer 
            FROM pel_pendidikan pp 
            LEFT JOIN pel_pendidikan_reviewer ppr 
            ON ppr.id_pel_pendidikan = pp.id_pel_pendidikan
             WHERE id_usulan = '" . $id . "' AND pp.jenis_pelaksanaan like '%" . $search_value . "%' OR pp.detail_pelaksanaan like '%" . $search_value . "%' OR pp.jumlah_volume_dosen like '%" . $search_value . "%' OR ppr.jumlah_volume_reviewer like '%" . $search_value . "%'")->getResult();
            $data = $this->db->query("SELECT pp.id_pel_pendidikan, pp.jenis_pelaksanaan, pp.detail_pelaksanaan,
            pp.jumlah_volume_dosen, ppr.jumlah_volume_reviewer 
            FROM pel_pendidikan pp 
            LEFT JOIN pel_pendidikan_reviewer ppr 
            ON ppr.id_pel_pendidikan = pp.id_pel_pendidikan
             WHERE id_usulan = '" . $id . "' AND pp.jenis_pelaksanaan like '%" . $search_value . "%' OR pp.detail_pelaksanaan like '%" . $search_value . "%' OR pp.jumlah_volume_dosen like '%" . $search_value . "%' OR ppr.jumlah_volume_reviewer like '%" . $search_value . "%' limit $start, $length")->getResult();
        } else {
            $total_count = $this->db->query("SELECT pp.id_pel_pendidikan, pp.jenis_pelaksanaan, pp.detail_pelaksanaan,
            pp.jumlah_volume_dosen, ppr.jumlah_volume_reviewer 
            FROM pel_pendidikan pp 
            LEFT JOIN pel_pendidikan_reviewer ppr 
            ON ppr.id_pel_pendidikan = pp.id_pel_pendidikan
            WHERE id_usulan = '" . $id . "'")->getResult();
            $data = $this->db->query("SELECT pp.id_pel_pendidikan, pp.jenis_pelaksanaan, pp.detail_pelaksanaan,
            pp.jumlah_volume_dosen, ppr.jumlah_volume_reviewer 
            FROM pel_pendidikan pp 
            LEFT JOIN pel_pendidikan_reviewer ppr 
            ON ppr.id_pel_pendidikan = pp.id_pel_pendidikan 
            WHERE id_usulan = '" . $id . "'limit $start, $length")->getResult();
        }

        $data1 = array();
        $no = 1;
        foreach ($data as $ld) {
            $row = array(
                "no"                        => $no++,
                "id_pel_pendidikan"         => $ld->id_pel_pendidikan,
                "jenis_pelaksanaan"          => $ld->jenis_pelaksanaan,
                "detail_pelaksanaan"         => $ld->detail_pelaksanaan,
                "jumlah_volume_dosen"       => $ld->jumlah_volume_dosen,
                "jumlah_volume_reviewer"    => $ld->jumlah_volume_reviewer,
                "action"                    => "<a href='" . Base_url('nilaipelpendidikan/edit/' . $ld->id_pel_pendidikan) . "'><button type='button' class='btn btn-warning btn-xs'><i class='fa fa-pencil'></i></button></a>",
            );
            $data1[] = $row;
        }

        $json_data = array(
            "data"              => $data1,
            "recordsTotal"      => count($total_count),
            "recordsFiltered"   => count($total_count),
            "draw"              => intval($params['draw']),
        );

        echo json_encode($json_data);
    }

    public function edit($id)
    {
        $id_pelpend = $id;
        $datapelpend = $this->pelpend->get_databyid($id);
        $idusul = $datapelpend->id_usulan;
        $datanpelpend = $this->npelpend->get_databyid($id);
        // $datausul = $this->usul->get_databyid($id);
        if ($datanpelpend != NULL || $datanpelpend != "") {
            $data = array(
                'title'         => 'Edit Data Nilai Pelaksanaan Pendidikan',
                'isi'           => 'rev/formpelpendidikan',
                'action'        => base_url('nilaipelpendidikan/edit_action/edit_action/' . $idusul . '/' . $id_pelpend),
                'button'        => 'Update',
                'js'            => '
            <script src="https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/7.18.0/sweetalert2.all.min.js"></script>
            <script src="https://cdnjs.cloudflare.com/ajax/libs/core-js/2.4.1/core.js"></script>',
                'id_pel_pendidikan'         => $id,
                'id_usulan'                 => $datapelpend->id_usulan,
                'nik_dosen'                 => $datapelpend->nik_dosen,
                'jenis_pelaksanaan'          => $datapelpend->jenis_pelaksanaan,
                'detail_pelaksanaan'         => $datapelpend->detail_pelaksanaan,
                'keterangan_pelaksanaan'     => $datapelpend->keterangan_pelaksanaan,
                'tgl_pelaksanaan'            => $datapelpend->tgl_pelaksanaan,
                'semester_waktu'            => $datapelpend->semester_waktu,
                'satuan_hasil'              => $datapelpend->satuan_hasil,
                'jumlah_volume_dosen'       => $datapelpend->jumlah_volume_dosen,
                'keterangan'                => $datapelpend->keterangan,
                'bukti_file'                => $datapelpend->bukti_file,
                'jumlah_volume_reviewer'    => $datanpelpend->jumlah_volume_dosen,
            );
        } else {
            $data = array(
                'title'         => 'Edit Data Nilai Pelaksanaan Pendidikan',
                'isi'           => 'rev/formpelpendidikan',
                'action'        => base_url('nilaipelpendidikan/create_action/edit_action/' . $idusul . '/' . $id_pelpend),
                'button'        => 'Update',
                'js'            => '
            <script src="https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/7.18.0/sweetalert2.all.min.js"></script>
            <script src="https://cdnjs.cloudflare.com/ajax/libs/core-js/2.4.1/core.js"></script>',
                'id_pel_pendidikan'         => $id,
                'id_usulan'                 => $datapelpend->id_usulan,
                'nik_dosen'                 => $datapelpend->nik_dosen,
                'jenis_pelaksanaan'          => $datapelpend->jenis_pelaksanaan,
                'detail_pelaksanaan'         => $datapelpend->detail_pelaksanaan,
                'keterangan_pelaksanaan'     => $datapelpend->keterangan_pelaksanaan,
                'tgl_pelaksanaan'            => $datapelpend->tgl_pelaksanaan,
                'semester_waktu'            => $datapelpend->semester_waktu,
                'satuan_hasil'              => $datapelpend->satuan_hasil,
                'jumlah_volume_dosen'       => $datapelpend->jumlah_volume_dosen,
                'keterangan'                => $datapelpend->keterangan,
                'bukti_file'                => $datapelpend->bukti_file,

                'id_pendidikan'    => 'id_pendidikan',
                'nik_reviewer' => 'nik_reviewer',
                'jumlah_volume_reviewer'    => 'jumlah_volume_reviewer',
            );
        }
        echo view("layout/dsnwraper", $data);
    }

    public function create_action($idusul, $id)
    {
        $session = session();
        $nik = $session->nik_dosen;
        $data = array(
            'id_pel_pendidikan'             => $id,
            'nik_reviewer'             => $nik,
            'jumlah_volume_reviewer' => !empty($this->request->getPost('jumlah_volume_reviewer')) ? $this->request->getPost('jumlah_volume_reviewer') : NULL,
        );

        $this->npend->insertdata($data);
        return redirect()->to(base_url('nilaipelpendidikan/index/' . $idusul));
    }

    public function edit_action($idusul, $id)
    {
        // $id_usulan = $this->request->getPost('id_usulan');
        $data = array(
            'jumlah_volume_reviewer' => !empty($this->request->getPost('jumlah_volume_reviewer')) ? $this->request->getPost('jumlah_volume_reviewer') : NULL,
        );
        $this->npelpend->updatedata($id, $data);
        return redirect()->to(base_url('nilaipelpendidikan/index/' . $idusul));
    }
}
