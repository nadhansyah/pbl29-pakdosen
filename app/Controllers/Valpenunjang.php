<?php

namespace App\Controllers;

use App\Models\PenunjangModel;
use CodeIgniter\Controller;
use App\Libraries\Datatables;
use App\Models\UsulanModel;

class Valpenunjang extends BaseController
{
    protected $penj;
    protected $usul;
    protected $db;
    public function __construct()
    {
        $this->penj = new PenunjangModel();
        $this->usul = new UsulanModel();
        $this->db = \Config\Database::connect();
    }

    public function index($id)
    {
        $data = array(
            'title' => 'Data Ajuan Penunjang Tugas Dosen',
            'isi' => 'opt/listpenunjang',
            'css' => '
            <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/v/bs/jszip-2.5.0/dt-1.10.16/af-2.2.2/b-1.5.1/b-colvis-1.5.1/b-flash-1.5.1/b-html5-1.5.1/b-print-1.5.1/cr-1.4.1/fc-3.2.4/fh-3.1.3/kt-2.3.2/r-2.2.1/rg-1.0.2/rr-1.2.3/sc-1.4.4/sl-1.2.5/datatables.min.css"/>
            ',
            'js' => '
            <script type="text/javascript" src="https://cdn.datatables.net/v/bs/jszip-2.5.0/dt-1.10.16/af-2.2.2/b-1.5.1/b-colvis-1.5.1/b-flash-1.5.1/b-html5-1.5.1/b-print-1.5.1/cr-1.4.1/fc-3.2.4/fh-3.1.3/kt-2.3.2/r-2.2.1/rg-1.0.2/rr-1.2.3/sc-1.4.4/sl-1.2.5/datatables.min.js"></script>
            ',
            'idusul' => $id,
        );
        echo view('layout/optwraper', $data);
    }

    public function ajaxloaddata($id)
    {
        // $datapend = $this->usul->get_databyid($id);
        // $idusul = $datapend->id_usulan;
        $params['draw'] = $_REQUEST['draw'];
        $start = $_REQUEST['start'];
        $length = $_REQUEST['length'];
        $search_value = $_REQUEST['search']['value'];
        //$data=array();
        if (!empty($search_value)) {
            $total_count = $this->db->query("SELECT id_usulan, id_penunjang, jenis_pelaksanaan, detail_pelaksanaan, jumlah_volume_dosen
            from penunjang where id_usulan = '" . $id . "' 
            AND jenis_pelaksanaan like '%" . $search_value . "%' OR detail_pelaksanaan like '%" . $search_value . "%' OR jumlah_volume_dosen like '%" . $search_value . "%'")->getResult();
            $data = $this->db->query("SELECT id_usulan, id_penunjang, jenis_pelaksanaan, detail_pelaksanaan, jumlah_volume_dosen
            from penunjang where id_usulan = '" . $id . "'
            AND jenis_pelaksanaan like '%" . $search_value . "%' OR detail_pelaksanaan like '%" . $search_value . "%' OR jumlah_volume_dosen like '%" . $search_value . "%' limit $start, $length")->getResult();
        } else {
            $total_count = $this->db->query("SELECT id_usulan, id_penunjang, jenis_pelaksanaan, detail_pelaksanaan, jumlah_volume_dosen
            from penunjang where id_usulan = '" . $id . "'")->getResult();
            $data = $this->db->query("SELECT id_usulan, id_penunjang, jenis_pelaksanaan, detail_pelaksanaan, jumlah_volume_dosen
            from penunjang where id_usulan = '" . $id . "' limit $start, $length")->getResult();
        }

        $data1 = array();
        $no = 1;
        foreach ($data as $ld) {
            $row = array(
                "no"                        => $no++,
                "id_usulan"                 => $ld->id_usulan,
                "id_penunjang"            => $ld->id_penunjang,
                "jenis_pelaksanaan"          => $ld->jenis_pelaksanaan,
                "detail_pelaksanaan"         => $ld->detail_pelaksanaan,
                "jumlah_volume_dosen"       => $ld->jumlah_volume_dosen,
                "action"                    => "<a href='" . Base_url('valpenunjang/edit/' . $ld->id_penunjang) . "'><button type='button' class='btn btn-warning btn-xs'><i class='fa fa-pencil'></i></button></a>",
            );
            $data1[] = $row;
        }

        $json_data = array(
            "data"              => $data1,
            "recordsTotal"      => count($total_count),
            "recordsFiltered"   => count($total_count),
            "draw"              => intval($params['draw']),
        );

        echo json_encode($json_data);
    }

    public function edit($id)
    {
        $datapenj = $this->penj->get_databyid($id);
        $idusul = $datapenj->id_usulan;
        // $datausul = $this->usul->get_databyid($id);
        $data = array(
            'title'         => 'Edit Data Penunjang',
            'isi'           => 'opt/formpenunjang',
            'action'        => base_url('valpenunjang/edit_action/' . $idusul . '/' . $id),
            'button'        => 'Update',
            'js'            => '
            <script src="https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/7.18.0/sweetalert2.all.min.js"></script>
            <script src="https://cdnjs.cloudflare.com/ajax/libs/core-js/2.4.1/core.js"></script>
            <script>
                    // delete
                    $(function() {
                        $(\'.delete\').on(\'click\', function(){
                            var delete_url = $(this).attr(\'data-url\');
                            swal({
                                title: \'Are you sure?\',
                                text: \'You wont be able to revert this!\',
                                type: \'warning\',
                                showCancelButton: true,
                                confirmButtonColor: \'#3085d6\',
                                cancelButtonColor: \'#d33\',
                                confirmButtonText: \'Yes, delete it!\',
                                cancelButtonText: \'No, cancel!\',
                                confirmButtonClass: \'btn btn-success\',
                                cancelButtonClass: \'btn btn-danger\',
                                buttonsStyling: false,
                                reverseButtons: true
                            }).then((result) => {
                                if (result.value) {
                                    window.location.href = delete_url;
                                }
                            });
                        });
                    });
                </script>
            ',
            'id_penunjang'             => $id,
            'id_usulan'                 => $datapenj->id_usulan,
            'nik_dosen'                 => $datapenj->nik_dosen,
            'jenis_pelaksanaan'          => $datapenj->jenis_pelaksanaan,
            'detail_pelaksanaan'         => $datapenj->detail_pelaksanaan,
            'keterangan_pelaksanaan'     => $datapenj->keterangan_pelaksanaan,
            'tgl_penunjang'            => $datapenj->tgl_penunjang,
            'satuan_hasil'            => $datapenj->satuan_hasil,
            'jumlah_volume_dosen'       => $datapenj->jumlah_volume_dosen,
            'keterangan'                => $datapenj->keterangan,
            'bukti_file'                => $datapenj->bukti_file,
        );
        echo view("layout/optwraper", $data);
    }
    public function edit_action($idusul, $id)
    {
        $id_usulan = $idusul;
        $data = array(
            'jenis_pelaksanaan' => !empty($this->request->getPost('jenis_pelaksanaan')) ? $this->request->getPost('jenis_pelaksanaan') : NULL,
            'detail_pelaksanaan' => !empty($this->request->getPost('detail_pelaksanaan')) ? $this->request->getPost('detail_pelaksanaan') : NULL,
            'keterangan_pelaksanaan' => !empty($this->request->getPost('keterangan_pelaksanaan')) ? $this->request->getPost('keterangan_pelaksanaan') : NULL,
            'tgl_penunjang' => !empty($this->request->getPost('tgl_penunjang')) ? $this->request->getPost('tgl_penunjang') : NULL,
            'satuan_hasil' => !empty($this->request->getPost('satuan_hasil')) ? $this->request->getPost('satuan_hasil') : NULL,
            'jumlah_volume_dosen' => !empty($this->request->getPost('jumlah_volume_dosen')) ? $this->request->getPost('jumlah_volume_dosen') : NULL,
            'keterangan' => !empty($this->request->getPost('keterangan')) ? $this->request->getPost('keterangan') : NULL,
            'bukti_file' => !empty($this->request->getPost('bukti_file')) ? $this->request->getPost('bukti_file') : NULL,
        );
        $this->penj->updatedata($id, $data);
        // return base_url('valpendidikan/index/1');
        return redirect()->to(base_url('valpenunjang/index/' . $id_usulan));
    }

    public function delete($id)
    {
        $datapenj = $this->penj->get_databyid($id);
        $id_usulan = $datapenj->id_usulan;
        $this->penj->hapusdata($id);
        return redirect()->to(base_url('valpenunjang/index/' . $id_usulan));
    }
}
