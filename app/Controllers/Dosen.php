<?php

namespace App\Controllers;

use App\Models\DosenModel;
use CodeIgniter\Controller;
use App\Libraries\Datatables;
use CodeIgniter\Files\File;


class Dosen extends BaseController
{
    protected $dsn;
    protected $db;
    protected $helpers = ['form'];
    public function __construct()
    {
        $this->dsn = new DosenModel();
        $this->db = \Config\Database::connect();
    }

    public function index()
    {
        $data = array(
            'title' => 'Daftar Dosen',
            'isi' => 'opt/listdosen',
            'css' => '
            <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/v/bs/jszip-2.5.0/dt-1.10.16/af-2.2.2/b-1.5.1/b-colvis-1.5.1/b-flash-1.5.1/b-html5-1.5.1/b-print-1.5.1/cr-1.4.1/fc-3.2.4/fh-3.1.3/kt-2.3.2/r-2.2.1/rg-1.0.2/rr-1.2.3/sc-1.4.4/sl-1.2.5/datatables.min.css"/>
            ',
            'js' => '
            <script type="text/javascript" src="https://cdn.datatables.net/v/bs/jszip-2.5.0/dt-1.10.16/af-2.2.2/b-1.5.1/b-colvis-1.5.1/b-flash-1.5.1/b-html5-1.5.1/b-print-1.5.1/cr-1.4.1/fc-3.2.4/fh-3.1.3/kt-2.3.2/r-2.2.1/rg-1.0.2/rr-1.2.3/sc-1.4.4/sl-1.2.5/datatables.min.js"></script>
            ',
        );
        echo view('layout/optwraper', $data);
    }

    public function ajaxloaddata()
    {
        $params['draw'] = $_REQUEST['draw'];
        $start = $_REQUEST['start'];
        $length = $_REQUEST['length'];
        $search_value = $_REQUEST['search']['value'];
        //$data=array();
        if (!empty($search_value)) {
            $total_count = $this->db->query("SELECT * from dosen WHERE nik_dosen like '%" . $search_value . "%' OR nama like '%" . $search_value . "%' OR email like '%" . $search_value . "%' OR no_hp like '%" . $search_value . "%' OR role like '%" . $search_value . "%'")->getResult();
            $data = $this->db->query("SELECT * from dosen WHERE nik_dosen like '%" . $search_value . "%' OR nama like '%" . $search_value . "%' OR email like '%" . $search_value . "%' OR no_hp like '%" . $search_value . "%' OR role like '%" . $search_value . "%' limit $start, $length")->getResult();
        } else {
            $total_count = $this->db->query("SELECT * from dosen where role = 'Dosen'")->getResult();
            $data = $this->db->query("SELECT * from dosen where role = 'Dosen' limit $start, $length")->getResult();
        }

        $data1 = array();
        foreach ($data as $ld) {
            $row = array(
                "nik_dosen"     => $ld->nik_dosen,
                "nama"          => $ld->nama,
                "email"         => $ld->email,
                "no_hp"         => $ld->no_hp,
                "role"          => $ld->role,
                "action"        => "<a href='" . Base_url('dosen/edit/' . $ld->nik_dosen) . "'><button type='button' class='btn btn-warning btn-xs'><i class='fa fa-pencil'></i></button></a>",

            );
            $data1[] = $row;
        }

        $json_data = array(
            "data"              => $data1,
            "recordsTotal"      => count($total_count),
            "recordsFiltered"   => count($total_count),
            "draw"              => intval($params['draw']),
        );

        echo json_encode($json_data);
    }

    public function create()
    {
        $data = array(
            'title'         => 'Input Data Dosen',
            'isi'           => 'opt/formdosen',
            'action'        => 'create_action',
            'button'        => 'Simpan',
            // 'idkamar'   => set_value('idkamar'),
            // 'kodekamar' => set_value('kodekamar'),
            // 'namatipe'  => set_value('namatipe'),
            // 'ukuran'    => set_value('ukuran'),
            'nik_dosen'     => 'nik_dosen',
            'nama'          => 'nama',
            'password'      => 'password',
            'email'         => 'email',
            'no_hp'         => 'no_hp',
            'role'          => 'role'
        );
        echo view("layout/optwraper", $data);
    }

    public function create_action()
    {
        $data = array(
            'nik_dosen'     => !empty($this->request->getPost('nik_dosen')) ? $this->request->getPost('nik_dosen') : NULL,
            'nama'          => !empty($this->request->getPost('nama')) ? $this->request->getPost('nama') : NULL,
            'password'      => !empty($this->request->getPost('password')) ? $this->request->getPost('password') : NULL,
            'email'         => !empty($this->request->getPost('email')) ? $this->request->getPost('email') : NULL,
            'no_hp'         => !empty($this->request->getPost('no_hp')) ? $this->request->getPost('no_hp') : NULL,
            'role'          => !empty($this->request->getPost('role')) ? $this->request->getPost('role') : NULL,
        );
        $this->dsn->insertdata($data);
        return redirect()->to(base_url('dosen'));
    }

    public function edit($id)
    {
        $datadsn = $this->dsn->get_databyid($id);
        $data = array(
            'title'         => 'Edit Data Dosen',
            'isi'           => 'opt/formdosen',
            'action'        => base_url('dosen/edit_action'),
            'button'        => 'Update',
            'js'            => '
            <script src="https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/7.18.0/sweetalert2.all.min.js"></script>
            <script src="https://cdnjs.cloudflare.com/ajax/libs/core-js/2.4.1/core.js"></script>
            <script>
                    // delete
                    $(function() {
                        $(\'.delete\').on(\'click\', function(){
                            var delete_url = $(this).attr(\'data-url\');
                            swal({
                                title: \'Are you sure?\',
                                text: \'You wont be able to revert this!\',
                                type: \'warning\',
                                showCancelButton: true,
                                confirmButtonColor: \'#3085d6\',
                                cancelButtonColor: \'#d33\',
                                confirmButtonText: \'Yes, delete it!\',
                                cancelButtonText: \'No, cancel!\',
                                confirmButtonClass: \'btn btn-success\',
                                cancelButtonClass: \'btn btn-danger\',
                                buttonsStyling: false,
                                reverseButtons: true
                            }).then((result) => {
                                if (result.value) {
                                    window.location.href = delete_url;
                                }
                            });
                        });
                    });
                </script>
            ',
            'nik_dosen'     => $id,
            'nama'          => $datadsn->nama,
            'password'      => $datadsn->password,
            'email'         => $datadsn->email,
            'no_hp'         => $datadsn->no_hp,
            'role'          => $datadsn->role,
        );
        echo view("layout/optwraper", $data);
    }

    public function edit_action()
    {
        $nik_dosen = $this->request->getPost('nik_dosen');
        $data = array(
            'nama'      => !empty($this->request->getPost('nama')) ? $this->request->getPost('nama') : NULL,
            'password'  => !empty($this->request->getPost('password')) ? $this->request->getPost('password') : NULL,
            'email'     => !empty($this->request->getPost('email')) ? $this->request->getPost('email') : NULL,
            'no_hp'     => !empty($this->request->getPost('no_hp')) ? $this->request->getPost('no_hp') : NULL,
            'role'      => !empty($this->request->getPost('role')) ? $this->request->getPost('role') : NULL,
        );
        $this->dsn->updatedata($nik_dosen, $data);
        return redirect()->to(base_url('dosen'));
    }

    public function delete($id)
    {
        $this->dsnt->hapusdata($id);
        return redirect()->to(base_url('dosen'));
    }

    // Logout
    public function logout()
    {
        $session = session();
        $session->destroy();
        return redirect()->to('login');
    }

    // Menu Dosen
    // public function dsn()
    // {
    //     $data = array(
    //         'title' => 'Daftar Dosen',
    //         'isi' => 'dsn/listdosen',
    //         'css' => '
    //         <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/v/bs/jszip-2.5.0/dt-1.10.16/af-2.2.2/b-1.5.1/b-colvis-1.5.1/b-flash-1.5.1/b-html5-1.5.1/b-print-1.5.1/cr-1.4.1/fc-3.2.4/fh-3.1.3/kt-2.3.2/r-2.2.1/rg-1.0.2/rr-1.2.3/sc-1.4.4/sl-1.2.5/datatables.min.css"/>
    //         ',
    //         'js' => '
    //         <script type="text/javascript" src="https://cdn.datatables.net/v/bs/jszip-2.5.0/dt-1.10.16/af-2.2.2/b-1.5.1/b-colvis-1.5.1/b-flash-1.5.1/b-html5-1.5.1/b-print-1.5.1/cr-1.4.1/fc-3.2.4/fh-3.1.3/kt-2.3.2/r-2.2.1/rg-1.0.2/rr-1.2.3/sc-1.4.4/sl-1.2.5/datatables.min.js"></script>
    //         ',
    //     );
    //     echo view('layout/optwraper', $data);
    // }

    // public function ajaxloaddatadsn()
    // {
    //     $params['draw'] = $_REQUEST['draw'];
    //     $start = $_REQUEST['start'];
    //     $length = $_REQUEST['length'];
    //     $search_value = $_REQUEST['search']['value'];
    //     //$data=array();
    //     if (!empty($search_value)) {
    //         $total_count = $this->db->query("SELECT * FROM dosen WHERE nik_dosen like '%" . $search_value . "%' OR nama like '%" . $search_value . "%' OR email like '%" . $search_value . "%' OR no_hp like '%" . $search_value . "%' OR role like '%" . $search_value . "%'")->getResult();
    //         $data = $this->db->query("SELECT * FROM dosen WHERE nik_dosen like '%" . $search_value . "%' OR nama like '%" . $search_value . "%' OR email like '%" . $search_value . "%' OR no_hp like '%" . $search_value . "%' OR role like '%" . $search_value . "%' limit $start, $length")->getResult();
    //     } else {
    //         $total_count = $this->db->query("SELECT * FROM dosen")->getResult();
    //         $data = $this->db->query("SELECT * FROM dosen limit $start, $length")->getResult();
    //     }

    //     $data2 = array();
    //     foreach ($data as $ldd) {
    //         $row = array(
    //             "nik_dosen"     => $ldd->nik_dosen,
    //             "nama"          => $ldd->nama,
    //             "email"         => $ldd->email,
    //             "no_hp"         => $ldd->no_hp,
    //             "role"          => $ldd->role,
    //         );
    //         $data2[] = $row;
    //     }

    //     $json_data = array(
    //         "data"              => $data2,
    //         "recordsTotal"      => count($total_count),
    //         "recordsFiltered"   => count($total_count),
    //         "draw"              => intval($params['draw']),
    //     );

    //     echo json_encode($json_data);
    // }

    // profile

    public function myprofile()
    {
        $session = session();
        $nik = $session->nik_dosen;
        $datadsn = $this->dsn->get_profiledsn($nik);
        $data = array(
            'title' => 'Profile',
            'action' => base_url('dosen/myprofile_action'),
            'isi' => 'dsn/profile',
            'nik_dosen' => $datadsn->nik_dosen,
            'nama' => $datadsn->nama,
            'email' => $datadsn->email,
            'no_hp' => $datadsn->no_hp,
            'role' => $datadsn->role,
            'tempat_lahir' => $datadsn->tempat_lahir,
            'tgl_lahir' => $datadsn->tgl_lahir,
            'jenis_kelamin' => $datadsn->jenis_kelamin,
            'jurusan' => $datadsn->jurusan,
            'prodi' => $datadsn->prodi,
            'jenjang_pendidikan' => $datadsn->jenjang_pendidikan,
            'jabatan_fungsional' => $datadsn->jabatan_fungsional,
            'bidang_ilmu' => $datadsn->bidang_ilmu,
            'foto_profile' => $datadsn->foto_profile,
            'nidn' => $datadsn->nidn,
            'nip' => $datadsn->nip,
            'pangkat_golongan' => $datadsn->pangkat_golongan,
            'masa_gol_lama' => $datadsn->masa_gol_lama,
            'masa_gol_baru' => $datadsn->masa_gol_baru,

        );
        echo view('layout/dsnwraper', $data);
    }

    public function myprofile_action()
    {
        $session = session();
        $nik = $session->nik_dosen;

        //ambil gambar
        // $fileFoto = $this->request->getPost('foto_profile');
        //pindah gambar ke folder img
        // $fileFoto->move('/asset/profile');
        // $namaFoto = $fileFoto->getName();
        $data = array(
            'nama' => !empty($this->request->getPost('nama')) ? $this->request->getPost('nama') : NULL,
            'email' => !empty($this->request->getPost('email')) ? $this->request->getPost('email') : NULL,
            'no_hp' => !empty($this->request->getPost('no_hp')) ? $this->request->getPost('no_hp') : NULL,
            'role' => !empty($this->request->getPost('role')) ? $this->request->getPost('role') : NULL,
            'tempat_lahir' => !empty($this->request->getPost('tempat_lahir')) ? $this->request->getPost('tempat_lahir') : NULL,
            'tgl_lahir' => !empty($this->request->getPost('tgl_lahir')) ? $this->request->getPost('tgl_lahir') : NULL,
            'jenis_kelamin' => !empty($this->request->getPost('jenis_kelamin')) ? $this->request->getPost('jenis_kelamin') : NULL,
            'jurusan' => !empty($this->request->getPost('jurusan')) ? $this->request->getPost('jurusan') : NULL,
            'prodi' => !empty($this->request->getPost('prodi')) ? $this->request->getPost('prodi') : NULL,
            'jenjang_pendidikan' => !empty($this->request->getPost('jenjang_pendidikan')) ? $this->request->getPost('nama') : NULL,
            'jabatan_fungsional' => !empty($this->request->getPost('jabatan_fungsional')) ? $this->request->getPost('email') : NULL,
            'bidang_ilmu' => !empty($this->request->getPost('bidang_ilmu')) ? $this->request->getPost('bidang_ilmu') : NULL,
            // 'foto_profile' => !empty($this->request->getPost('foto_profile')) ? $namaFoto : NULL,
            'nidn' => !empty($this->request->getPost('nidn')) ? $this->request->getPost('nidn') : NULL,
            'nip' => !empty($this->request->getPost('nip')) ? $this->request->getPost('nip') : NULL,
            'pangkat_golongan' => !empty($this->request->getPost('pangkat_golongan')) ? $this->request->getPost('pangkat_golongan') : NULL,
            'masa_gol_lama' => !empty($this->request->getPost('masa_gol_lama')) ? $this->request->getPost('masa_gol_lama') : NULL,
            'masa_gol_baru' => !empty($this->request->getPost('masa_gol_baru')) ? $this->request->getPost('masa_gol_baru') : NULL,

        );
        $this->dsn->updateprofile($nik, $data,);
        return redirect()->to(base_url('dosen/myprofile'));
    }

    public function profile()
    {
        $datadsn = $this->dsn->get_profile();
        $data = array(
            'title' => 'Profile',
            'action' => base_url('dosen/profile_action'),
            'isi' => 'dsn/profile',
            'nik_dosen' => $datadsn->nik_dosen,
            'nama' => $datadsn->nama,
            'email' => $datadsn->email,
            'no_hp' => $datadsn->no_hp,
            'role' => $datadsn->role,
            'tempat_lahir' => $datadsn->tempat_lahir,
            'tgl_lahir' => $datadsn->tgl_lahir,
            'jenis_kelamin' => $datadsn->jenis_kelamin,
            'jurusan' => $datadsn->jurusan,
            'prodi' => $datadsn->prodi,
            'jenjang_pendidikan' => $datadsn->jenjang_pendidikan,
            'jabatan_fungsional' => $datadsn->jabatan_fungsional,
            'bidang_ilmu' => $datadsn->bidang_ilmu,
            'foto_profile' => $datadsn->foto_profile,
            'nidn' => $datadsn->nidn,
            'nip' => $datadsn->nip,
            'pangkat_golongan' => $datadsn->pangkat_golongan,
            'masa_gol_lama' => $datadsn->masa_gol_lama,
            'masa_gol_baru' => $datadsn->masa_gol_baru,

        );
        echo view('layout/dsnwraper', $data);
    }

    // public function upload()
    // {
    //     $validationRule = [
    //         'foto_profile' => [
    //             'label' => 'Image File',
    //             'rules' => 'uploaded[foto_profile]'
    //                 . '|is_image[foto_profile]'
    //                 . '|mime_in[foto_profile,image/jpg,image/jpeg,image/gif,image/png,image/webp]'
    //                 . '|max_size[foto_profile,100]'
    //                 . '|max_dims[foto_profile,1024,768]',
    //         ],
    //     ];
    //     if (!$this->validate($validationRule)) {
    //         $data = ['errors' => $this->validator->getErrors()];

    //         return view('dsn/profile', $data);
    //     }

    //     $img = $this->request->getFile('foto_profile');

    //     if (!$img->hasMoved()) {
    //         $filepath = WRITEPATH . 'uploads/' . $img->store();

    //         $data = ['uploaded_flleinfo' => new File($filepath)];

    //         return view('upload_success', $data);
    //     }
    //     $data = ['errors' => 'The file has already been moved.'];

    //     return view('dsn/profile', $data);
    // }

    public function profile_action()
    {
        $session = session();
        $nik = $session->nik_dosen;

        //ambil gambar
        // $fileFoto = $this->request->getPost('foto_profile');
        //pindah gambar ke folder img
        // $fileFoto->move('/asset/profile');
        // $namaFoto = $fileFoto->getName();
        $data = array(
            'nama' => !empty($this->request->getPost('nama')) ? $this->request->getPost('nama') : NULL,
            'email' => !empty($this->request->getPost('email')) ? $this->request->getPost('email') : NULL,
            'no_hp' => !empty($this->request->getPost('no_hp')) ? $this->request->getPost('no_hp') : NULL,
            'role' => !empty($this->request->getPost('role')) ? $this->request->getPost('role') : NULL,
            'tempat_lahir' => !empty($this->request->getPost('tempat_lahir')) ? $this->request->getPost('tempat_lahir') : NULL,
            'tgl_lahir' => !empty($this->request->getPost('tgl_lahir')) ? $this->request->getPost('tgl_lahir') : NULL,
            'jenis_kelamin' => !empty($this->request->getPost('jenis_kelamin')) ? $this->request->getPost('jenis_kelamin') : NULL,
            'jurusan' => !empty($this->request->getPost('jurusan')) ? $this->request->getPost('jurusan') : NULL,
            'prodi' => !empty($this->request->getPost('prodi')) ? $this->request->getPost('prodi') : NULL,
            'jenjang_pendidikan' => !empty($this->request->getPost('jenjang_pendidikan')) ? $this->request->getPost('nama') : NULL,
            'jabatan_fungsional' => !empty($this->request->getPost('jabatan_fungsional')) ? $this->request->getPost('email') : NULL,
            'bidang_ilmu' => !empty($this->request->getPost('bidang_ilmu')) ? $this->request->getPost('bidang_ilmu') : NULL,
            // 'foto_profile' => !empty($this->request->getPost('foto_profile')) ? $namaFoto : NULL,
            'nidn' => !empty($this->request->getPost('nidn')) ? $this->request->getPost('nidn') : NULL,
            'nip' => !empty($this->request->getPost('nip')) ? $this->request->getPost('nip') : NULL,
            'pangkat_golongan' => !empty($this->request->getPost('pangkat_golongan')) ? $this->request->getPost('pangkat_golongan') : NULL,
            'masa_gol_lama' => !empty($this->request->getPost('masa_gol_lama')) ? $this->request->getPost('masa_gol_lama') : NULL,
            'masa_gol_baru' => !empty($this->request->getPost('masa_gol_baru')) ? $this->request->getPost('masa_gol_baru') : NULL,

        );
        $this->dsn->updateprofile($nik, $data,);
        return redirect()->to(base_url('dosen/profile'));
    }

    //change password
    public function pass()
    {
        $data = array(
            'title' => 'Ubah Sandi',
            'action' => base_url('dosen/pass_action'),
            'isi' => 'dsn/formpass',
        );
        echo view('layout/dsnwraper', $data);
    }

    public function pass_action()
    {
        $session = session();
        $id = $session->nik_dosen;

        $nik = $this->request->getPost('nik_dosen');
        $passlama = $this->request->getPost('passlama');
        $passbaru = $this->request->getPost('passbaru');
        $valpassbaru = $this->request->getPost('valpassbaru');

        if ($this->dsn->passprocess($nik, $passlama) && $passbaru == $valpassbaru && $nik == $id) {
            $data = $passbaru;

            $this->dsn->updatepass($nik, $data);
        }
        return redirect()->to(base_url('dosen/pass'));
    }
}
