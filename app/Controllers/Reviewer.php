<?php

namespace App\Controllers;

use App\Models\DosenModel;
use CodeIgniter\Controller;
use App\Libraries\Datatables;

class Reviewer extends BaseController
{
    protected $rev;
    protected $db;
    public function __construct()
    {
        $this->rev = new DosenModel();
        $this->db = \Config\Database::connect();
    }

    public function index()
    {
        $data = array(
            'title' => 'Daftar Reviewer',
            'isi' => 'opt/listreviewer',
            'css' => '
            <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/v/bs/jszip-2.5.0/dt-1.10.16/af-2.2.2/b-1.5.1/b-colvis-1.5.1/b-flash-1.5.1/b-html5-1.5.1/b-print-1.5.1/cr-1.4.1/fc-3.2.4/fh-3.1.3/kt-2.3.2/r-2.2.1/rg-1.0.2/rr-1.2.3/sc-1.4.4/sl-1.2.5/datatables.min.css"/>
            ',
            'js' => '
            <script type="text/javascript" src="https://cdn.datatables.net/v/bs/jszip-2.5.0/dt-1.10.16/af-2.2.2/b-1.5.1/b-colvis-1.5.1/b-flash-1.5.1/b-html5-1.5.1/b-print-1.5.1/cr-1.4.1/fc-3.2.4/fh-3.1.3/kt-2.3.2/r-2.2.1/rg-1.0.2/rr-1.2.3/sc-1.4.4/sl-1.2.5/datatables.min.js"></script>
            ',
        );
        echo view('layout/optwraper', $data);
    }

    public function ajaxloaddata()
    {
        $params['draw'] = $_REQUEST['draw'];
        $start = $_REQUEST['start'];
        $length = $_REQUEST['length'];
        $search_value = $_REQUEST['search']['value'];
        //$data=array();
        if (!empty($search_value)) {
            $total_count = $this->db->query("SELECT * from dosen WHERE nik_dosen like '%" . $search_value . "%' OR nama like '%" . $search_value . "%' OR email like '%" . $search_value . "%' OR no_hp like '%" . $search_value . "%' OR role like '%" . $search_value . "%'")->getResult();
            $data = $this->db->query("SELECT * from dosen WHERE nik_dosen like '%" . $search_value . "%' OR nama like '%" . $search_value . "%' OR email like '%" . $search_value . "%' OR no_hp like '%" . $search_value . "%' OR role like '%" . $search_value . "%' limit $start, $length")->getResult();
        } else {
            $total_count = $this->db->query("SELECT * from dosen where role = 'Reviewer'")->getResult();
            $data = $this->db->query("SELECT * from dosen where role = 'Reviewer' limit $start, $length")->getResult();
        }

        $data1 = array();
        foreach ($data as $ld) {
            $row = array(
                "nik_dosen"     => $ld->nik_dosen,
                "nama"          => $ld->nama,
                "email"         => $ld->email,
                "no_hp"         => $ld->no_hp,
                "role"          => $ld->role,
                "action"        => "<a href='" . Base_url('reviewer/edit/' . $ld->nik_dosen) . "'><button type='button' class='btn btn-warning btn-xs'><i class='fa fa-pencil'></i></button></a>",

            );
            $data1[] = $row;
        }

        $json_data = array(
            "data"              => $data1,
            "recordsTotal"      => count($total_count),
            "recordsFiltered"   => count($total_count),
            "draw"              => intval($params['draw']),
        );

        echo json_encode($json_data);
    }

    public function create()
    {
        $data = array(
            'title'         => 'Input Data Reviewer',
            'isi'           => 'opt/formreviewer',
            'action'        => 'create_action',
            'button'        => 'Simpan',
            // 'idkamar'   => set_value('idkamar'),
            // 'kodekamar' => set_value('kodekamar'),
            // 'namatipe'  => set_value('namatipe'),
            // 'ukuran'    => set_value('ukuran'),
            'nik_dosen'     => 'nik_dosen',
            'nama'          => 'nama',
            'password'      => 'password',
            'email'         => 'email',
            'no_hp'         => 'no_hp',
            'role'          => 'role'
        );
        echo view("layout/optwraper", $data);
    }

    public function create_action()
    {
        $data = array(
            'nik_dosen'     => !empty($this->request->getPost('nik_dosen')) ? $this->request->getPost('nik_dosen') : NULL,
            'nama'          => !empty($this->request->getPost('nama')) ? $this->request->getPost('nama') : NULL,
            'password'      => !empty($this->request->getPost('password')) ? $this->request->getPost('password') : NULL,
            'email'         => !empty($this->request->getPost('email')) ? $this->request->getPost('email') : NULL,
            'no_hp'         => !empty($this->request->getPost('no_hp')) ? $this->request->getPost('no_hp') : NULL,
            'role'          => !empty($this->request->getPost('role')) ? $this->request->getPost('role') : NULL,
        );
        $this->rev->insertdata($data);
        return redirect()->to(base_url('reviewer'));
    }

    public function edit($id)
    {
        $datarev = $this->rev->get_databyid($id);
        $data = array(
            'title'         => 'Edit Data Reviewer',
            'isi'           => 'opt/formreviewer',
            'action'        => base_url('reviewer/edit_action'),
            'button'        => 'Update',
            'js'            => '
            <script src="https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/7.18.0/sweetalert2.all.min.js"></script>
            <script src="https://cdnjs.cloudflare.com/ajax/libs/core-js/2.4.1/core.js"></script>
            <script>
                    // delete
                    $(function() {
                        $(\'.delete\').on(\'click\', function(){
                            var delete_url = $(this).attr(\'data-url\');
                            swal({
                                title: \'Are you sure?\',
                                text: \'You wont be able to revert this!\',
                                type: \'warning\',
                                showCancelButton: true,
                                confirmButtonColor: \'#3085d6\',
                                cancelButtonColor: \'#d33\',
                                confirmButtonText: \'Yes, delete it!\',
                                cancelButtonText: \'No, cancel!\',
                                confirmButtonClass: \'btn btn-success\',
                                cancelButtonClass: \'btn btn-danger\',
                                buttonsStyling: false,
                                reverseButtons: true
                            }).then((result) => {
                                if (result.value) {
                                    window.location.href = delete_url;
                                }
                            });
                        });
                    });
                </script>
            ',
            'nik_dosen'     => $id,
            'nama'          => $datarev->nama,
            'password'      => $datarev->password,
            'email'         => $datarev->email,
            'no_hp'         => $datarev->no_hp,
            'role'          => $datarev->role,
        );
        echo view("layout/optwraper", $data);
    }

    public function edit_action()
    {
        $nik_dosen = $this->request->getPost('nik_dosen');
        $data = array(
            'nama'      => !empty($this->request->getPost('nama')) ? $this->request->getPost('nama') : NULL,
            'password'  => !empty($this->request->getPost('password')) ? $this->request->getPost('password') : NULL,
            'email'     => !empty($this->request->getPost('email')) ? $this->request->getPost('email') : NULL,
            'no_hp'     => !empty($this->request->getPost('no_hp')) ? $this->request->getPost('no_hp') : NULL,
            'role'      => !empty($this->request->getPost('role')) ? $this->request->getPost('role') : NULL,
        );
        $this->rev->updatedata($nik_dosen, $data);
        return redirect()->to(base_url('reviewer'));
    }

    public function delete($id)
    {
        $this->rev->hapusdata($id);
        return redirect()->to(base_url('reviewer'));
    }
}
