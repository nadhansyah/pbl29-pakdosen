<?php

namespace App\Controllers;

use App\Models\PendidikanModel;
use App\Models\VerifpendidikanModel;
use App\Models\NilaipendidikanModel;
use CodeIgniter\Controller;
use App\Libraries\Datatables;
use App\Models\UsulanModel;

class Verifpendidikan extends BaseController
{
    protected $vpend;
    protected $npend;
    protected $pend;
    protected $usul;
    protected $db;
    public function __construct()
    {
        $this->pend = new PendidikanModel();
        $this->vpend = new VerifpendidikanModel();
        $this->npend = new NilaipendidikanModel();
        $this->usul = new UsulanModel();
        $this->db = \Config\Database::connect();
    }

    public function index($id)
    {
        $data = array(
            'title' => 'Verifikasi Nilai Pendidikan',
            'isi' => 'opt/listverifpendidikan',
            'css' => '
            <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/v/bs/jszip-2.5.0/dt-1.10.16/af-2.2.2/b-1.5.1/b-colvis-1.5.1/b-flash-1.5.1/b-html5-1.5.1/b-print-1.5.1/cr-1.4.1/fc-3.2.4/fh-3.1.3/kt-2.3.2/r-2.2.1/rg-1.0.2/rr-1.2.3/sc-1.4.4/sl-1.2.5/datatables.min.css"/>
            ',
            'js' => '
            <script type="text/javascript" src="https://cdn.datatables.net/v/bs/jszip-2.5.0/dt-1.10.16/af-2.2.2/b-1.5.1/b-colvis-1.5.1/b-flash-1.5.1/b-html5-1.5.1/b-print-1.5.1/cr-1.4.1/fc-3.2.4/fh-3.1.3/kt-2.3.2/r-2.2.1/rg-1.0.2/rr-1.2.3/sc-1.4.4/sl-1.2.5/datatables.min.js"></script>
            ',
            'idusul' => $id,
            // 'bcreate'  => "<a href=" . Base_url('pendidikan/create/' . $ld->id_usulan) . "><button type='button' class='btn btn-success btn-sm'><i class='fa fa-plus'></i> Create</button></a>"
        );
        echo view('layout/dsnwraper', $data);
    }

    public function ajaxloaddata($id)
    {
        $session = session();
        $nik = $session->nik_dosen;
        $params['draw'] = $_REQUEST['draw'];
        $start = $_REQUEST['start'];
        $length = $_REQUEST['length'];
        $search_value = $_REQUEST['search']['value'];
        //$data=array();
        if (!empty($search_value)) {
            $total_count = $this->db->query("SELECT p.id_pendidikan, p.jenis_pendidikan, p.detail_pendidikan,
            p.jumlah_volume_dosen, pr.jumlah_volume_reviewer 
            FROM pendidikan p 
            LEFT JOIN pendidikan_reviewer pr 
            ON pr.id_pendidikan = p.id_pendidikan 
            WHERE id_usulan = '" . $id . "' AND p.jenis_pendidikan like '%" . $search_value . "%' OR p.detail_pendidikan like '%" . $search_value . "%' OR p.jumlah_volume_dosen like '%" . $search_value . "%' OR pr.jumlah_volume_reviewer like '%" . $search_value . "%'")->getResult();
            $data = $this->db->query("SELECT p.id_pendidikan, p.jenis_pendidikan, p.detail_pendidikan,
            p.jumlah_volume_dosen, pr.jumlah_volume_reviewer 
            FROM pendidikan p 
            LEFT JOIN pendidikan_reviewer pr 
            ON pr.id_pendidikan = p.id_pendidikan 
            WHERE id_usulan = '" . $id . "' AND p.jenis_pendidikan like '%" . $search_value . "%' OR p.detail_pendidikan like '%" . $search_value . "%' OR p.jumlah_volume_dosen like '%" . $search_value . "%' OR pr.jumlah_volume_reviewer like '%" . $search_value . "%' limit $start, $length")->getResult();
        } else {
            $total_count = $this->db->query("SELECT p.id_pendidikan, p.jenis_pendidikan, p.detail_pendidikan,
            p.jumlah_volume_dosen, pr.jumlah_volume_reviewer 
            FROM pendidikan p 
            LEFT JOIN pendidikan_reviewer pr 
            ON pr.id_pendidikan = p.id_pendidikan
            WHERE id_usulan = '" . $id . "'")->getResult();
            $data = $this->db->query("SELECT p.id_pendidikan, p.jenis_pendidikan, p.detail_pendidikan,
            p.jumlah_volume_dosen, pr.jumlah_volume_reviewer 
            FROM pendidikan p 
            LEFT JOIN pendidikan_reviewer pr 
            ON pr.id_pendidikan = p.id_pendidikan 
            WHERE  id_usulan = '" . $id . "'limit $start, $length")->getResult();
        }

        $data1 = array();
        $no = 1;
        foreach ($data as $ld) {
            $row = array(
                "no"                        => $no++,
                "id_pendidikan"             => $ld->id_pendidikan,
                "jenis_pendidikan"          => $ld->jenis_pendidikan,
                "detail_pendidikan"         => $ld->detail_pendidikan,
                "jumlah_volume_dosen"       => $ld->jumlah_volume_dosen,
                "jumlah_volume_reviewer"    => $ld->jumlah_volume_reviewer,
                "action"                    => "<a href='" . Base_url('verifpendidikan/edit/' . $ld->id_pendidikan) . "'><button type='button' class='btn btn-warning btn-xs'><i class='fa fa-pencil'></i></button></a>",
            );
            $data1[] = $row;
        }

        $json_data = array(
            "data"              => $data1,
            "recordsTotal"      => count($total_count),
            "recordsFiltered"   => count($total_count),
            "draw"              => intval($params['draw']),
        );

        echo json_encode($json_data);
    }

    public function edit($id)
    {
        $id_pend = $id;
        $datapend = $this->pend->get_databyid($id);
        $idusul = $datapend->id_usulan;
        $datavpend = $this->vpend->get_databyid($id);
        if ($datavpend != NULL || $datavpend != "") {
            $data = array(
                'title'         => 'Edit Data Nilai Pendidikan',
                'isi'           => 'opt/formverifpendidikan',
                'action'        => base_url('verifpendidikan/edit_action/' . $idusul . '/' . $id_pend),
                'button'        => 'Update',
                'js'            => '
                <script src="https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/7.18.0/sweetalert2.all.min.js"></script>
                <script src="https://cdnjs.cloudflare.com/ajax/libs/core-js/2.4.1/core.js"></script>',
                'id_pendidikan'             => $id,
                'id_usulan'                 => $datapend->id_usulan,
                'nik_dosen'                 => $datapend->nik_dosen,
                'jenis_pendidikan'          => $datapend->jenis_pendidikan,
                'detail_pendidikan'         => $datapend->detail_pendidikan,
                'tgl_pendidikan'            => $datapend->tgl_pendidikan,
                'satuan_hasil'              => $datapend->satuan_hasil,
                'jumlah_volume_dosen'       => $datapend->jumlah_volume_dosen,
                'keterangan'                => $datapend->keterangan,
                'bukti_file'                => $datapend->bukti_file,
                'jumlah_volume_reviewer'    => $datavpend->jumlah_volume_reviewer,
            );
        } else {
            $data = array(
                'title'         => 'Edit Data Nilai Pendidikan',
                'isi'           => 'opt/formverifpendidikan',
                'action'        => base_url('verifpendidikan/create_action/' . $idusul . '/' . $id_pend),
                'button'        => 'Update',
                'js'            => '
                <script src="https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/7.18.0/sweetalert2.all.min.js"></script>
                <script src="https://cdnjs.cloudflare.com/ajax/libs/core-js/2.4.1/core.js"></script>',
                'id_pendidikan'             => $id,
                'id_usulan'                 => $datapend->id_usulan,
                'nik_dosen'                 => $datapend->nik_dosen,
                'jenis_pendidikan'          => $datapend->jenis_pendidikan,
                'detail_pendidikan'         => $datapend->detail_pendidikan,
                'tgl_pendidikan'            => $datapend->tgl_pendidikan,
                'satuan_hasil'              => $datapend->satuan_hasil,
                'jumlah_volume_dosen'       => $datapend->jumlah_volume_dosen,
                'keterangan'                => $datapend->keterangan,
                'bukti_file'                => $datapend->bukti_file,

                'id_pendidikan'    => 'id_pendidikan',
                'nik_reviewer' => 'nik_reviewer',
                'jumlah_volume_reviewer'    => 'jumlah_volume_reviewer',
            );
        }

        echo view("layout/dsnwraper", $data);
    }

    public function create_action($idusul, $id)
    {
        $session = session();
        $nik = $session->nik_dosen;
        $data = array(
            'id_pendidikan'             => $id,
            'nik_reviewer'             => $nik,
            'jumlah_volume_reviewer' => !empty($this->request->getPost('jumlah_volume_reviewer')) ? $this->request->getPost('jumlah_volume_reviewer') : NULL,
        );

        $this->vpend->insertdata($data);
        return redirect()->to(base_url('verifpendidikan/index/' . $idusul));
    }


    public function edit_action($idusul, $id)
    {
        // $id_usulan = $this->request->getPost('id_usulan');
        $data = array(
            'jumlah_volume_reviewer' => !empty($this->request->getPost('jumlah_volume_reviewer')) ? $this->request->getPost('jumlah_volume_reviewer') : NULL,
        );

        $this->vpend->updatedata($id, $data);
        return redirect()->to(base_url('verifpendidikan/index/' . $idusul));
    }
}
