<?php

namespace App\Controllers;

use App\Models\PendidikanModel;
use CodeIgniter\Controller;
use App\Libraries\Datatables;
use App\Models\UsulanModel;

class Valpendidikan extends BaseController
{
    protected $pend;
    protected $usul;
    protected $db;
    public function __construct()
    {
        $this->pend = new PendidikanModel();
        $this->usul = new UsulanModel();
        $this->db = \Config\Database::connect();
    }

    public function index($id)
    {
        $data = array(
            'title' => 'Data Ajuan Pendidikan',
            'isi' => 'opt/listpendidikan',
            'css' => '
            <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/v/bs/jszip-2.5.0/dt-1.10.16/af-2.2.2/b-1.5.1/b-colvis-1.5.1/b-flash-1.5.1/b-html5-1.5.1/b-print-1.5.1/cr-1.4.1/fc-3.2.4/fh-3.1.3/kt-2.3.2/r-2.2.1/rg-1.0.2/rr-1.2.3/sc-1.4.4/sl-1.2.5/datatables.min.css"/>
            ',
            'js' => '
            <script type="text/javascript" src="https://cdn.datatables.net/v/bs/jszip-2.5.0/dt-1.10.16/af-2.2.2/b-1.5.1/b-colvis-1.5.1/b-flash-1.5.1/b-html5-1.5.1/b-print-1.5.1/cr-1.4.1/fc-3.2.4/fh-3.1.3/kt-2.3.2/r-2.2.1/rg-1.0.2/rr-1.2.3/sc-1.4.4/sl-1.2.5/datatables.min.js"></script>
            ',
            'idusul' => $id,
        );
        echo view('layout/optwraper', $data);
    }

    public function ajaxloaddata($id)
    {
        // $datapend = $this->usul->get_databyid($id);
        // $idusul = $datapend->id_usulan;
        $params['draw'] = $_REQUEST['draw'];
        $start = $_REQUEST['start'];
        $length = $_REQUEST['length'];
        $search_value = $_REQUEST['search']['value'];
        //$data=array();
        if (!empty($search_value)) {
            $total_count = $this->db->query("SELECT id_usulan, id_pendidikan, jenis_pendidikan, detail_pendidikan, jumlah_volume_dosen
            from pendidikan where id_usulan = '" . $id . "' 
            AND jenis_pendidikan like '%" . $search_value . "%' OR detail_pendidikan like '%" . $search_value . "%' OR jumlah_volume_dosen like '%" . $search_value . "%'")->getResult();
            $data = $this->db->query("SELECT id_usulan, id_pendidikan, jenis_pendidikan, detail_pendidikan, jumlah_volume_dosen
            from pendidikan where id_usulan = '" . $id . "'
            AND jenis_pendidikan like '%" . $search_value . "%' OR detail_pendidikan like '%" . $search_value . "%' OR jumlah_volume_dosen like '%" . $search_value . "%' limit $start, $length")->getResult();
        } else {
            $total_count = $this->db->query("SELECT id_usulan, id_pendidikan, jenis_pendidikan, detail_pendidikan, jumlah_volume_dosen
            from pendidikan where id_usulan = '" . $id . "'")->getResult();
            $data = $this->db->query("SELECT id_usulan, id_pendidikan, jenis_pendidikan, detail_pendidikan, jumlah_volume_dosen
            from pendidikan where id_usulan = '" . $id . "' limit $start, $length")->getResult();
        }

        $data1 = array();
        $no = 1;
        foreach ($data as $ld) {
            $row = array(
                "no"                        => $no++,
                "id_usulan"                 => $ld->id_usulan,
                "id_pendidikan"             => $ld->id_pendidikan,
                "jenis_pendidikan"          => $ld->jenis_pendidikan,
                "detail_pendidikan"         => $ld->detail_pendidikan,
                "jumlah_volume_dosen"       => $ld->jumlah_volume_dosen,
                "action"                    => "<a href='" . Base_url('valpendidikan/edit/' . $ld->id_pendidikan) . "'><button type='button' class='btn btn-warning btn-xs'><i class='fa fa-pencil'></i></button></a>",
            );
            $data1[] = $row;
        }

        $json_data = array(
            "data"              => $data1,
            "recordsTotal"      => count($total_count),
            "recordsFiltered"   => count($total_count),
            "draw"              => intval($params['draw']),
        );

        echo json_encode($json_data);
    }

    public function edit($id)
    {
        $datapend = $this->pend->get_databyid($id);
        $idusul = $datapend->id_usulan;
        // $datausul = $this->usul->get_databyid($id);
        $data = array(
            'title'         => 'Edit Data Pendidikan',
            'isi'           => 'opt/formpendidikan',
            'action'        => base_url('valpendidikan/edit_action/' . $idusul . '/' . $id),
            'button'        => 'Update',
            'js'            => '
            <script src="https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/7.18.0/sweetalert2.all.min.js"></script>
            <script src="https://cdnjs.cloudflare.com/ajax/libs/core-js/2.4.1/core.js"></script>
            <script>
                    // delete
                    $(function() {
                        $(\'.delete\').on(\'click\', function(){
                            var delete_url = $(this).attr(\'data-url\');
                            swal({
                                title: \'Are you sure?\',
                                text: \'You wont be able to revert this!\',
                                type: \'warning\',
                                showCancelButton: true,
                                confirmButtonColor: \'#3085d6\',
                                cancelButtonColor: \'#d33\',
                                confirmButtonText: \'Yes, delete it!\',
                                cancelButtonText: \'No, cancel!\',
                                confirmButtonClass: \'btn btn-success\',
                                cancelButtonClass: \'btn btn-danger\',
                                buttonsStyling: false,
                                reverseButtons: true
                            }).then((result) => {
                                if (result.value) {
                                    window.location.href = delete_url;
                                }
                            });
                        });
                    });
                </script>
            ',
            'id_pendidikan'             => $id,
            'id_usulan'                 => $datapend->id_usulan,
            'nik_dosen'                 => $datapend->nik_dosen,
            'jenis_pendidikan'          => $datapend->jenis_pendidikan,
            'detail_pendidikan'         => $datapend->detail_pendidikan,
            'tgl_pendidikan'            => $datapend->tgl_pendidikan,
            'satuan_hasil'              => $datapend->satuan_hasil,
            'jumlah_volume_dosen'       => $datapend->jumlah_volume_dosen,
            'keterangan'                => $datapend->keterangan,
            'bukti_file'                => $datapend->bukti_file,
        );
        echo view("layout/optwraper", $data);
    }
    public function edit_action($idusul, $id)
    {
        $id_usulan = $idusul;
        // $id_pendidikan = $this->request->getPost('id_pendidikan');

        $data = array(
            'jenis_pendidikan' => !empty($this->request->getPost('jenis_pendidikan')) ? $this->request->getPost('jenis_pendidikan') : NULL,
            'detail_pendidikan' => !empty($this->request->getPost('detail_pendidikan')) ? $this->request->getPost('detail_pendidikan') : NULL,
            'tgl_pendidikan' => !empty($this->request->getPost('tgl_pendidikan')) ? $this->request->getPost('tgl_pendidikan') : NULL,
            'satuan_hasil' => !empty($this->request->getPost('satuan_hasil')) ? $this->request->getPost('satuan_hasil') : NULL,
            'jumlah_volume_dosen' => !empty($this->request->getPost('jumlah_volume_dosen')) ? $this->request->getPost('jumlah_volume_dosen') : NULL,
            'keterangan' => !empty($this->request->getPost('keterangan')) ? $this->request->getPost('keterangan') : NULL,
            'bukti_file' => !empty($this->request->getPost('bukti_file')) ? $this->request->getPost('bukti_file') : NULL,
        );
        $this->pend->updatedata($id, $data);
        // return base_url('valpendidikan/index/1');
        return redirect()->to(base_url('valpendidikan/index/' . $id_usulan));
    }

    public function delete($id)
    {
        $datapend = $this->pend->get_databyid($id);
        $id_usulan = $datapend->id_usulan;
        $this->pend->hapusdata($id);
        return redirect()->to(base_url('valpendidikan/index/' . $id_usulan));
    }
}
