<?php

namespace App\Controllers;

use App\Models\UsulanModel;
use App\Models\VerifusulanModel;
use App\Models\NilaiusulanModel;
use CodeIgniter\Controller;
use App\Libraries\Datatables;

class Verifusulan extends BaseController
{
    protected $usul;
    protected $vsul;
    protected $nusul;
    protected $db;
    public function __construct()
    {
        $this->usul = new UsulanModel();
        $this->nusul = new VerifusulanModel();
        $this->nusul = new NilaiusulanModel();
        $this->db = \Config\Database::connect();
    }

    public function index()
    {
        $data = array(
            'title' => 'Penilaian Usulan',
            'isi'   => 'opt/listverifusulan',
            'css'   => '
            <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/v/bs/jszip-2.5.0/dt-1.10.16/af-2.2.2/b-1.5.1/b-colvis-1.5.1/b-flash-1.5.1/b-html5-1.5.1/b-print-1.5.1/cr-1.4.1/fc-3.2.4/fh-3.1.3/kt-2.3.2/r-2.2.1/rg-1.0.2/rr-1.2.3/sc-1.4.4/sl-1.2.5/datatables.min.css"/>
            ',
            'js'    => '
            <script type="text/javascript" src="https://cdn.datatables.net/v/bs/jszip-2.5.0/dt-1.10.16/af-2.2.2/b-1.5.1/b-colvis-1.5.1/b-flash-1.5.1/b-html5-1.5.1/b-print-1.5.1/cr-1.4.1/fc-3.2.4/fh-3.1.3/kt-2.3.2/r-2.2.1/rg-1.0.2/rr-1.2.3/sc-1.4.4/sl-1.2.5/datatables.min.js"></script>
            ',
        );
        echo view('layout/dsnwraper', $data);
    }

    public function ajaxloaddata()
    {
        $session = session();
        $nik = $session->nik_dosen;
        $params['draw'] = $_REQUEST['draw'];
        $start          = $_REQUEST['start'];
        $length         = $_REQUEST['length'];
        $search_value   = $_REQUEST['search']['value'];
        //$data=array();
        if (!empty($search_value)) {
            $total_count = $this->db->query("SELECT ur.id_usulan_reviewer, ur.nik_reviewer_1, ur.nik_reviewer_2, u.id_usulan, u.nik_dosen, u.jenis_usulan, u.status_usulan FROM usulan_reviewer as ur
            LEFT JOIN usulan as u ON u.id_usulan = ur.id_usulan WHERE ur.nik_reviewer_1 = '" . $nik . "' OR ur.nik_reviewer_2 = '" . $nik . "' AND ur.nik_reviewer_1 like '%" . $search_value . "%' OR ur.nik_reviewer_2 like '%" . $search_value . "%' OR u.nik_dosen like '%" . $search_value . "%' OR u.jenis_usulan like '%" . $search_value . "%'")->getResult();
            $data = $this->db->query("SELECT ur.id_usulan_reviewer, ur.nik_reviewer_1, ur.nik_reviewer_2, u.id_usulan, u.nik_dosen, u.jenis_usulan, u.status_usulan FROM usulan_reviewer as ur
            LEFT JOIN usulan as u ON u.id_usulan = ur.id_usulan WHERE ur.nik_reviewer_1 = '" . $nik . "' OR ur.nik_reviewer_2 = '" . $nik . "' AND ur.nik_reviewer_1 like '%" . $search_value . "%' OR ur.nik_reviewer_2 like '%" . $search_value . "%' OR u.nik_dosen like '%" . $search_value . "%' OR u.jenis_usulan like '%" . $search_value . "%' limit $start, $length")->getResult();
        } else {
            $total_count = $this->db->query("SELECT ur.id_usulan_reviewer, ur.nik_reviewer_1, ur.nik_reviewer_2, u.id_usulan, u.nik_dosen, u.jenis_usulan, u.status_usulan FROM usulan_reviewer as ur
            LEFT JOIN usulan as u ON u.id_usulan = ur.id_usulan WHERE ur.nik_reviewer_1 = '" . $nik . "' OR ur.nik_reviewer_2 = '" . $nik . "'")->getResult();
            $data = $this->db->query("SELECT ur.id_usulan_reviewer, ur.nik_reviewer_1, ur.nik_reviewer_2, u.id_usulan, u.nik_dosen, u.jenis_usulan, u.status_usulan FROM usulan_reviewer as ur
            LEFT JOIN usulan as u ON u.id_usulan = ur.id_usulan WHERE ur.nik_reviewer_1 = '" . $nik . "' OR ur.nik_reviewer_2 = '" . $nik . "' limit $start, $length")->getResult();
        }

        $data1 = array();
        foreach ($data as $ld) {
            $row = array(
                "id_usulan_reviewer"    => $ld->id_usulan_reviewer,
                "id_usulan"             => $ld->id_usulan,
                "nik_dosen"             => $ld->nik_dosen,
                "jenis_usulan"          => $ld->jenis_usulan,
                "status_usulan"          => $ld->status_usulan,
                "dokumen"               => "<a href='" . Base_url('verifusulan/detail/' . $ld->id_usulan) . "'><button type='button' class='btn btn-info btn-xs'><i class='fa fa-info-circle'></i> Detail </button></a>",
            );
            $data1[] = $row;
        }

        $json_data = array(
            "data"              => $data1,
            "recordsTotal"      => count($total_count),
            "recordsFiltered"   => count($total_count),
            "draw"              => intval($params['draw']),
        );

        echo json_encode($json_data);
    }

    public function detail($id)
    {
        $datausul = $this->usul->get_databyid($id);

        $data = array(
            'title' => 'Detail Dokumen Penilaian',
            'isi'   => 'opt/detailverifajuan',
            'css'   => '
                <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/v/bs/jszip-2.5.0/dt-1.10.16/af-2.2.2/b-1.5.1/b-colvis-1.5.1/b-flash-1.5.1/b-html5-1.5.1/b-print-1.5.1/cr-1.4.1/fc-3.2.4/fh-3.1.3/kt-2.3.2/r-2.2.1/rg-1.0.2/rr-1.2.3/sc-1.4.4/sl-1.2.5/datatables.min.css"/>
                ',
            'js'            => '
                <script src="https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/7.18.0/sweetalert2.all.min.js"></script>
                <script src="https://cdnjs.cloudflare.com/ajax/libs/core-js/2.4.1/core.js"></script>
                <script>
                    $(function() {
                        $(\'.ver\').on(\'click\', function(){
                            var ver_url = $(this).attr(\'data-url\');
                            swal({
                                title: \'Yakin ingin mengumpulkan nilai sekarang?\',
                                text: \'Anda tidak dapat mengubah data lagi!\',
                                type: \'warning\',
                                showCancelButton: true,
                                confirmButtonColor: \'#3085d6\',
                                cancelButtonColor: \'#d33\',
                                confirmButtonText: \'Ya, yakin!\',
                                cancelButtonText: \'Tidak, kembali!\',
                                confirmButtonClass: \'btn btn-success\',
                                cancelButtonClass: \'btn btn-danger\',
                                buttonsStyling: false,
                                reverseButtons: true
                            }).then((result) => {
                                if (result.value) {
                                    window.location.href = ver_url;
                                }
                            });
                        });
                    });
                </script>',
            'id_usulan'         => $id,
            'nik_dosen'         => $datausul->nik_dosen,
            'jenis_usulan'      => $datausul->jenis_usulan,
        );
        echo view('layout/optwraper', $data);
    }
}
