<?php

namespace App\Controllers;

use App\Models\DosenModel;
use App\Models\OperatorModel;
use CodeIgniter\Controller;
use App\Libraries\Datatables;

class Login extends BaseController
{
    protected $user;
    protected $db;
    public function __construct()
    {
        $this->dsn = new DosenModel();
        $this->opt = new OperatorModel();
        $this->db = \Config\Database::connect();
    }

    public function index()
    {
        $data = array(
            'title'    => 'Login',
            'action'    => base_url('login/login_action'),
        );
        echo view('login', $data);
    }
    function login_action()
    {
        $username = $this->request->getPost('username');
        $password = $this->request->getPost('password');

        if ($data = $this->dsn->loginprocessdsn($username, $password)) {
            $session = session();
            $ses_data = [
                'nik_dosen'      => $data->nik_dosen,
                'nama'              => $data->nama,
                'role'              => $data->role,
                'logged'            => TRUE
            ];
            $session->set($ses_data);
            return redirect()->to(base_url('home/dsn'));
        } else if ($data = $this->opt->loginprocessopt($username, $password)) {
            $session = session();
            $ses_data = [
                'nik_operator'      => $data->nik_operator,
                'nama'              => $data->nama,
                'logged'            => TRUE
            ];
            $session->set($ses_data);
            return redirect()->to(base_url('home'));
        } else {
            return redirect()->to(base_url('login'));
        }
    }
}
