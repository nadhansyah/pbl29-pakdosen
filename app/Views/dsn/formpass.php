<div class="right_col" role="main">
    <div class="">
        <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                    <div class="x_title">
                        <h2><?php echo $title ?></h2>
                        <div class="clearfix"></div>
                    </div>
                    <div class="x_content">
                        <form id="applications" data-parsley-validate class="form-horizontal form-label-left" action="<?php echo $action; ?>" method="post">
                            <div class="form-group">
                                <label class="control-label col-md-3 col-sm-3 col-xs-12">Username <span class="required">*</span></label>
                                <div class="col-md-3 col-sm-6 col-xs-12">
                                    <input type="text" id="nik_dosen" name="nik_dosen" required="required" class="form-control col-md-7 col-xs-12" maxlength="20" placeholder="Masukkan NIK Anda">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-md-3 col-sm-3 col-xs-12">Password Lama <span class="required">*</span></label>
                                <div class="col-md-5 col-sm-6 col-xs-12">
                                    <input type="password" id="passlama" name="passlama" required="required" class="form-control col-md-7 col-xs-12" maxlength="30" placeholder="Masukkan Password Lama Anda">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-md-3 col-sm-3 col-xs-12">Password Baru <span class="required">*</span></label>
                                <div class="col-md-5 col-sm-6 col-xs-12">
                                    <input type="password" id="passbaru" name="passbaru" required="required" class="form-control col-md-7 col-xs-12" maxlength="30" placeholder="Ketikkan Password Baru Anda (max 30 karakter)">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-md-3 col-sm-3 col-xs-12">Ketik Ulang <span class="required">*</span></label>
                                <div class="col-md-5 col-sm-6 col-xs-12">
                                    <input type="password" id="valpassbaru" name="valpassbaru" required="required" class="form-control col-md-7 col-xs-12" maxlength="30" placeholder="Ketikkan ulang Password Baru Anda">
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3 col-sm-offset-3">
                                    <button type="submit" class="btn btn-success"><i class="fa fa-save"></i> Update</button>
                                </div>
                            </div>
                    </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
</div>