<head>
    <style>
        .linktautan {
            /* font-size: 12pt; */
            color: black;
            text-decoration: underline;
        }

        .linktautan:hover {
            color: red;
        }

        .linktautan:link {
            color: blue;
        }

        .linktautan:active {
            color: green;
        }

        .linktautan:visited {
            background: yellow;
        }
    </style>
</head>

<div class="right_col" role="main">
    <div class="">
        <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                    <div class="x_title">
                        <h2><?php echo $title ?></h2>
                        <div class="clearfix"></div>
                    </div>
                    <div class="x_content">
                        <form id="applications" data-parsley-validate class="form-horizontal form-label-left" action="<?php echo $action; ?>" method="post">
                            <!-- <div class="form-group">
                                <label class="control-label col-md-2 col-sm-3 col-xs-12">ID Usulan <span class="required">*</span></label>
                                <div class="col-md-2 col-sm-6 col-xs-12">
                                    <input type="text" id="id_usulan" name="id_usulan" required="required" class="form-control col-md-7 col-xs-12" value="<php echo $id_usulan; ?>">
                                </div>
                            </div> -->
                            <div class="form-group">
                                <label class="control-label col-md-2 col-sm-3 col-xs-12">Jenis Pelaksanaan <span class="required">*</span></label>
                                <div class="col-md-9 col-sm-6 col-xs-12">
                                    <select id="jenis_pelaksanaan" name="jenis_pelaksanaan" required="required" class="form-control col-md-7 col-xs-12" value="<?php echo $jenis_pelaksanaan; ?>">
                                        <option value="">--</option>
                                        <option value="A- Melaksanakan perkulihan/ tutorial dan membimbing, menguji serta menyelenggarakan pendidikan di laboratorium, praktek keguruan bengkel/ studio/kebun percobaan/teknologi pengajaran dan praktek lapangan" <?php if ($jenis_pelaksanaan == 'A- Melaksanakan perkulihan/ tutorial dan membimbing, menguji serta menyelenggarakan pendidikan di laboratorium, praktek keguruan bengkel/ studio/kebun percobaan/teknologi pengajaran dan praktek lapangan') echo 'Selected'; ?>>A- Melaksanakan perkulihan/ tutorial dan membimbing, menguji serta menyelenggarakan pendidikan di laboratorium, praktek keguruan bengkel/ studio/kebun percobaan/teknologi pengajaran dan praktek lapangan</option>
                                        <option value="B - Membimbing seminar (maksimum 1 kum per semester)" <?php if ($jenis_pelaksanaan == 'B - Membimbing seminar (maksimum 1 kum per semester)') echo 'Selected'; ?>>B - Membimbing seminar (maksimum 1 kum per semester)</option>
                                        <option value="C - Membing kuliah kerja nyata, pratek kerja nyata, praktek kerja lapangan (setiap semester nilai 1 kum)" <?php if ($jenis_pelaksanaan == 'C - Membing kuliah kerja nyata, pratek kerja nyata, praktek kerja lapangan (setiap semester nilai 1 kum)') echo 'Selected'; ?>>C - Membing kuliah kerja nyata, pratek kerja nyata, praktek kerja lapangan (setiap semester nilai 1 kum)</option>
                                        <option value="D - Membimbing dan ikut membimbing dalam menghasilkan disertasi, thesis, skripsi dan laporan akhir studi (maksimum 32 kum per semester)" <?php if ($jenis_pelaksanaan == 'D - Membimbing dan ikut membimbing dalam menghasilkan disertasi, thesis, skripsi dan laporan akhir studi (maksimum 32 kum per semester)') echo 'Selected'; ?>>D - Membimbing dan ikut membimbing dalam menghasilkan disertasi, thesis, skripsi dan laporan akhir studi (maksimum 32 kum per semester)</option>
                                        <option value="E - Bertugas sebagai penguji pada ujian akhir (maksimum 8 kum per semester)" <?php if ($jenis_pelaksanaan == 'E - Bertugas sebagai penguji pada ujian akhir (maksimum 8 kum per semester)') echo 'Selected'; ?>>E - Bertugas sebagai penguji pada ujian akhir (maksimum 8 kum per semester)</option>
                                        <option value="F - Membina kegiatan mahasiswa (maksimum 2 kegiatan per semester)" <?php if ($jenis_pelaksanaan == 'F - Membina kegiatan mahasiswa (maksimum 2 kegiatan per semester)') echo 'Selected'; ?>>F - Membina kegiatan mahasiswa (maksimum 2 kegiatan per semester)</option>
                                        <option value="G - Mengembangkan program kuliah" <?php if ($jenis_pelaksanaan == 'G - Mengembangkan program kuliah') echo 'Selected'; ?>>G - Mengembangkan program kuliah</option>
                                        <option value="H - Mengembangkan bahan pengajaran" <?php if ($jenis_pelaksanaan == 'H - Mengembangkan bahan pengajaran') echo 'Selected'; ?>>H - Mengembangkan bahan pengajaran</option>
                                        <option value="I - Menyampaikan orasi ilmiah di tingkat perguruan tinggi" <?php if ($jenis_pelaksanaan == 'I - Menyampaikan orasi ilmiah di tingkat perguruan tinggi') echo 'Selected'; ?>>I - Menyampaikan orasi ilmiah di tingkat perguruan tinggi</option>
                                        <option value="J - Menduduki jabatan pimpinan perguruan tinggi (maksimum 1 jabatan per semester)" <?php if ($jenis_pelaksanaan == 'J - Menduduki jabatan pimpinan perguruan tinggi (maksimum 1 jabatan per semester)') echo 'Selected'; ?>>J - Menduduki jabatan pimpinan perguruan tinggi (maksimum 1 jabatan per semester)</option>
                                        <option value="K - Membimbing Akademik Dosen yang lebih rendah jabatannya" <?php if ($jenis_pelaksanaan == 'K - Membimbing Akademik Dosen yang lebih rendah jabatannya') echo 'Selected'; ?>>K - Membimbing Akademik Dosen yang lebih rendah jabatannya</option>
                                        <option value="L - Melaksanakan kegiatan Detasering dan pencangkokan Akademik Dosen" <?php if ($jenis_pelaksanaan == 'L - Melaksanakan kegiatan Detasering dan pencangkokan Akademik Dosen') echo 'Selected'; ?>>L - Melaksanakan kegiatan Detasering dan pencangkokan Akademik Dosen</option>
                                        <option value="M - Melakukan kegiatan pengembangan diri untuk meningkatkan kompetensi" <?php if ($jenis_pelaksanaan == 'M - Melakukan kegiatan pengembangan diri untuk meningkatkan kompetensi') echo 'Selected'; ?>>M - Melakukan kegiatan pengembangan diri untuk meningkatkan kompetensi</option>

                                    </select>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-md-2 col-sm-3 col-xs-12">Detail Pelaksanaan <span class="required">*</span></label>
                                <div class="col-md-9 col-sm-6 col-xs-12">
                                    <select id="detail_pelaksanaan" name="detail_pelaksanaan" required="required" class="form-control col-md-7 col-xs-12" value="<?php echo $detail_pelaksanaan; ?>">
                                        //asal dulu
                                        <option value="">--</option>
                                        <option value="A- Melaksanakan perkulihan/ tutorial dan membimbing, menguji serta menyelenggarakan pendidikan di laboratorium, praktek keguruan bengkel/ studio/kebun percobaan/teknologi pengajaran dan praktek lapangan" <?php if ($detail_pelaksanaan == 'A- Melaksanakan perkulihan/ tutorial dan membimbing, menguji serta menyelenggarakan pendidikan di laboratorium, praktek keguruan bengkel/ studio/kebun percobaan/teknologi pengajaran dan praktek lapangan') echo 'Selected'; ?>>A- Melaksanakan perkulihan/ tutorial dan membimbing, menguji serta menyelenggarakan pendidikan di laboratorium, praktek keguruan bengkel/ studio/kebun percobaan/teknologi pengajaran dan praktek lapangan</option>
                                        <option value="B - Membimbing seminar (maksimum 1 kum per semester)" <?php if ($detail_pelaksanaan == 'B - Membimbing seminar (maksimum 1 kum per semester)') echo 'Selected'; ?>>B - Membimbing seminar (maksimum 1 kum per semester)</option>
                                        <option value="C - Membing kuliah kerja nyata, pratek kerja nyata, praktek kerja lapangan (setiap semester nilai 1 kum)" <?php if ($detail_pelaksanaan == 'C - Membing kuliah kerja nyata, pratek kerja nyata, praktek kerja lapangan (setiap semester nilai 1 kum)') echo 'Selected'; ?>>C - Membing kuliah kerja nyata, pratek kerja nyata, praktek kerja lapangan (setiap semester nilai 1 kum)</option>
                                        <option value="D - Membimbing dan ikut membimbing dalam menghasilkan disertasi, thesis, skripsi dan laporan akhir studi (maksimum 32 kum per semester)" <?php if ($detail_pelaksanaan == 'D - Membimbing dan ikut membimbing dalam menghasilkan disertasi, thesis, skripsi dan laporan akhir studi (maksimum 32 kum per semester)') echo 'Selected'; ?>>D - Membimbing dan ikut membimbing dalam menghasilkan disertasi, thesis, skripsi dan laporan akhir studi (maksimum 32 kum per semester)</option>
                                        <option value="E - Bertugas sebagai penguji pada ujian akhir (maksimum 8 kum per semester)" <?php if ($detail_pelaksanaan == 'E - Bertugas sebagai penguji pada ujian akhir (maksimum 8 kum per semester)') echo 'Selected'; ?>>E - Bertugas sebagai penguji pada ujian akhir (maksimum 8 kum per semester)</option>
                                        <option value="F - Membina kegiatan mahasiswa (maksimum 2 kegiatan per semester)" <?php if ($detail_pelaksanaan == 'F - Membina kegiatan mahasiswa (maksimum 2 kegiatan per semester)') echo 'Selected'; ?>>F - Membina kegiatan mahasiswa (maksimum 2 kegiatan per semester)</option>
                                        <option value="G - Mengembangkan program kuliah" <?php if ($detail_pelaksanaan == 'G - Mengembangkan program kuliah') echo 'Selected'; ?>>G - Mengembangkan program kuliah</option>
                                        <option value="H - Mengembangkan bahan pengajaran" <?php if ($detail_pelaksanaan == 'H - Mengembangkan bahan pengajaran') echo 'Selected'; ?>>H - Mengembangkan bahan pengajaran</option>
                                        <option value="I - Menyampaikan orasi ilmiah di tingkat perguruan tinggi" <?php if ($detail_pelaksanaan == 'I - Menyampaikan orasi ilmiah di tingkat perguruan tinggi') echo 'Selected'; ?>>I - Menyampaikan orasi ilmiah di tingkat perguruan tinggi</option>
                                        <option value="J - Menduduki jabatan pimpinan perguruan tinggi (maksimum 1 jabatan per semester)" <?php if ($detail_pelaksanaan == 'J - Menduduki jabatan pimpinan perguruan tinggi (maksimum 1 jabatan per semester)') echo 'Selected'; ?>>J - Menduduki jabatan pimpinan perguruan tinggi (maksimum 1 jabatan per semester)</option>
                                        <option value="K - Membimbing Akademik Dosen yang lebih rendah jabatannya" <?php if ($detail_pelaksanaan == 'K - Membimbing Akademik Dosen yang lebih rendah jabatannya') echo 'Selected'; ?>>K - Membimbing Akademik Dosen yang lebih rendah jabatannya</option>
                                        <option value="L - Melaksanakan kegiatan Detasering dan pencangkokan Akademik Dosen" <?php if ($detail_pelaksanaan == 'L - Melaksanakan kegiatan Detasering dan pencangkokan Akademik Dosen') echo 'Selected'; ?>>L - Melaksanakan kegiatan Detasering dan pencangkokan Akademik Dosen</option>
                                        <option value="M - Melakukan kegiatan pengembangan diri untuk meningkatkan kompetensi" <?php if ($detail_pelaksanaan == 'M - Melakukan kegiatan pengembangan diri untuk meningkatkan kompetensi') echo 'Selected'; ?>>M - Melakukan kegiatan pengembangan diri untuk meningkatkan kompetensi</option>

                                    </select>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-md-2 col-sm-3 col-xs-12">Keterangan Pelaksanaan <span class="required">*</span></label>
                                <div class="col-md-9 col-sm-6 col-xs-12">
                                    <textarea id="keterangan_pelaksanaan" name="keterangan_pelaksanaan" required="required" class="form-control col-md-7 col-xs-12"><?php echo $keterangan_pelaksanaan; ?></textarea>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-md-2 col-sm-3 col-xs-12">Tanggal Pelaksanaan <span class="required">*</span></label>
                                <div class="col-md-2 col-sm-6 col-xs-12">
                                    <input type="date" id="tgl_pelaksanaan" name="tgl_pelaksanaan" required="required" class="form-control col-md-7 col-xs-12" value="<?php echo $tgl_pelaksanaan; ?>">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-md-2 col-sm-3 col-xs-12">Semester/Waktu <span class="required">*</span></label>
                                <div class="col-md-4 col-sm-6 col-xs-12">
                                    <select id="semester_waktu" name="semester_waktu" required="required" class="form-control col-md-7 col-xs-12" value="<?php echo $semester_waktu; ?>">
                                        <option value="">--</option>
                                        <option value="Semester Ganjil 2016/2017" <?php if ($semester_waktu == 'Semester Ganjil 2016/2017') echo 'Selected'; ?>>Semester Ganjil 2016/2017</option>
                                        <option value="Semester Genap 2016/2017" <?php if ($semester_waktu == 'Semester Genap 2016/2017') echo 'Selected'; ?>>Semester Genap 2016/2017</option>
                                        <option value="Semester Ganjil 2017/2018" <?php if ($semester_waktu == 'Semester Ganjil 2017/2018') echo 'Selected'; ?>>Semester Ganjil 2017/2018</option>
                                        <option value="Semester Genap 2017/2018" <?php if ($semester_waktu == 'Semester Genap 2017/2018') echo 'Selected'; ?>>Semester Genap 2017/2018</option>
                                        <option value="Semester Ganjil 2018/2019" <?php if ($semester_waktu == 'Semester Ganjil 2018/2019') echo 'Selected'; ?>>Semester Ganjil 2018/2019</option>
                                        <option value="Semester Genap 2018/2019" <?php if ($semester_waktu == 'Semester Genap 2018/2019') echo 'Selected'; ?>>Semester Genap 2018/2019</option>
                                        <option value="Semester Ganjil 2019/2020" <?php if ($semester_waktu == 'Semester Ganjil 2019/2020') echo 'Selected'; ?>>Semester Ganjil 2019/2020</option>
                                        <option value="Semester Genap 2019/2020" <?php if ($semester_waktu == 'Semester Genap 2019/2020') echo 'Selected'; ?>>Semester Genap 2019/2020</option>
                                        <option value="Semester Ganjil 2020/2021" <?php if ($semester_waktu == 'Semester Ganjil 2020/2021') echo 'Selected'; ?>>Semester Ganjil 2020/2021</option>
                                        <option value="Semester Genap 2020/2021" <?php if ($semester_waktu == 'Semester Genap 2020/2021') echo 'Selected'; ?>>Semester Genap 2020/2021</option>
                                        <option value="Semester Ganjil 2021/2022" <?php if ($semester_waktu == 'Semester Ganjil 2021/2022') echo 'Selected'; ?>>Semester Ganjil 2021/2022</option>
                                        <option value="Semester Genap 2021/2022" <?php if ($semester_waktu == 'Semester Genap 2021/2022') echo 'Selected'; ?>>Semester Genap 2021/2022</option>
                                        <option value="Semester Ganjil 2022/2023" <?php if ($semester_waktu == 'Semester Ganjil 2022/2023') echo 'Selected'; ?>>Semester Ganjil 2022/2023</option>
                                        <option value="Semester Genap 2022/2023" <?php if ($semester_waktu == 'Semester Genap 2022/2023') echo 'Selected'; ?>>Semester Genap 2022/2023</option>

                                    </select>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-md-2 col-sm-3 col-xs-12">Satuan Hasil <span class="required">*</span></label>
                                <div class="col-md-4 col-sm-6 col-xs-12">
                                    <select id="satuan_hasil" name="satuan_hasil" required="required" class="form-control col-md-7 col-xs-12" value="<?php echo $satuan_hasil; ?>">
                                        <option value="">--</option>
                                        <option value="SKS" <?php if ($satuan_hasil == 'SKS') echo 'Selected'; ?>>SKS</option>
                                        <option value="Semester" <?php if ($satuan_hasil == 'Semester') echo 'Selected'; ?>>Semester</option>
                                        <option value="Mahasiswa" <?php if ($satuan_hasil == 'Mahasiswa') echo 'Selected'; ?>>Mahasiswa</option>
                                        <option value="Kegiatan" <?php if ($satuan_hasil == 'Kegiatan') echo 'Selected'; ?>>Kegiatan</option>
                                        <option value="Produk" <?php if ($satuan_hasil == 'Produk') echo 'Selected'; ?>>Produk</option>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-md-2 col-sm-3 col-xs-12">Jumlah Volume Dosen <span class="required">*</span></label>
                                <div class="col-md-2 col-sm-6 col-xs-12">
                                    <input type="number" id="jumlah_volume_dosen" name="jumlah_volume_dosen" required="required" class="form-control col-md-7 col-xs-12" value="<?php echo $jumlah_volume_dosen; ?>" autofocus>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-md-2 col-sm-3 col-xs-12">Keterangan&ensp;</label>
                                <div class="col-md-9 col-sm-6 col-xs-12">
                                    <textarea id="keterangan" name="keterangan" class="form-control col-md-7 col-xs-12"><?php echo $keterangan; ?></textarea>
                                </div>
                            </div>
                            <div class="form-group">
                                <?php
                                if ($bukti_file != NULL) {
                                    echo "File Uploaded: " . $bukti_file;
                                }
                                ?>
                                <label class="control-label col-md-2 col-sm-3 col-xs-12">Bukti File <span class="required">*</span></label>
                                <div class="col-md-4 col-sm-6 col-xs-12">
                                    <input type="file" id="bukti_file" name="bukti_file" required="required" class="form-control col-md-7 col-xs-12">
                                    <p><b>PENTING!<br>Format bukti dokumen/file dapat diunduh dengan mengunjungi <a class="linktautan" href="https://drive.google.com/drive/folders/10CqPjOn7T_YX2Tsh9aQpOW9Nu6wnTy7m?usp=share_link"> Tautan Berikut</a><b></p>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-2 col-sm-offset-3">
                                    <button type="submit" class="btn btn-success"><i class="fa fa-save"></i> <?php echo $button ?></button>
                                    <?php
                                    if (!empty($id_pel_pendidikan)) { ?>
                                        <button class="btn btn-danger delete" type="button" data-url="<?php echo base_url('pelpendidikan/delete/' . $id_usulan . '/' . $id_pel_pendidikan) ?>"><i class="fa fa-trash"></i> Delete</button>
                                    <?php } ?>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>