<div class="right_col" role="main">
    <div class="">
        <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                    <div class="x_title">
                        <h2><?php echo $title ?></h2>
                        <div class="clearfix"></div>
                    </div>
                    <div class="x_content">
                        <form id="applications" data-parsley-validate class="form-horizontal form-label-left" action="<?php echo $action; ?>" method="post" enctype="multipart/formdata">
                            <!-- <div class="form-group"> -->
                            <!-- <php
                                if ($foto_profile != "") {
                                ?>
                                    <img src="/asset/profile/<php echo $foto_profile; ?>">
                                <php
                                }
                                ?> -->
                            <!-- <label class="control-label col-md-3 col-sm-3 col-xs-12">Foto Profile Anda&ensp;</label>
                                <div class="col-md-4 col-sm-6 col-xs-12">
                                    <input type="file" id="foto_profile" name="foto_profile" class="form-control col-md-7 col-xs-12">
                                </div> -->
                            <!-- </div> -->
                            <div class="form-group">
                                <label class="control-label col-md-3 col-sm-3 col-xs-12">NIK Anda <span class="required">*</span></label>
                                <div class="col-md-4 col-sm-6 col-xs-12">
                                    <input type="text" id="nik_dosen" name="nik_dosen" required="required" class="form-control col-md-7 col-xs-12" value="<?php echo $nik_dosen; ?>" disabled>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-md-3 col-sm-3 col-xs-12">Nama Anda <span class="required">*</span></label>
                                <div class="col-md-4 col-sm-6 col-xs-12">
                                    <input type="text" id="nama" name="nama" required="required" class="form-control col-md-7 col-xs-12" value="<?php echo $nama; ?>" placeholder="Nama lengkap beserta title pangkat">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-md-3 col-sm-3 col-xs-12">Email Anda <span class="required">*</span></label>
                                <div class="col-md-4 col-sm-6 col-xs-12">
                                    <input type="email" id="email" name="email" required="required" class="form-control col-md-7 col-xs-12" value="<?php echo $email; ?>" placeholder="Email aktif dan resmi">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-md-3 col-sm-3 col-xs-12">No HP Anda <span class="required">*</span></label>
                                <div class="col-md-4 col-sm-6 col-xs-12">
                                    <input type="text" id="no_hp" name="no_hp" required="required" class="form-control col-md-7 col-xs-12" value="<?php echo $no_hp; ?>" pattern="\+62?(\d+)+|\(\d+\)" maxlength="15" placeholder="Format: +62.... (max 15 karakter)">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-md-3 col-sm-3 col-xs-12">Role Anda <span class="required">*</span></label>
                                <div class="col-md-4 col-sm-6 col-xs-12">
                                    <input type="text" id="role" name="role" class="form-control col-md-7 col-xs-12" value="<?php echo $role; ?>" readonly>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-md-3 col-sm-3 col-xs-12">Tempat Lahir Anda <span class="required">*</span></label>
                                <div class="col-md-4 col-sm-6 col-xs-12">
                                    <input type="text" id="tempat_lahir" name="tempat_lahir" class="form-control col-md-7 col-xs-12" value="<?php echo $tempat_lahir; ?>">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-md-3 col-sm-3 col-xs-12">Tanggal Lahir Anda <span class="required">*</span></label>
                                <div class="col-md-4 col-sm-6 col-xs-12">
                                    <input type="date" id="tgl_lahir" name="tgl_lahir" class="form-control col-md-7 col-xs-12" value="<?php echo $tgl_lahir; ?>">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-md-3 col-sm-3 col-xs-12">Jenis Kelamin Anda <span class="required">*</span></label>
                                <div class="col-md-4 col-sm-6 col-xs-12">
                                    <select id="jenis_kelamin" name="jenis_kelamin" required="required" class="form-control col-md-7 col-xs-12" value="<?php echo $jenis_kelamin; ?>">
                                        <option value="">--</option>
                                        <option value="Laki-laki" <?php if ($jenis_kelamin == 'Laki-laki') echo 'Selected'; ?>>Laki-laki</option>
                                        <option value="Perempuan" <?php if ($jenis_kelamin == 'Perempuan') echo 'Selected'; ?>>Perempuan</option>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-md-3 col-sm-3 col-xs-12">Jurusan Anda&ensp;</label>
                                <div class="col-md-6 col-sm-6 col-xs-12">
                                    <input type="text" id="jurusan" name="jurusan" class="form-control col-md-7 col-xs-12" value="<?php echo $jurusan; ?>">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-md-3 col-sm-3 col-xs-12">Prodi Anda&ensp;</label>
                                <div class="col-md-6 col-sm-6 col-xs-12">
                                    <input type="text" id="prodi" name="prodi" class="form-control col-md-7 col-xs-12" value="<?php echo $prodi; ?>" placeholder="Program Studi">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-md-3 col-sm-3 col-xs-12">Jenjang Pendidikan Anda&ensp;</label>
                                <div class="col-md-6 col-sm-6 col-xs-12">
                                    <input type="text" id="jenjang_pendidikan" name="jenjang_pendidikan" class="form-control col-md-7 col-xs-12" value="<?php echo $jenjang_pendidikan; ?>" placeholder="cth: S1/2/3 ... sederajat">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-md-3 col-sm-3 col-xs-12">Jabatan Fungsional Anda&ensp;</label>
                                <div class="col-md-6 col-sm-6 col-xs-12">
                                    <input type="text" id="jabatan_fungsional" name="jabatan_fungsional" class="form-control col-md-7 col-xs-12" value="<?php echo $jabatan_fungsional; ?>">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-md-3 col-sm-3 col-xs-12">Bidang Ilmu Anda&ensp;</label>
                                <div class="col-md-6 col-sm-6 col-xs-12">
                                    <input type="text" id="bidang_ilmu" name="bidang_ilmu" class="form-control col-md-7 col-xs-12" value="<?php echo $bidang_ilmu; ?>">
                                </div>
                            </div>
                            <!-- <div class="form-group">
                                <label class="control-label col-md-3 col-sm-3 col-xs-12">Foto Profile Dosen</label>
                                <div class="col-md-4 col-sm-6 col-xs-12">
                                    <input type="file" id="foto_profile" name="foto_profile" class="form-control col-md-7 col-xs-12" value="<php echo $foto_profile; ?>">
                                </div>
                            </div> -->
                            <div class="form-group">
                                <label class="control-label col-md-3 col-sm-3 col-xs-12">NIDN Anda&ensp;</label>
                                <div class="col-md-4 col-sm-6 col-xs-12">
                                    <input type="text" id="nidn" name="nidn" class="form-control col-md-7 col-xs-12" value="<?php echo $nidn; ?>" placeholder="Nomor Induk Dosen Nasional">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-md-3 col-sm-3 col-xs-12">NIP Anda&ensp;</label>
                                <div class="col-md-4 col-sm-6 col-xs-12">
                                    <input type="text" id="nip" name="nip" class="form-control col-md-7 col-xs-12" value="<?php echo $nip; ?>" placeholder="Nomor Induk Pegawai">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-md-3 col-sm-3 col-xs-12">Pangkat/Golongan Anda&ensp;</label>
                                <div class="col-md-6 col-sm-6 col-xs-12">
                                    <input type="text" id="pangkat_golongan" name="pangkat_golongan" class="form-control col-md-7 col-xs-12" value="<?php echo $pangkat_golongan; ?>">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-md-3 col-sm-3 col-xs-12">Masa Golongan Lama Anda&ensp;</label>
                                <div class="col-md-6 col-sm-6 col-xs-12">
                                    <input type="text" id="masa_gol_lama" name="masa_gol_lama" class="form-control col-md-7 col-xs-12" value="<?php echo $masa_gol_lama; ?>">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-md-3 col-sm-3 col-xs-12">Masa Golongan Baru Anda&ensp;</label>
                                <div class="col-md-6 col-sm-6 col-xs-12">
                                    <input type="text" id="masa_gol_baru" name="masa_gol_baru" class="form-control col-md-7 col-xs-12" value="<?php echo $masa_gol_baru; ?>">
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3 col-sm-offset-3">
                                    <button type="submit" class="btn btn-success"><i class="fa fa-save"></i> Update</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>