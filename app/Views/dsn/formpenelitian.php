<head>
    <style>
        .linktautan {
            /* font-size: 12pt; */
            color: black;
            text-decoration: underline;
        }

        .linktautan:hover {
            color: red;
        }

        .linktautan:link {
            color: blue;
        }

        .linktautan:active {
            color: green;
        }

        .linktautan:visited {
            background: yellow;
        }
    </style>
</head>

<div class="right_col" role="main">
    <div class="">
        <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                    <div class="x_title">
                        <h2><?php echo $title ?></h2>
                        <div class="clearfix"></div>
                    </div>
                    <div class="x_content">
                        <form id="applications" data-parsley-validate class="form-horizontal form-label-left" action="<?php echo $action; ?>" method="post">
                            <!-- <div class="form-group">
                                <label class="control-label col-md-2 col-sm-3 col-xs-12">ID Usulan <span class="required">*</span></label>
                                <div class="col-md-2 col-sm-6 col-xs-12">
                                    <input type="text" id="id_usulan" name="id_usulan" required="required" class="form-control col-md-7 col-xs-12" value="<php echo $id_usulan; ?>">
                                </div>
                            </div> -->
                            <div class="form-group">
                                <label class="control-label col-md-2 col-sm-3 col-xs-12">Jenis Penelitian <span class="required">*</span></label>
                                <div class="col-md-9 col-sm-6 col-xs-12">
                                    <textarea id="jenis_pelaksanaan" name="jenis_pelaksanaan" required="required" class="form-control col-md-7 col-xs-12"><?php echo "$jenis_pelaksanaan"; ?></textarea>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-md-2 col-sm-3 col-xs-12">Jenis Hasil Penelitian <span class="required">*</span></label>
                                <div class="col-md-9 col-sm-6 col-xs-12">
                                    <textarea id="jenis_hasil_pelaksanaan" name="jenis_hasil_pelaksanaan" required="required" class="form-control col-md-7 col-xs-12"><?php echo $jenis_hasil_pelaksanaan; ?></textarea>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-md-2 col-sm-3 col-xs-12">Detail Penelitian <span class="required">*</span></label>
                                <div class="col-md-9 col-sm-6 col-xs-12">
                                    <textarea id="detail_pelaksanaan" name="detail_pelaksanaan" required="required" class="form-control col-md-7 col-xs-12"><?php echo $detail_pelaksanaan; ?></textarea>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-md-2 col-sm-3 col-xs-12">Tanggal Pelaksanaan <span class="required">*</span></label>
                                <div class="col-md-2 col-sm-6 col-xs-12">
                                    <input type="date" id="tgl_penelitian" name="tgl_penelitian" required="required" class="form-control col-md-7 col-xs-12" value="<?php echo $tgl_penelitian; ?>">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-md-2 col-sm-3 col-xs-12">Satuan Hasil <span class="required">*</span></label>
                                <div class="col-md-4 col-sm-6 col-xs-12">
                                    <input type="text" id="satuan_hasil" name="satuan_hasil" required="required" class="form-control col-md-7 col-xs-12" value="<?php echo $satuan_hasil; ?>">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-md-2 col-sm-3 col-xs-12">Jumlah Volume Dosen <span class="required">*</span></label>
                                <div class="col-md-2 col-sm-6 col-xs-12">
                                    <input type="number" id="jumlah_volume_dosen" name="jumlah_volume_dosen" required="required" class="form-control col-md-7 col-xs-12" value="<?php echo $jumlah_volume_dosen; ?>" autofocus>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-md-2 col-sm-3 col-xs-12">Keterangan&ensp;</label>
                                <div class="col-md-9 col-sm-6 col-xs-12">
                                    <textarea id="keterangan" name="keterangan" class="form-control col-md-7 col-xs-12"><?php echo $keterangan; ?></textarea>
                                </div>
                            </div>
                            <div class="form-group">
                                <?php
                                if ($bukti_file != NULL) {
                                    echo "File Uploaded: " . $bukti_file;
                                }
                                ?>
                                <label class="control-label col-md-2 col-sm-3 col-xs-12">Bukti File <span class="required">*</span></label>
                                <div class="col-md-4 col-sm-6 col-xs-12">
                                    <input type="file" id="bukti_file" name="bukti_file" required="required" class="form-control col-md-7 col-xs-12">
                                    <p><b>PENTING!<br>Format bukti dokumen/file dapat diunduh dengan mengunjungi <a class="linktautan" href="https://drive.google.com/drive/folders/10CqPjOn7T_YX2Tsh9aQpOW9Nu6wnTy7m?usp=share_link"> Tautan Berikut</a><b></p>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-2 col-sm-offset-3">
                                    <button type="submit" class="btn btn-success"><i class="fa fa-save"></i> <?php echo $button ?></button>
                                    <?php
                                    if (!empty($id_penelitian)) { ?>
                                        <button class="btn btn-danger delete" type="button" data-url="<?php echo base_url('penelitian/delete/' . $id_usulan . '/' . $id_penelitian) ?>"><i class="fa fa-trash"></i> Delete</button>
                                    <?php } ?>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>