<div class="right_col" role="main">
    <div class="">
        <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                    <div class="x_title">
                        <h2><?php echo $title ?></h2>
                        <div class="clearfix"></div>
                    </div>
                    <div class="x_content">
                        <form id="applications" data-parsley-validate class="form-horizontal form-label-left" action="<?php echo $action; ?>" method="post">
                            <!-- <div class="form-group">
                                <label class="control-label col-md-3 col-sm-3 col-xs-12">NIK Pengaju <span class="required">*</span></label>
                                <div class="col-md-2 col-sm-6 col-xs-12">
                                    <input type="text" id="nik_dosen" name="nik_dosen" required="required" class="form-control col-md-7 col-xs-12" value="<php echo $nik_dosen; ?>">
                                </div>
                            </div> -->
                            <div class="form-group">
                                <label class="control-label col-md-3 col-sm-3 col-xs-12">Jenis Ajuan <span class="required">*</span></label>
                                <div class="col-md-4 col-sm-6 col-xs-12">
                                    <select id="jenis_usulan" name="jenis_usulan" required="required" class="form-control col-md-7 col-xs-12" value="<?php echo $jenis_usulan; ?>">
                                        <option value="">--</option>
                                        <option value="Asisten Ahli" <?php if ($jenis_usulan == 'Asisten Ahli') echo 'Selected'; ?>>Asisten Ahli</option>
                                        <option value="Lektor" <?php if ($jenis_usulan == 'Lektor') echo 'Selected'; ?>>Lektor</option>
                                    </select>
                                </div>
                            </div>
                            <!-- <div class="form-group">
                                <label class="control-label col-md-3 col-sm-3 col-xs-12">Tanggal Ajuan <span class="required">*</span></label>
                                <div class="col-md-3 col-sm-6 col-xs-12">
                                    <input type="text" id="tgl_usulan" name="tgl_usulan" class="form-control col-md-7 col-xs-12" value="<php echo $tgl_usulan; ?>">
                                </div>
                            </div> -->
                            <!-- <div class="form-group">
                                <label class="control-label col-md-3 col-sm-3 col-xs-12">Status <span class="required">*</span></label>
                                <div class="col-md-2 col-sm-6 col-xs-12">
                                    <input type="text" id="status_usulan" name="status_usulan" required="required" class="form-control col-md-7 col-xs-12" value="<php echo $status_usulan; ?>">
                                </div>
                            </div> -->
                            <div class="form-group">
                                <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3 col-sm-offset-3">
                                    <button type="submit" class="btn btn-success"><i class="fa fa-save"></i> <?php echo $button ?></button>
                                    <?php
                                    if (!empty($id_usulan)) { ?>
                                        <button class="btn btn-danger delete" type="button" data-url="<?php echo base_url('usulan/delete/' . $id_usulan) ?>"><i class="fa fa-trash"></i> Delete</button>
                                    <?php } ?>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>