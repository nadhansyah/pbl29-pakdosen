<head>
    <style>
        .linktautan {
            /* font-size: 12pt; */
            color: black;
            text-decoration: underline;
        }

        .linktautan:hover {
            color: red;
        }

        .linktautan:link {
            color: blue;
        }

        .linktautan:active {
            color: green;
        }

        .linktautan:visited {
            background: yellow;
        }
    </style>
</head>

<div class="right_col" role="main">
    <div class="">
        <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                    <div class="x_title">
                        <h2><?php echo $title ?></h2>
                        <div class="clearfix"></div>
                    </div>
                    <div class="x_content">
                        <form id="applications" data-parsley-validate class="form-horizontal form-label-left" action="<?php echo $action; ?>" method="post">
                            <!-- <div class="form-group">
                                <label class="control-label col-md-2 col-sm-2 col-xs-12">ID Usulan <span class="required">*</span></label>
                                <div class="col-md-5 col-sm-9 col-xs-12">
                                    <input type="text" id="id_usulan" name="id_usulan" required="required" class="form-control col-md-7 col-xs-12" value="<php echo $id_usulan; ?>">
                                </div>
                            </div> -->
                            <div class="form-group">
                                <label class="control-label col-md-2 col-sm-2 col-xs-12">Jenis Pengabdian <span class="required">*</span></label>
                                <div class="col-md-9 col-sm-9 col-xs-12">
                                    <select style="height: 75px; white-space:pre-wrap" id="jenis_pelaksanaan" name="jenis_pelaksanaan" required="required" class="form-control col-md-7 col-xs-12" value="<?php echo $jenis_pelaksanaan; ?>">
                                        <option value="">--</option>
                                        <option value="A - Menduduki jabatan pimpinan" <?php if ($jenis_pelaksanaan == 'A - Menduduki jabatan pimpinan') echo 'Selected'; ?>>A - Menduduki jabatan pimpinan</option>
                                        <option value="B - Melaksankan pengembangan hasil pendidikan dan penelitian" <?php if ($jenis_pelaksanaan == 'B - Melaksankan pengembangan hasil pendidikan dan penelitian') echo 'Selected'; ?>>B - Melaksankan pengembangan hasil pendidikan dan penelitian</option>
                                        <option value="C - Memberi latihan/ penyuluhan/ penataran/ ceramah pada masyarakat" <?php if ($jenis_pelaksanaan == 'C - Memberi latihan/penyuluhan/ penataran/ ceramah pada masyarakat') echo 'Selected'; ?>>C - Memberi latihan/penyuluhan/ penataran/ ceramah pada masyarakat</option>
                                        <option value="D - Memberi pelayanan kepada masyarakat atau kegiatan lain yang menunjang pelaksanaan tugas umum pemerintah dan pembangunan" <?php if ($jenis_pelaksanaan == 'D - Memberi pelayanan kepada masyarakat atau kegiatan lain yang menunjang pelaksanaan tugas umum pemerintah dan pembangunan') echo 'Selected'; ?>>D - Memberi pelayanan kepada masyarakat atau kegiatan lain yang menunjang pelaksanaan tugas umum pemerintah dan pembangunan</option>
                                        <option value="E - Membuat/menulis karya pengabdian" <?php if ($jenis_pelaksanaan == 'E - Membuat/menulis karya pengabdian') echo 'Selected'; ?>>E - Membuat/menulis karya pengabdian</option>
                                        <option value="F - Hasil kegiatan pengabdian kepada masyarakat yang dipublikasikan di sebuah berkala/ jurnal pengabdian kepada masyarakat atau teknologi tepat  merupakan diseminasi dari luaran program kegiatan pengabdian kepada masyarakat tiap karya " <?php if ($jenis_pelaksanaan == 'F - Hasil kegiatan pengabdian kepada masyarakat yang dipublikasikan di sebuah berkala/ jurnal pengabdian kepada masyarakat atau teknologi tepat  merupakan diseminasi dari luaran program kegiatan pengabdian kepada masyarakat tiap karya ') echo 'Selected'; ?>>F - Hasil kegiatan pengabdian kepada masyarakat yang dipublikasikan di sebuah berkala/ jurnal pengabdian kepada masyarakat atau teknologi tepat merupakan diseminasi dari luaran program kegiatan pengabdian kepada masyarakat tiap karya </option>
                                        <option value="G - Berperan aktif dalam pengelolaan jurnal ilmiah (per tahun sebagai editor /dewan penyunting/dewan redaksi jurnal ilmiah)" <?php if ($jenis_pelaksanaan == 'G - Berperan aktif dalam pengelolaan jurnal ilmiah (per tahun sebagai editor /dewan penyunting/dewan redaksi jurnal ilmiah)') echo 'Selected'; ?>>G - Berperan aktif dalam pengelolaan jurnal ilmiah (per tahun sebagai editor /dewan penyunting/dewan redaksi jurnal ilmiah)</option>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-md-2 col-sm-2 col-xs-12">Detail Pengabdian <span class="required">*</span></label>
                                <div class="col-md-9 col-sm-9 col-xs-12">
                                    <select style="height: 50px; white-space:pre-wrap" id="detail_pelaksanaan" name="detail_pelaksanaan" required="required" class="form-control col-md-7 col-xs-12" value="<?php echo $detail_pelaksanaan; ?>">
                                        <option value="">--</option>
                                        <option value="Menduduki jabatan pimpinan pada lembaga pemerintahan/ pejabat negara yang harus dibebaskan dari jabatan organiknya" <?php if ($detail_pelaksanaan == 'Menduduki jabatan pimpinan pada lembaga pemerintahan/ pejabat negara yang harus dibebaskan dari jabatan organiknya') echo 'Selected'; ?>>Menduduki jabatan pimpinan pada lembaga pemerintahan/ pejabat negara yang harus dibebaskan dari jabatan organiknya</option>
                                        <option value="Melaksanakan pengembangan hasil pendidikan dan penelitian yang dapat dimanfaatkan oleh masyarakat" <?php if ($detail_pelaksanaan == 'Melaksanakan pengembangan hasil pendidikan dan penelitian yang dapat dimanfaatkan oleh masyarakat') echo 'Selected'; ?>>Melaksanakan pengembangan hasil pendidikan dan penelitian yang dapat dimanfaatkan oleh masyarakat</option>
                                        <option value="1. Terjadwal/terprogram dalam satu semester/lebih" <?php if ($detail_pelaksanaan == '1. Terjadwal/terprogram dalam satu semester/lebih') echo 'Selected'; ?>>1. Terjadwal/terprogram dalam satu semester/lebih</option>
                                        <option value="2. Terjadwal/terprogram kurang dari satu semester dan minimal satu bulan" <?php if ($detail_pelaksanaan == '2. Terjadwal/terprogram kurang dari satu semester dan minimal satu bulan') echo 'Selected'; ?>>2. Terjadwal/terprogram kurang dari satu semester dan minimal satu bulan</option>
                                        <option value="1. Berdasarkan bidang keahlian" <?php if ($detail_pelaksanaan == '1. Berdasarkan bidang keahlian') echo 'Selected'; ?>>1. Berdasarkan bidang keahlian</option>
                                        <option value="2. Berdasarkan penugasan lembaga perguruan tinggi" <?php if ($detail_pelaksanaan == '2. Berdasarkan penugasan lembaga perguruan tinggi') echo 'Selected'; ?>>2. Berdasarkan penugasan lembaga perguruan tinggi</option>
                                        <option value="3. Berdasarkan fungsi/ jabatan" <?php if ($detail_pelaksanaan == '3. Berdasarkan fungsi/ jabatan') echo 'Selected'; ?>>3. Berdasarkan fungsi/ jabatan</option>
                                        <option value="Membuat/menulis karya pengabdian pada masyarakat yang tidak dipublikasikan" <?php if ($detail_pelaksanaan == 'Membuat/menulis karya pengabdian pada masyarakat yang tidak dipublikasikan') echo 'Selected'; ?>>Membuat/menulis karya pengabdian pada masyarakat yang tidak dipublikasikan</option>
                                        <option value="1. Jurnal imliah internasional" <?php if ($detail_pelaksanaan == '1. Jurnal imliah internasional') echo 'Selected'; ?>>1. Jurnal imliah internasional</option>
                                        <option value="2. Jurnal ilmiah nasional" <?php if ($detail_pelaksanaan == '2. Jurnal ilmiah nasional') echo 'Selected'; ?>>2. Jurnal ilmiah nasional</option>

                                    </select>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-md-2 col-sm-2 col-xs-12">Keterangan Pengabdian <span class="required">*</span></label>
                                <div class="col-md-5 col-sm-9 col-xs-12">
                                    <select id="keterangan_pelaksanaan" name="keterangan_pelaksanaan" required="required" class="form-control col-md-7 col-xs-12" value="<?php echo $keterangan_pelaksanaan; ?>">
                                        <option value="">--</option>
                                        <option value="a) Tingkat internasional" <?php if ($keterangan_pelaksanaan == 'a) Tingkat internasional') echo 'Selected'; ?>>a) Tingkat internasional</option>
                                        <option value="b) Tingkat nasional" <?php if ($keterangan_pelaksanaan == 'b) Tingkat nasional') echo 'Selected'; ?>>b) Tingkat nasional</option>
                                        <option value="c) Tingkat lokal" <?php if ($keterangan_pelaksanaan == 'c) Tingkat lokal') echo 'Selected'; ?>>c) Tingkat lokal</option>
                                        <option value="d) Insidental" <?php if ($keterangan_pelaksanaan == 'd) Insidental') echo 'Selected'; ?>>d) Insidental</option>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-md-2 col-sm-2 col-xs-12">Tanggal Pengabdian <span class="required">*</span></label>
                                <div class="col-md-5 col-sm-9 col-xs-12">
                                    <input type="date" id="tgl_pengabdian" name="tgl_pengabdian" required="required" class="form-control col-md-7 col-xs-12" value="<?php echo $tgl_pengabdian; ?>">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-md-2 col-sm-2 col-xs-12">Satuan Hasil <span class="required">*</span></label>
                                <div class="col-md-5 col-sm-9 col-xs-12">
                                    <select id="satuan_hasil" name="satuan_hasil" required="required" class="form-control col-md-7 col-xs-12" value="<?php echo $satuan_hasil; ?>">
                                        <option value="">--</option>
                                        <option value="Tiap semester" <?php if ($satuan_hasil == 'Tiap semester') echo 'Selected'; ?>>Tiap semester</option>
                                        <option value="Tiap program" <?php if ($satuan_hasil == 'Tiap program') echo 'Selected'; ?>>Tiap program</option>
                                        <option value="Per program/ semester" <?php if ($satuan_hasil == 'Per program/ semester') echo 'Selected'; ?>>Per program/ semester</option>
                                        <option value="Per karya" <?php if ($satuan_hasil == 'Per karya') echo 'Selected'; ?>>Per karya</option>
                                        <option value="Tiap tahun" <?php if ($satuan_hasil == 'Tiap tahun') echo 'Selected'; ?>>Tiap tahun</option>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-md-2 col-sm-2 col-xs-12">Jumlah Volume Dosen <span class="required">*</span></label>
                                <div class="col-md-5 col-sm-9 col-xs-12">
                                    <input type="number" id="jumlah_volume_dosen" name="jumlah_volume_dosen" required="required" class="form-control col-md-7 col-xs-12" value="<?php echo $jumlah_volume_dosen; ?>" autofocus>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-md-2 col-sm-2 col-xs-12">Keterangan&ensp;</label>
                                <div class="col-md-5 col-sm-9 col-xs-12">
                                    <textarea id="keterangan" name="keterangan" class="form-control col-md-7 col-xs-12"><?php echo $keterangan; ?></textarea>
                                </div>
                            </div>
                            <div class="form-group">
                                <?php
                                if ($bukti_file != NULL) {
                                    echo "File Uploaded: " . $bukti_file;
                                }
                                ?>
                                <label class="control-label col-md-2 col-sm-2 col-xs-12">Bukti File <span class="required">*</span></label>
                                <div class="col-md-5 col-sm-9 col-xs-12">
                                    <input type="file" id="bukti_file" name="bukti_file" required="required" class="form-control col-md-7 col-xs-12">
                                    <p><b>PENTING!<br>Format bukti dokumen/file dapat diunduh dengan mengunjungi <a class="linktautan" href="https://drive.google.com/drive/folders/10CqPjOn7T_YX2Tsh9aQpOW9Nu6wnTy7m?usp=share_link"> Tautan Berikut</a><b></p>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-2 col-sm-offset-2">
                                    <button type="submit" class="btn btn-success"><i class="fa fa-save"></i> <?php echo $button ?></button>
                                    <?php
                                    if (!empty($id_pengabdian)) { ?>
                                        <button class="btn btn-danger delete" type="button" data-url="<?php echo base_url('pengabdian/delete/' . $id_usulan . '/' . $id_pengabdian) ?>"><i class="fa fa-trash"></i> Delete</button>
                                    <?php } ?>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script>
    var optionText, array = [],
        newString, maxChar = 40;
    $('select').each(function() {
        $(this).find('option').each(function(i, e) {
            $(e).attr('title', $(e).text());
            optionText = $(e).text();
            newString = '';
            if (optionText.length > maxChar) {
                array = optionText.split(' ');
                $.each(array, function(ind, ele) {
                    newString += ele + ' ';
                    if (ind > 0 && newString.length > maxChar) {
                        $(e).text(newString);
                        return false;
                    }
                });

            }
        });
    });
</script>