<head>
    <style>
        .linktautan {
            /* font-size: 12pt; */
            color: black;
            text-decoration: underline;
        }

        .linktautan:hover {
            color: red;
        }

        .linktautan:link {
            color: blue;
        }

        .linktautan:active {
            color: green;
        }

        .linktautan:visited {
            background: yellow;
        }
    </style>
</head>

<div class="right_col" role="main">
    <div class="">
        <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                    <div class="x_title">
                        <h2><?php echo $title ?></h2>
                        <div class="clearfix"></div>
                    </div>
                    <div class="x_content">
                        <form id="applications" data-parsley-validate class="form-horizontal form-label-left" action="<?php echo $action; ?>" method="post">
                            <!-- <div class="form-group">
                                <label class="control-label col-md-2 col-sm-3 col-xs-12">ID Usulan <span class="required">*</span></label>
                                <div class="col-md-5 col-sm-8 col-xs-12">
                                    <input type="text" id="id_usulan" name="id_usulan" required="required" class="form-control col-md-7 col-xs-12" value="<php echo $id_usulan; ?>">
                                </div>
                            </div> -->
                            <div class="form-group">
                                <label class="control-label col-md-2 col-sm-3 col-xs-12">Jenis Penunjang <span class="required">*</span></label>
                                <div class="col-md-9 col-sm-8 col-xs-12">
                                    <select id="jenis_pelaksanaan" name="jenis_pelaksanaan" required="required" class="form-control col-md-7 col-xs-12" value="<?php echo $jenis_pelaksanaan; ?>">
                                        <option value="">--</option>
                                        <option value="A - Menjadi anggota dalam suatu Panitia/Badan pada perguruan tinggi" <?php if ($jenis_pelaksanaan == 'A - Menjadi anggota dalam suatu Panitia/Badan pada perguruan tinggi') echo 'Selected'; ?>>A - Menjadi anggota dalam suatu Panitia/Badan pada perguruan tinggi</option>
                                        <option value="B - Menjadi anggota panitia/badan pada lembaga pemerintah" <?php if ($jenis_pelaksanaan == 'B - Menjadi anggota panitia/badan pada lembaga pemerintah') echo 'Selected'; ?>>B - Menjadi anggota panitia/badan pada lembaga pemerintah</option>
                                        <option value="C - Menjadi anggota organisasi profesi" <?php if ($jenis_pelaksanaan == 'C - Menjadi anggota organisasi profesi') echo 'Selected'; ?>>C - Menjadi anggota organisasi profesi</option>
                                        <option value="D - Mewakili perguruan tinggi/lembaga pemerintah" <?php if ($jenis_pelaksanaan == 'D - Mewakili perguruan tinggi/lembaga pemerintah') echo 'Selected'; ?>>D - Mewakili perguruan tinggi/lembaga pemerintah</option>
                                        <option value="E - Menjadi anggota delegasi nasional ke pertemuan internasional" <?php if ($jenis_pelaksanaan == 'E - Menjadi anggota delegasi nasional ke pertemuan internasional') echo 'Selected'; ?>>E - Menjadi anggota delegasi nasional ke pertemuan internasional</option>
                                        <option value="F - Berperan serta aktif dalam pertemuan ilmiah" <?php if ($jenis_pelaksanaan == 'F - Berperan serta aktif dalam pertemuan ilmiah') echo 'Selected'; ?>>F - Berperan serta aktif dalam pertemuan ilmiah</option>
                                        <option value="G - Mendapat penghargaan/ tanda jasa" <?php if ($jenis_pelaksanaan == 'G - Mendapat penghargaan/ tanda jasa') echo 'Selected'; ?>>G - Mendapat penghargaan/ tanda jasa</option>
                                        <option value="H - Menulis buku pelajaran SLTA ke bawah yang diterbitkan dan diedarkan secara nasional" <?php if ($jenis_pelaksanaan == 'H - Menulis buku pelajaran SLTA ke bawah yang diterbitkan dan diedarkan secara nasional') echo 'Selected'; ?>>H - Menulis buku pelajaran SLTA ke bawah yang diterbitkan dan diedarkan secara nasional</option>
                                        <option value="I - Mempunyai prestasi di bidang olahraga/humaniora" <?php if ($jenis_pelaksanaan == 'I - Mempunyai prestasi di bidang olahraga/humaniora') echo 'Selected'; ?>>I - Mempunyai prestasi di bidang olahraga/humaniora</option>
                                        <option value="J - Keanggotaan dalam tim penilaian " <?php if ($jenis_pelaksanaan == 'J - Keanggotaan dalam tim penilaian ') echo 'Selected'; ?>>J - Keanggotaan dalam tim penilaian </option>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-md-2 col-sm-3 col-xs-12">Detail Penunjang <span class="required">*</span></label>
                                <div class="col-md-9 col-sm-8 col-xs-12">
                                    <select id="detail_pelaksanaan" name="detail_pelaksanaan" required="required" class="form-control col-md-7 col-xs-12" value="<?php echo $detail_pelaksanaan; ?>">
                                        <option value="">--</option>
                                        <option value="1. Sebagai ketua/wakil ketua merangkap anggota tiap tahun" <?php if ($detail_pelaksanaan == '1. Sebagai ketua/wakil ketua merangkap anggota tiap tahun') echo 'Selected'; ?>>1. Sebagai ketua/wakil ketua merangkap anggota tiap tahun</option>
                                        <option value="2. Sebagai anggota tiap tahun" <?php if ($detail_pelaksanaan == '2. Sebagai anggota tiap tahun') echo 'Selected'; ?>>2. Sebagai anggota tiap tahun</option>
                                        <option value="1. Panitian pusat" <?php if ($detail_pelaksanaan == '1. Panitian pusat') echo 'Selected'; ?>>1. Panitian pusat</option>
                                        <option value="2. Panitian daerah" <?php if ($detail_pelaksanaan == '2. Panitian daerah') echo 'Selected'; ?>>2. Panitian daerah</option>
                                        <option value="1. Tingkat internasional" <?php if ($detail_pelaksanaan == '1. Tingkat internasional') echo 'Selected'; ?>>1. Tingkat internasional</option>
                                        <option value="2. Tingkat nasional" <?php if ($detail_pelaksanaan == '2. Tingkat nasional') echo 'Selected'; ?>>2. Tingkat nasional</option>
                                        <option value="Mewakili perguruan tinggi/ lembaga pemerintah duduk dalam panitia antar lembaga" <?php if ($detail_pelaksanaan == 'Mewakili perguruan tinggi/ lembaga pemerintah duduk dalam panitia antar lembaga') echo 'Selected'; ?>>Mewakili perguruan tinggi/ lembaga pemerintah duduk dalam panitia antar lembaga</option>
                                        <option value="1. Sebagai ketua delegasi" <?php if ($detail_pelaksanaan == '1. Sebagai ketua delegasi') echo 'Selected'; ?>>1. Sebagai ketua delegasi</option>
                                        <option value="2. Sebagai anggota delegasi" <?php if ($detail_pelaksanaan == '2. Sebagai anggota delegasi') echo 'Selected'; ?>>2. Sebagai anggota delegasi</option>
                                        <option value="1. Tingkat internasional/nasional/regional" <?php if ($detail_pelaksanaan == '1. Tingkat internasional/nasional/regional') echo 'Selected'; ?>>1. Tingkat internasional/nasional/regional</option>
                                        <option value="2. Di lingkungan perguruan tinggi " <?php if ($detail_pelaksanaan == '2. Di lingkungan perguruan tinggi ') echo 'Selected'; ?>>2. Di lingkungan perguruan tinggi </option>
                                        <option value="1. Penghargaan/ tanda jasa Satya Lencana Karya Satya" <?php if ($detail_pelaksanaan == '1. Penghargaan/ tanda jasa Satya Lencana Karya Satya') echo 'Selected'; ?>>1. Penghargaan/ tanda jasa Satya Lencana Karya Satya</option>
                                        <option value="2. Memperoleh penghargaan lainnya" <?php if ($detail_pelaksanaan == '2. Memperoleh penghargaan lainnya') echo 'Selected'; ?>>2. Memperoleh penghargaan lainnya</option>
                                        <option value="1. Buku SLTA atau setingkat " <?php if ($detail_pelaksanaan == '1. Buku SLTA atau setingkat ') echo 'Selected'; ?>>1. Buku SLTA atau setingkat </option>
                                        <option value="2. Buku SLTP atau setingkat" <?php if ($detail_pelaksanaan == '2. Buku SLTP atau setingkat') echo 'Selected'; ?>>2. Buku SLTP atau setingkat</option>
                                        <option value="3. Buku SD atau setingkat" <?php if ($detail_pelaksanaan == '3. Buku SD atau setingkat') echo 'Selected'; ?>>3. Buku SD atau setingkat</option>
                                        <option value="1. Tingkat internasional " <?php if ($detail_pelaksanaan == '1. Tingkat internasional ') echo 'Selected'; ?>>1. Tingkat internasional </option>
                                        <option value="2. Tingkat nasional" <?php if ($detail_pelaksanaan == '2. Tingkat nasional') echo 'Selected'; ?>>2. Tingkat nasional</option>
                                        <option value="3. Tingkat daerah/ lokal" <?php if ($detail_pelaksanaan == '3. Tingkat daerah/ lokal') echo 'Selected'; ?>>3. Tingkat daerah/ lokal</option>
                                        <option value="Menjadi anggota tim penilaian jabatan Akademik Dosen Politeknik Negeri Batam (Tiap semester)" <?php if ($detail_pelaksanaan == 'Menjadi anggota tim penilaian jabatan Akademik Dosen Politeknik Negeri Batam (Tiap semester)') echo 'Selected'; ?>>Menjadi anggota tim penilaian jabatan Akademik Dosen Politeknik Negeri Batam (Tiap semester)</option>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-md-2 col-sm-3 col-xs-12">Keterangan Penunjang <span class="required">*</span></label>
                                <div class="col-md-9 col-sm-8 col-xs-12">
                                    <select id="keterangan_pelaksanaan" name="keterangan_pelaksanaan" required="required" class="form-control col-md-7 col-xs-12" value="<?php echo $keterangan_pelaksanaan; ?>">
                                        <option value="">--</option>
                                        <option value="a) Ketua/ Wakil Ketua" <?php if ($keterangan_pelaksanaan == 'a) Ketua/ Wakil Ketua') echo 'Selected'; ?>>a) Ketua/ Wakil Ketua</option>
                                        <option value="b) Anggota" <?php if ($keterangan_pelaksanaan == 'b) Anggota') echo 'Selected'; ?>>b) Anggota</option>
                                        <option value="a) Pengurus, tiap periode jabatan" <?php if ($keterangan_pelaksanaan == 'a) Pengurus, tiap periode jabatan') echo 'Selected'; ?>>a) Pengurus, tiap periode jabatan</option>
                                        <option value="b) Anggota atas permintaan, tiap periode jabatan" <?php if ($keterangan_pelaksanaan == 'b) Anggota atas permintaan, tiap periode jabatan') echo 'Selected'; ?>>b) Anggota atas permintaan, tiap periode jabatan</option>
                                        <option value="c) Anggota, tiap periode jabatan" <?php if ($keterangan_pelaksanaan == 'c) Anggota, tiap periode jabatan') echo 'Selected'; ?>>c) Anggota, tiap periode jabatan</option>
                                        <option value="a) Ketua, tiap kegiatan" <?php if ($keterangan_pelaksanaan == 'a) Ketua, tiap kegiatan') echo 'Selected'; ?>>a) Ketua, tiap kegiatan</option>
                                        <option value="b) Anggota, tiap kegiatan" <?php if ($keterangan_pelaksanaan == 'b) Anggota, tiap kegiatan') echo 'Selected'; ?>>b) Anggota, tiap kegiatan</option>
                                        <option value="a) 30 (tiga puluh) tahun" <?php if ($keterangan_pelaksanaan == 'a) 30 (tiga puluh) tahun') echo 'Selected'; ?>>a) 30 (tiga puluh) tahun</option>
                                        <option value="b) 20 (dua puluh) tahun" <?php if ($keterangan_pelaksanaan == 'b) 20 (dua puluh) tahun') echo 'Selected'; ?>>b) 20 (dua puluh) tahun</option>
                                        <option value="c) 10 (sepuluh) tahun" <?php if ($keterangan_pelaksanaan == 'c) 10 (sepuluh) tahun') echo 'Selected'; ?>>c) 10 (sepuluh) tahun</option>
                                        <option value="a) Tingkat internasional" <?php if ($keterangan_pelaksanaan == 'a) Tingkat internasional') echo 'Selected'; ?>>a) Tingkat internasional</option>
                                        <option value="b) Tingkat nasional" <?php if ($keterangan_pelaksanaan == 'b) Tingkat nasional') echo 'Selected'; ?>>b) Tingkat nasional</option>
                                        <option value="c) Tingkat provinsi" <?php if ($keterangan_pelaksanaan == 'c) Tingkat provinsi') echo 'Selected'; ?>>c) Tingkat provinsi</option>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-md-2 col-sm-3 col-xs-12">Tanggal Penunjang <span class="required">*</span></label>
                                <div class="col-md-5 col-sm-8 col-xs-12">
                                    <input type="date" id="tgl_penunjang" name="tgl_penunjang" required="required" class="form-control col-md-7 col-xs-12" value="<?php echo $tgl_penunjang; ?>">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-md-2 col-sm-3 col-xs-12">Satuan Hasil <span class="required">*</span></label>
                                <div class="col-md-5 col-sm-8 col-xs-12">
                                    <select id="satuan_hasil" name="satuan_hasil" required="required" class="form-control col-md-7 col-xs-12" value="<?php echo $satuan_hasil; ?>">
                                        <option value="">--</option>
                                        <option value="Kepanitiaan/ Pertahun" <?php if ($satuan_hasil == 'Kepanitiaan/ Pertahun') echo 'Selected'; ?>>Kepanitiaan/ Pertahun</option>
                                        <option value="Per panitia" <?php if ($satuan_hasil == 'Per panitia') echo 'Selected'; ?>>Per panitia</option>
                                        <option value="Per periode" <?php if ($satuan_hasil == 'Per periode') echo 'Selected'; ?>>Per periode</option>
                                        <option value="Kegiatan" <?php if ($satuan_hasil == 'Kegiatan') echo 'Selected'; ?>>Kegiatan</option>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-md-2 col-sm-3 col-xs-12">Jumlah Volume Dosen <span class="required">*</span></label>
                                <div class="col-md-5 col-sm-8 col-xs-12">
                                    <input type="number" id="jumlah_volume_dosen" name="jumlah_volume_dosen" required="required" class="form-control col-md-7 col-xs-12" value="<?php echo $jumlah_volume_dosen; ?>" autofocus>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-md-2 col-sm-3 col-xs-12">Keterangan Bukti <span class="required">*</span></label>
                                <div class="col-md-5 col-sm-8 col-xs-12">
                                    <select id="keterangan" name="keterangan" required="required" class="form-control col-md-7 col-xs-12" value="<?php echo $keterangan; ?>">
                                        <option value="">--</option>
                                        <option value="SK Direktur,,,," <?php if ($keterangan == 'SK Direktur,,,,') echo 'Selected'; ?>>SK Direktur,,,,</option>
                                        <option value="SK Direktur NO,,," <?php if ($keterangan == 'SK Direktur NO,,,') echo 'Selected'; ?>>SK Direktur NO,,,</option>
                                        <option value="Printout kartu keanggotaan" <?php if ($keterangan == 'Printout kartu keanggotaan') echo 'Selected'; ?>>Printout kartu keanggotaan</option>
                                        <option value="Surat Tugas" <?php if ($keterangan == 'Surat Tugas') echo 'Selected'; ?>>Surat Tugas</option>
                                        <option value="Surat Penugasan; Surat undangan" <?php if ($keterangan == 'Surat Penugasan; Surat undangan') echo 'Selected'; ?>>Surat Penugasan; Surat undangan</option>
                                        <option value="Sertifikat; Name-tag" <?php if ($keterangan == 'Sertifikat; Name-tag') echo 'Selected'; ?>>Sertifikat; Name-tag</option>
                                        <option value="Sertifikat Asli" <?php if ($keterangan == 'Sertifikat Asli') echo 'Selected'; ?>>Sertifikat Asli</option>
                                        <option value="Fotocopy piagam/sertifikat tanda jasa" <?php if ($keterangan == 'Fotocopy piagam/sertifikat tanda jasa') echo 'Selected'; ?>>Fotocopy piagam/sertifikat tanda jasa</option>
                                        <option value="Buku" <?php if ($keterangan == 'Buku') echo 'Selected'; ?>>Buku</option>
                                        <option value="Fotocopy/ sertifikat tanda jasa" <?php if ($keterangan == 'Fotocopy/ sertifikat tanda jasa') echo 'Selected'; ?>>Fotocopy/ sertifikat tanda jasa</option>
                                        <option value="SK penugasan" <?php if ($keterangan == 'SK penugasan') echo 'Selected'; ?>>SK penugasan</option>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group">
                                <?php
                                if ($bukti_file != NULL) {
                                    echo "File Uploaded: " . $bukti_file;
                                }
                                ?>
                                <label class="control-label col-md-2 col-sm-3 col-xs-12">Bukti File <span class="required">*</span></label>
                                <div class="col-md-5 col-sm-8 col-xs-12">
                                    <input type="file" id="bukti_file" name="bukti_file" required="required" class="form-control col-md-7 col-xs-12">
                                    <p><b>PENTING!<br>Format bukti dokumen/file dapat diunduh dengan mengunjungi <a class="linktautan" href="https://drive.google.com/drive/folders/10CqPjOn7T_YX2Tsh9aQpOW9Nu6wnTy7m?usp=share_link"> Tautan Berikut</a><b></p>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-2 col-sm-offset-3">
                                    <button type="submit" class="btn btn-success"><i class="fa fa-save"></i> <?php echo $button ?></button>
                                    <?php
                                    if (!empty($id_penunjang)) { ?>
                                        <button class="btn btn-danger delete" type="button" data-url="<?php echo base_url('penunjang/delete/' . $id_usulan . '/' . $id_penunjang) ?>"><i class="fa fa-trash"></i> Delete</button>
                                    <?php } ?>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>