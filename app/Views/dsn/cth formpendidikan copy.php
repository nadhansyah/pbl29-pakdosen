<head>
    <style>
        .linktautan {
            /* font-size: 12pt; */
            color: black;
            text-decoration: underline;
        }

        .linktautan:hover {
            color: red;
        }

        .linktautan:link {
            color: blue;
        }

        .linktautan:active {
            color: green;
        }

        .linktautan:visited {
            background: yellow;
        }
    </style>
</head>

<div class="right_col" role="main">
    <div class="">
        <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                    <div class="x_title">
                        <h2><?php echo $title ?></h2>
                        <div class="clearfix"></div>
                    </div>
                    <div class="x_content">
                        <form id="applications" data-parsley-validate class="form-horizontal form-label-left" action="<?php echo $action; ?>" method="post">
                            <div class="form-group">
                                <label class="control-label col-md-2 col-sm-3 col-xs-12">ID Usulan <span class="required">*</span></label>
                                <div class="col-md-2 col-sm-6 col-xs-12">
                                    <input type="text" id="id_usulan" name="id_usulan" required="required" class="form-control col-md-7 col-xs-12" value="<?php echo $id_usulan; ?>">
                                </div>
                            </div>
                            //select option
                            <!-- <div class="form-group">
                                <label class="control-label col-md-2 col-sm-3 col-xs-12">Jenis Pendidikan <span class="required">*</span></label>
                                <div class="col-md-9 col-sm-6 col-xs-12">
                                    <select id="jenis_pendidikan" name="jenis_pendidikan" required="required" class="form-control col-md-7 col-xs-12" value="<php echo $jenis_pendidikan; ?>" onchange='if(this.value != 0) { this.form.submit(); }'>
                                        <option value="">--</option>
                                        <option value="1" <php if ($jenis_pendidikan == '1') echo 'Selected'; ?>>A - Pendidikan Formal</option>
                                        <option value="2" <php if ($jenis_pendidikan == '2') echo 'Selected'; ?>>B - Pendidikan dan Pelatihan Prajabatan</option>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-md-2 col-sm-3 col-xs-12">Detail Pendidikan <span class="required">*</span></label>
                                <div class="col-md-9 col-sm-6 col-xs-12">
                                    <select id="detail_pendidikan" name="detail_pendidikan" required="required" class="form-control col-md-7 col-xs-12" value="<php echo $detail_pendidikan; ?>">
                                        <option value="">--</option>
                                        <php
                                        if ($jenis_pendidikan == '1') {
                                        ?>
                                            <option value="1" <php if ($detail_pendidikan == '1') echo 'Selected'; ?>>A - Pendidikan Formal</option>
                                            <option value="2" <php if ($detail_pendidikan == '2') echo 'Selected'; ?>>B - Pendidikan dan Pelatihan Prajabatan</option>
                                    </select>
                                <php  } else if ($jenis_pendidikan == '2') { ?>
                                    <option value="3">C - Pendidikan Formal</option>
                                <php } ?>
                                </select>
                                </div>
                            </div> -->
                            <div class="form-group">
                                <label class="control-label col-md-2 col-sm-3 col-xs-12">Jenis Pendidikan <span class="required">*</span></label>
                                <div class="col-md-9 col-sm-6 col-xs-12">
                                    <textarea id="jenis_pendidikan" name="jenis_pendidikan" required="required" class="form-control col-md-7 col-xs-12"><?php echo "$jenis_pendidikan"; ?></textarea>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-md-2 col-sm-3 col-xs-12">Detail Pendidikan <span class="required">*</span></label>
                                <div class="col-md-9 col-sm-6 col-xs-12">
                                    <textarea id="detail_pendidikan" name="detail_pendidikan" required="required" class="form-control col-md-7 col-xs-12"><?php echo $detail_pendidikan; ?></textarea>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-md-2 col-sm-3 col-xs-12">Tanggal Pendidikan <span class="required">*</span></label>
                                <div class="col-md-2 col-sm-6 col-xs-12">
                                    <input type="date" id="tgl_pendidikan" name="tgl_pendidikan" required="required" class="form-control col-md-7 col-xs-12" value="<?php echo $tgl_pendidikan; ?>">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-md-2 col-sm-3 col-xs-12">Satuan Hasil <span class="required">*</span></label>
                                <div class="col-md-4 col-sm-6 col-xs-12">
                                    <input type="text" id="satuan_hasil" name="satuan_hasil" required="required" class="form-control col-md-7 col-xs-12" value="<?php echo $satuan_hasil; ?>">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-md-2 col-sm-3 col-xs-12">Jumlah Volume Dosen <span class="required">*</span></label>
                                <div class="col-md-2 col-sm-6 col-xs-12">
                                    <input type="number" id="jumlah_volume_dosen" name="jumlah_volume_dosen" required="required" class="form-control col-md-7 col-xs-12" value="<?php echo $jumlah_volume_dosen; ?>" autofocus>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-md-2 col-sm-3 col-xs-12">Keterangan&ensp;</label>
                                <div class="col-md-9 col-sm-6 col-xs-12">
                                    <textarea id="keterangan" name="keterangan" class="form-control col-md-7 col-xs-12"><?php echo $keterangan; ?></textarea>
                                </div>
                            </div>
                            <div class="form-group">
                                <?php
                                if ($bukti_file != NULL) {
                                    echo "File Uploaded: " . $bukti_file;
                                }
                                ?>
                                <label class="control-label col-md-2 col-sm-3 col-xs-12">Bukti File <span class="required">*</span></label>
                                <div class="col-md-4 col-sm-6 col-xs-12">
                                    <input type="file" id="bukti_file" name="bukti_file" required="required" class="form-control col-md-7 col-xs-12">
                                    <p><b>PENTING!<br>Format bukti dokumen/file dapat diunduh dengan mengunjungi <a class="linktautan" href="#"> Tautan Berikut</a><b></p>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-2 col-sm-offset-3">
                                    <button type="submit" class="btn btn-success"><i class="fa fa-save"></i> <?php echo $button ?></button>
                                    <?php
                                    if (!empty($id_pendidikan)) { ?>
                                        <button class="btn btn-danger delete" type="button" data-url="<?php echo base_url('pendidikan/delete/' . $id_pendidikan) ?>"><i class="fa fa-trash"></i> Delete</button>
                                    <?php } ?>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>