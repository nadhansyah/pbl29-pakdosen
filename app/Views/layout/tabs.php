<div class=container-fluid>
    <ul class="nav nav-tabs ms-3 mt-2" id="myTab" role="tablist">
        <li class="nav-item" role="presentation">
            <button class="nav-link active" id="pendidikan-tab" data-bs-toggle="tab" data-bs-target="#pendidikan-tab-pane" type="button" role="tab" aria-controls="pendidikan-tab-pane" aria-selected="true">Pendidikan</button>
        </li>
        <li class="nav-item" role="presentation">
            <button class="nav-link" id="pelaksanaanpendidikan-tab" data-bs-toggle="tab" data-bs-target="#pelaksanaanpendidikan-tab-pane" type="button" role="tab" aria-controls="pelaksanaanpendidikan-tab-pane" aria-selected="false">Pelaksanaan Pendidikan</button>
        </li>
        <li class="nav-item" role="presentation">
            <button class="nav-link" id="penelitian-tab" data-bs-toggle="tab" data-bs-target="#penelitian-tab-pane" type="button" role="tab" aria-controls="penelitian-tab-pane" aria-selected="false">Penelitian</button>
        </li>
        <li class="nav-item" role="presentation">
            <button class="nav-link" id="pengabdian-tab" data-bs-toggle="tab" data-bs-target="#pengabdian-tab-pane" type="button" role="tab" aria-controls="pengabdian-tab-pane" aria-selected="true">Pengabdian</button>
        </li>
        <li class="nav-item" role="presentation">
            <button class="nav-link" id="penunjang-tab" data-bs-toggle="tab" data-bs-target="#penunjang-tab-pane" type="button" role="tab" aria-controls="penunjang-tab-pane" aria-selected="false">Penunjang</button>
        </li>

    </ul>
</div>

<!-- end tabs -->

<!-- tab konten: -->