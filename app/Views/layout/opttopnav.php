<!-- top navigation -->
<div class="top_nav">
	<div class="nav_menu">
		<nav>
			<div class="nav toggle">
				<a id="menu_toggle"><i class="fa fa-bars"></i></a>
			</div>

			<ul class="nav navbar-nav navbar-right">
				<li class="">
					<a href="javascript:;" class="user-profile dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
						Operator
					</a>
					<ul class="dropdown-menu dropdown-usermenu pull-right">
						<li><a href="<?php echo base_url('operator/profile') ?>"><i class="fa fa-user-circle pull-right"></i> Profile</a></li>
						<li><a href="<?php echo base_url('operator/pass') ?>"><i class="fa fa-key pull-right"></i>Change Password</a></li>
						<li><a href="<?php echo site_url('operator/logout') ?>"><i class="fa fa-sign-out pull-right"></i> Sign Out</a></li>
					</ul>
				</li>
				<li role="presentation" class="dropdown">

					<ul id="menu1" class="dropdown-menu list-unstyled msg_list" role="menu">

						<li>
							<div class="text-center">
								<a href="<?php echo site_url('contactus') ?>">
									<strong>See All Messages</strong>
									<i class="fa fa-angle-right"></i>
								</a>
							</div>
						</li>
					</ul>
				</li>
			</ul>
		</nav>
	</div>
</div>

<!-- /top navigation -->