<head>
    <style>
        .linktautan {
            /* font-size: 12pt; */
            color: black;
            text-decoration: underline;
        }

        .linktautan:hover {
            color: red;
        }

        .linktautan:link {
            color: blue;
        }

        .linktautan:active {
            color: green;
        }

        .linktautan:visited {
            background: yellow;
        }
    </style>
</head>

<div class="right_col" role="main">
    <div class="">
        <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                    <div class="x_title">
                        <h2><?php echo $title ?></h2>
                        <div class="clearfix"></div>
                    </div>
                    <div class="x_content">
                        <form id="applications" data-parsley-validate class="form-horizontal form-label-left" action="<?php echo $action; ?>" method="post">


                            <div class="form-group">
                                <label class="control-label col-md-3 col-sm-3 col-xs-12">Jenis Pelaksanaan&ensp;</label>
                                <div class="col-md-5 col-sm-7 col-xs-12">
                                    <input type="text" id="jenis_pelaksanaan" name="jenis_pelaksanaan" required="required" class="form-control col-md-7 col-xs-12" value="<?php echo $jenis_pelaksanaan; ?>" readonly>

                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-md-3 col-sm-3 col-xs-12">Detail Pelaksanaan&ensp;</label>
                                <div class="col-md-5 col-sm-7 col-xs-12">
                                    <input type="text" id="detail_pelaksanaan" name="detail_pelaksanaan" required="required" class="form-control col-md-7 col-xs-12" value="<?php echo $detail_pelaksanaan; ?>" readonly>

                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-md-3 col-sm-3 col-xs-12">Keterangan Pelaksanaan&ensp;</label>
                                <div class="col-md-5 col-sm-7 col-xs-12">
                                    <input type="text" id="keterangan_pelaksanaan" name="keterangan_pelaksanaan" required="required" class="form-control col-md-7 col-xs-12" value="<?php echo $keterangan_pelaksanaan; ?>" readonly>

                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-md-3 col-sm-3 col-xs-12">Tanggal Pelaksanaan&ensp;</label>
                                <div class="col-md-3 col-sm-7 col-xs-12">
                                    <input type="date" id="tgl_pelaksanaan" name="tgl_pelaksanaan" required="required" class="form-control col-md-7 col-xs-12" value="<?php echo $tgl_pelaksanaan; ?>" readonly>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-md-3 col-sm-3 col-xs-12">Semester/ Waktu&ensp;</label>
                                <div class="col-md-3 col-sm-7 col-xs-12">
                                    <input type="text" id="semester_waktu" name="semester_waktu" required="required" class="form-control col-md-7 col-xs-12" value="<?php echo $semester_waktu; ?>" readonly>


                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-md-3 col-sm-3 col-xs-12">Satuan Hasil&ensp;</label>
                                <div class="col-md-3 col-sm-7 col-xs-12">
                                    <input type="text" id="satuan_hasil" name="satuan_hasil" required="required" class="form-control col-md-7 col-xs-12" value="<?php echo $satuan_hasil; ?>" readonly>


                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-md-3 col-sm-3 col-xs-12">Jumlah Volume Dosen&ensp;</label>
                                <div class="col-md-3 col-sm-7 col-xs-12">
                                    <input type="text" id="jumlah_volume_dosen" name="jumlah_volume_dosen" required="required" class="form-control col-md-7 col-xs-12" value="<?php echo $jumlah_volume_dosen; ?>" readonly>

                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-md-3 col-sm-3 col-xs-12">Keterangan Bukti&ensp;</label>
                                <div class="col-md-3 col-sm-7 col-xs-12">
                                    <input type="text" id="keterangan" name="keterangan" required="required" class="form-control col-md-7 col-xs-12" value="<?php echo $keterangan; ?>" readonly>

                                </div>
                            </div>
                            <div class="form-group">

                                <label class="control-label col-md-3 col-sm-3 col-xs-12">Bukti File&ensp;</label>
                                <div class="col-md-3 col-sm-7 col-xs-12">
                                    <?php
                                    if ($bukti_file != NULL) {
                                    ?>
                                        <input type="text" class="form-control col-md-7 col-xs-12" placeholder="File uploaded: <?= $bukti_file ?>" readonly>
                                    <?php

                                        echo '<br>For Download, <a class="linktautan" href="#">CLICK HERE!<a>';
                                    } else {
                                    ?>
                                        <input type="text" class="form-control col-md-7 col-xs-12" placeholder="NO FILE UPLOADED" readonly>
                                    <?php
                                    }
                                    ?>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-md-3 col-sm-3 col-xs-12">Jumlah Volume Reviewer <span class="required">*</span></label>
                                <div class="col-md-3 col-sm-7 col-xs-12">
                                    <select id="jumlah_volume_reviewer" name="jumlah_volume_reviewer" required="required" class="form-control col-md-7 col-xs-12" value="<?php echo $jumlah_volume_reviewer; ?>" autofocus>
                                        <option value="">--</option>
                                        <option value="200" <?php if ($jumlah_volume_reviewer == '200') echo 'Selected'; ?>>S3 - 200</option>
                                        <option value="150" <?php if ($jumlah_volume_reviewer == '150') echo 'Selected'; ?>>S2 - 150</option>
                                        <option value="3" <?php if ($jumlah_volume_reviewer == '3') echo 'Selected'; ?>>3</option>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3 col-sm-offset-3">
                                    <button type="submit" class="btn btn-success"><i class="fa fa-save"></i> <?php echo $button ?></button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>