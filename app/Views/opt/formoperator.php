<div class="right_col" role="main">
    <div class="">
        <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                    <div class="x_title">
                        <h2><?php echo $title ?></h2>
                        <div class="clearfix"></div>
                    </div>
                    <div class="x_content">
                        <form id="applications" data-parsley-validate class="form-horizontal form-label-left" action="<?php echo $action; ?>" method="post">
                            <div class="form-group">
                                <label class="control-label col-md-3 col-sm-3 col-xs-12">NIK Operator <span class="required">*</span></label>
                                <div class="col-md-3 col-sm-6 col-xs-12">
                                    <input type="text" id="nik_operator" name="nik_operator" required="required" class="form-control col-md-7 col-xs-12" value="<?php echo $nik_operator; ?>" maxlength="20" placeholder="Nomor Induk Kepegawaian">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-md-3 col-sm-3 col-xs-12">Nama Operator <span class="required">*</span></label>
                                <div class="col-md-5 col-sm-6 col-xs-12">
                                    <input type="text" id="nama" name="nama" required="required" class="form-control col-md-7 col-xs-12" value="<?php echo $nama; ?>" placeholder="Nama lengkap">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-md-3 col-sm-3 col-xs-12">Password Operator <span class="required">*</span></label>
                                <div class="col-md-5 col-sm-6 col-xs-12">
                                    <input type="text" id="password" name="password" required="required" class="form-control col-md-7 col-xs-12" value="<?php echo $password; ?>" maxlength="30" placeholder="(max 30 karakter)">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-md-3 col-sm-3 col-xs-12">Email Operator <span class="required">*</span></label>
                                <div class="col-md-5 col-sm-6 col-xs-12">
                                    <input type="text" id="email" name="email" required="required" class="form-control col-md-7 col-xs-12" value="<?php echo $email; ?>" placeholder="Email aktif dan resmi">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-md-3 col-sm-3 col-xs-12">No HP Operator <span class="required">*</span></label>
                                <div class="col-md-3 col-sm-6 col-xs-12">
                                    <input type="text" id="no_hp" name="no_hp" required="required" class="form-control col-md-7 col-xs-12" value="<?php echo $no_hp; ?>" pattern="\+62?(\d+)+|\(\d+\)" maxlength="15" placeholder="Format: +62.... (max 15 karakter)">
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
                                    <button type="submit" class="btn btn-success"><i class="fa fa-save"></i> <?php echo $button ?></button>
                                    <?php
                                    if (!empty($nik_operator)) { ?>
                                        <button class="btn btn-danger delete" type="button" data-url="<?php echo base_url('operator/delete/' . $nik_operator) ?>"><i class="fa fa-trash"></i> Delete</button>
                                    <?php } ?>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>