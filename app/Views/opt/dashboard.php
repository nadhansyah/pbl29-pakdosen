<head>
    <link rel="stylesheet" href="/asset/css/dashboard.css">
</head>
<div class="right_col" role="main">
    <div class="">
        <div class="page-title">
            <div class="title_left">

                <div class="item box-biru ">
                    <div class="icon-user">
                        <i class="fa fa-user-plus fa-4x" aria-hidden="true"></i>
                    </div>
                    <div class="data-user">
                        <h3>Operator</h3>
                        <p class="data"><?php echo $jml_dosen ?></p>
                    </div>
                </div>
                <div class="item box-biru">
                    <div class="icon-user">
                        <i class="fa fa-users fa-4x" aria-hidden="true"></i>
                    </div>
                    <div class="data-user">
                        <h3>Users</h3>
                        <p class="data"><?php echo $jml_reviewer ?></p>
                    </div>
                </div>
                <div class="item box-biru">
                    <div class="icon-user">
                        <i class="fa fa-user fa-4x" aria-hidden="true"></i>
                    </div>
                    <div class="data-user">
                        <h3>Dosen</h3>
                        <p class="data"><?php echo $jml_operator ?></p>
                    </div>
                </div>

            </div>
            <div class="title_right">
            </div>
        </div>

        <div class="clearfix"></div>

    </div>
</div>