<!-- page content -->
<div class="right_col" role="main">
    <div class="">
        <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                    <div class="x_title">
                        <h2><?php echo $title ?></h2>
                        <ul class="nav navbar-right panel_toolbox">

                            <li><button class="btn btn-success val" type="button" data-url="<?php echo base_url('usulan/val/' . $id_usulan) ?>"><i class="fa fa-check-circle-o"></i> Selesai Validasi</button></li>
                        </ul>
                        <div class="clearfix"></div>
                    </div>
                    <div class="x_content">
                        <table id="dosen" class="table table-striped table-bordered">
                            <thead>
                                <tr>
                                    <th>
                                        <h4><b>Keterangan Ajuan</b></h4>
                                    </th>
                                    <th>
                                        <h4><b>Dokumentasi</b></h4>
                                    </th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td>Profile Pengaju</td>
                                    <td><a href="<?= Base_url('dsn/profile/' . $nik_dosen) ?>"><button type="button" class="btn btn-primary btn-xs"><i class="fa fa-user-circle"></i> Lihat Profile</button></a></td>
                                </tr>
                                <tr>
                                    <td>Pendidikan</td>
                                    <td><a href="<?= Base_url('valpendidikan/index/' . $id_usulan) ?>"><button type="button" class="btn btn-warning btn-xs"><i class="fa fa-pencil"></i> Validasi Dokumen</button></a></td>
                                </tr>
                                <tr>
                                    <td>Pelaksanaan Pendidikan</td>
                                    <td><a href="<?= Base_url('valpelpendidikan/index/' . $id_usulan) ?>"><button type="button" class="btn btn-warning btn-xs"><i class="fa fa-pencil"></i> Validasi Dokumen</button></a></td>
                                </tr>
                                <tr>
                                    <td>Pelaksanaan Penelitian</td>
                                    <td><a href="<?= Base_url('valpenelitian/index/' . $id_usulan) ?>"><button type="button" class="btn btn-warning btn-xs"><i class="fa fa-pencil"></i> Validasi Dokumen</button></a></td>
                                </tr>
                                <tr>
                                    <td>Pengabdian kepada Masyarakat</td>
                                    <td><a href="<?= Base_url('valpengabdian/index/' . $id_usulan) ?>"><button type="button" class="btn btn-warning btn-xs"><i class="fa fa-pencil"></i> Validasi Dokumen</button></a></td>
                                </tr>
                                <tr>
                                    <td>Penunjang Tugas Dosen</td>
                                    <td><a href="<?= Base_url('valpenunjang/index/' . $id_usulan) ?>"><button type="button" class="btn btn-warning btn-xs"><i class="fa fa-pencil"></i> Validasi Dokumen</button></a></td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script src="https://code.jquery.com/jquery-3.5.1.min.js"></script>
<script src="https://cdn.datatables.net/1.10.22/js/jquery.dataTables.min.js"></script>