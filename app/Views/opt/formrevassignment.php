<div class="right_col" role="main">
    <div class="">
        <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                    <div class="x_title">
                        <h2><?php echo $title ?></h2>
                        <div class="clearfix"></div>
                    </div>
                    <div class="x_content">
                        <form id="applications" data-parsley-validate class="form-horizontal form-label-left" action="<?php echo $action; ?>" method="post">
                            <div class="form-group">
                                <label class="control-label col-md-3 col-sm-3 col-xs-12">Dosen Pengaju <span class="required">*</span></label>
                                <div class="col-md-3 col-sm-6 col-xs-12">
                                    <select id="nik_dosen" name="nik_dosen" required="required" class="form-control col-md-7 col-xs-12" value="<?php echo $nik_dosen; ?>" disabled>
                                        <?php
                                        foreach ($listdosen as $ldsn) {
                                            if ($ldsn['nik_dosen'] == $nik_dosen) $selected = 'selected';
                                            else $selected = '';
                                            echo '<option value="' . $ldsn['nik_dosen'] . '"' . $selected . '>' . $ldsn['nik_dosen'] . ' - ' . $ldsn['nama'] . '</option>';
                                        }
                                        ?>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-md-3 col-sm-3 col-xs-12">Tanggal Validasi <span class="required">*</span></label>
                                <div class="col-md-5 col-sm-6 col-xs-12">
                                    <input type="date" id="tgl_validasi" name="tgl_validasi" required="required" class="form-control col-md-7 col-xs-12" value="<?php echo $tgl_validasi; ?>" disabled>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-md-3 col-sm-3 col-xs-12">Status Ajuan <span class="required">*</span></label>
                                <div class="col-md-3 col-sm-6 col-xs-12">
                                    <select id="status_usulan" name="status_usulan" required="required" class="form-control col-md-7 col-xs-12" value="<?php echo $status_usulan; ?>">
                                        <option value="">--</option>
                                        <option value="Diajukan" <?php if ($status_usulan == 'Diajukan') echo 'Selected'; ?>>Diajukan</option>
                                        <option value="Validasi Operator" <?php if ($status_usulan == 'Validasi Operator') echo 'Selected'; ?>>Validasi Operator</option>
                                        <option value="Tervalidasi" <?php if ($status_usulan == 'Tervalidasi') echo 'Selected'; ?>>Tervalidasi</option>
                                        <option value="Penilaian Reviewer" <?php if ($status_usulan == 'Penilaian Reviewer') echo 'Selected'; ?>>Penilaian Reviewer</option>
                                        <option value="Ternilai" <?php if ($status_usulan == 'Ternilai') echo 'Selected'; ?>>Ternilai</option>
                                        <option value="Diperbaiki" <?php if ($status_usulan == 'Diperbaiki') echo 'Selected'; ?>>Diperbaiki</option>
                                        <option value="Diajukan kembali" <?php if ($status_usulan == 'Diajukan kembali') echo 'Selected'; ?>>Diajukan kembali</option>
                                        <option value="Ditolak" <?php if ($status_usulan == 'Ditolak') echo 'Selected'; ?>>Ditolak</option>
                                        <option value="Diterima" <?php if ($status_usulan == 'Diterima') echo 'Selected'; ?>>Diterima</option>
                                        <option value="Diusulkan ke Senat" <?php if ($status_usulan == 'Diusulkan ke Senat') echo 'Selected'; ?>>Diusulkan ke Senat</option>
                                    </select>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-md-3 col-sm-3 col-xs-12">NIK Reviewer 1 <span class="required">*</span></label>
                                <div class="col-md-5 col-sm-6 col-xs-12">
                                    <select id="nik_reviewer_1" name="nik_reviewer_1" required="required" class="form-control col-md-7 col-xs-12" value="<?php echo $nik_reviewer_1; ?>">
                                        <?php
                                        foreach ($listreviewer as $lrev) {
                                            if ($lrev['nik_dosen'] == $nik_reviewer_1) $selected = 'selected';
                                            else $selected = '';
                                            echo '<option value="' . $lrev['nik_dosen'] . '"' . $selected . '>' . $lrev['nik_dosen'] . ' - ' . $lrev['nama'] . '</option>';
                                        }
                                        ?>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-md-3 col-sm-3 col-xs-12">NIK Reviewer 2 <span class="required">*</span></label>
                                <div class="col-md-5 col-sm-6 col-xs-12">
                                    <select id="nik_reviewer_2" name="nik_reviewer_2" required="required" class="form-control col-md-7 col-xs-12" value="<?php echo $nik_reviewer_2; ?>">
                                        <?php
                                        foreach ($listreviewer as $lrev) {
                                            if ($lrev['nik_dosen'] == $nik_reviewer_2) $selected = 'selected';
                                            else $selected = '';
                                            echo '<option value="' . $lrev['nik_dosen'] . '"' . $selected . '>' . $lrev['nik_dosen'] . ' - ' . $lrev['nama'] . '</option>';
                                        }
                                        ?>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-md-5 col-sm-6 col-xs-12 col-md-offset-3 col-sm-offset-3">
                                    <button type="submit" class="btn btn-success"><i class="fa fa-save"></i> <?php echo $button ?></button>
                                    <?php
                                    if (!empty($id_usulan)) { ?>
                                        <button class="btn btn-danger delete" type="button" data-url="<?php echo base_url('revassignment/delete/' . $id_usulan) ?>"><i class="fa fa-trash"></i> Delete</button>
                                    <?php } ?>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>