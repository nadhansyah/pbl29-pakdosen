<div class="right_col" role="main">
    <div class="">
        <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                    <div class="x_title">
                        <h2><?php echo $title ?></h2>
                        <div class="clearfix"></div>
                    </div>
                    <div class="x_content">
                        <form id="applications" data-parsley-validate class="form-horizontal form-label-left" action="<?php echo $action; ?>" method="post">

                            <div class="form-group">
                                <label class="control-label col-md-3 col-sm-3 col-xs-12">NIK Reviewer 1 <span class="required">*</span></label>
                                <div class="col-md-5 col-sm-6 col-xs-12">
                                    <select id="nik_reviewer_1" name="nik_reviewer_1" required="required" class="form-control col-md-7 col-xs-12" value="<?php echo $nik_reviewer_1; ?>">
                                        <option value="">--</option>
                                        <?php
                                        foreach ($listreviewer as $lrev) {
                                            if ($lrev['nik_dosen'] == $nik_reviewer_1) $selected = 'selected';
                                            else $selected = '';
                                            echo '<option value="' . $lrev['nik_dosen'] . '"' . $selected . '>' . $lrev['nik_dosen'] . ' - ' . $lrev['nama'] . '</option>';
                                        }
                                        ?>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-md-3 col-sm-3 col-xs-12">NIK Reviewer 2 <span class="required">*</span></label>
                                <div class="col-md-5 col-sm-6 col-xs-12">
                                    <select id="nik_reviewer_2" name="nik_reviewer_2" required="required" class="form-control col-md-7 col-xs-12" value="<?php echo $nik_reviewer_2; ?>">
                                        <option value="">--</option>
                                        <?php
                                        foreach ($listreviewer as $lrev) {
                                            if ($lrev['nik_dosen'] == $nik_reviewer_2) $selected = 'selected';
                                            else $selected = '';
                                            echo '<option value="' . $lrev['nik_dosen'] . '"' . $selected . '>' . $lrev['nik_dosen'] . ' - ' . $lrev['nama'] . '</option>';
                                        }
                                        ?>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-md-5 col-sm-6 col-xs-12 col-md-offset-3 col-sm-offset-3">
                                    <button type="submit" class="btn btn-success"><i class="fa fa-save"></i> <?php echo $button ?></button>

                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>