<div class="right_col" role="main">
    <div class="">
        <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                    <div class="x_title">
                        <h2><?php echo $title ?></h2>
                        <div class="clearfix"></div>
                    </div>
                    <div class="x_content">
                        <form id="applications" data-parsley-validate class="form-horizontal form-label-left" method="post" enctype="multipart/formdata">
                            <div class="form-group">
                                <?php
                                if ($foto_profile != NULL) {
                                ?>
                                    <img src="/asset/profile/<?php echo $foto_profile; ?>">
                                <?php
                                }
                                ?>
                                <label class="control-label col-md-3 col-sm-3 col-xs-12">Foto Profile Dosen&ensp;</label>
                                <div class="col-md-4 col-sm-6 col-xs-12">
                                    <input type="file" id="foto_profile" name="foto_profile" class="form-control col-md-7 col-xs-12" value="<?php echo $foto_profile; ?>" disabled>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-md-3 col-sm-3 col-xs-12">NIK Dosen <span class="required">*</span></label>
                                <div class="col-md-4 col-sm-6 col-xs-12">
                                    <input type="text" id="nik_dosen" name="nik_dosen" required="required" class="form-control col-md-7 col-xs-12" value="<?php echo $nik_dosen; ?>" readonly>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-md-3 col-sm-3 col-xs-12">Nama Dosen <span class="required">*</span></label>
                                <div class="col-md-4 col-sm-6 col-xs-12">
                                    <input type="text" id="nama" name="nama" required="required" class="form-control col-md-7 col-xs-12" value="<?php echo $nama; ?>" readonly>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-md-3 col-sm-3 col-xs-12">Email Dosen <span class="required">*</span></label>
                                <div class="col-md-4 col-sm-6 col-xs-12">
                                    <input type="email" id="email" name="email" required="required" class="form-control col-md-7 col-xs-12" value="<?php echo $email; ?>" readonly>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-md-3 col-sm-3 col-xs-12">No HP Dosen <span class="required">*</span></label>
                                <div class="col-md-4 col-sm-6 col-xs-12">
                                    <input type="text" id="no_hp" name="no_hp" required="required" class="form-control col-md-7 col-xs-12" value="<?php echo $no_hp; ?>" readonly>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-md-3 col-sm-3 col-xs-12">Tempat Lahir Dosen <span class="required">*</span></label>
                                <div class="col-md-4 col-sm-6 col-xs-12">
                                    <input type="text" id="tempat_lahir" name="tempat_lahir" class="form-control col-md-7 col-xs-12" value="<?php echo $tempat_lahir; ?>" readonly>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-md-3 col-sm-3 col-xs-12">Tanggal Lahir Dosen <span class="required">*</span></label>
                                <div class="col-md-4 col-sm-6 col-xs-12">
                                    <input type="date" id="tgl_lahir" name="tgl_lahir" class="form-control col-md-7 col-xs-12" value="<?php echo $tgl_lahir; ?>" readonly>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-md-3 col-sm-3 col-xs-12">Jenis Kelamin Dosen <span class="required">*</span></label>
                                <div class="col-md-4 col-sm-6 col-xs-12">
                                    <input type="text" id="jenis_kelamin" name="jenis_kelamin" required="required" class="form-control col-md-7 col-xs-12" value="<?php echo $jenis_kelamin; ?>" readonly>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-md-3 col-sm-3 col-xs-12">Jurusan Dosen&ensp;</label>
                                <div class="col-md-6 col-sm-6 col-xs-12">
                                    <input type="text" id="jurusan" name="jurusan" class="form-control col-md-7 col-xs-12" value="<?php echo $jurusan; ?>" readonly>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-md-3 col-sm-3 col-xs-12">Prodi Dosen&ensp;</label>
                                <div class="col-md-6 col-sm-6 col-xs-12">
                                    <input type="text" id="prodi" name="prodi" class="form-control col-md-7 col-xs-12" value="<?php echo $prodi; ?>" readonly>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-md-3 col-sm-3 col-xs-12">Jenjang Pendidikan Dosen&ensp;</label>
                                <div class="col-md-6 col-sm-6 col-xs-12">
                                    <input type="text" id="jenjang_pendidikan" name="jenjang_pendidikan" class="form-control col-md-7 col-xs-12" value="<?php echo $jenjang_pendidikan; ?>" readonly>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-md-3 col-sm-3 col-xs-12">Jabatan Fungsional Dosen&ensp;</label>
                                <div class="col-md-6 col-sm-6 col-xs-12">
                                    <input type="text" id="jabatan_fungsional" name="jabatan_fungsional" class="form-control col-md-7 col-xs-12" value="<?php echo $jabatan_fungsional; ?>" readonly>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-md-3 col-sm-3 col-xs-12">Bidang Ilmu Dosen&ensp;</label>
                                <div class="col-md-6 col-sm-6 col-xs-12">
                                    <input type="text" id="bidang_ilmu" name="bidang_ilmu" class="form-control col-md-7 col-xs-12" value="<?php echo $bidang_ilmu; ?>" readonly>
                                </div>
                            </div>
                            <!-- <div class="form-group">
                                <label class="control-label col-md-3 col-sm-3 col-xs-12">Foto Profile Dosen</label>
                                <div class="col-md-4 col-sm-6 col-xs-12">
                                    <input type="file" id="foto_profile" name="foto_profile" class="form-control col-md-7 col-xs-12" value="<php echo $foto_profile; ?>">
                                </div>
                            </div> -->
                            <div class="form-group">
                                <label class="control-label col-md-3 col-sm-3 col-xs-12">NIDN Dosen&ensp;</label>
                                <div class="col-md-4 col-sm-6 col-xs-12">
                                    <input type="text" id="nidn" name="nidn" class="form-control col-md-7 col-xs-12" value="<?php echo $nidn; ?>" readonly>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-md-3 col-sm-3 col-xs-12">NIP Dosen&ensp;</label>
                                <div class="col-md-4 col-sm-6 col-xs-12">
                                    <input type="text" id="nip" name="nip" class="form-control col-md-7 col-xs-12" value="<?php echo $nip; ?>" readonly>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-md-3 col-sm-3 col-xs-12">Pangkat/Golongan Dosen&ensp;</label>
                                <div class="col-md-6 col-sm-6 col-xs-12">
                                    <input type="text" id="pangkat_golongan" name="pangkat_golongan" class="form-control col-md-7 col-xs-12" value="<?php echo $pangkat_golongan; ?>" readonly>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-md-3 col-sm-3 col-xs-12">Masa Golongan Lama Dosen&ensp;</label>
                                <div class="col-md-6 col-sm-6 col-xs-12">
                                    <input type="text" id="masa_gol_lama" name="masa_gol_lama" class="form-control col-md-7 col-xs-12" value="<?php echo $masa_gol_lama; ?>" readonly>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-md-3 col-sm-3 col-xs-12">Masa Golongan Baru Dosen&ensp;</label>
                                <div class="col-md-6 col-sm-6 col-xs-12">
                                    <input type="text" id="masa_gol_baru" name="masa_gol_baru" class="form-control col-md-7 col-xs-12" value="<?php echo $masa_gol_baru; ?>" readonly>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>