<!-- page content -->
<div class="right_col" role="main">
    <div class="">
        <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                    <div class="x_title">
                        <h2>Detail Reviwer dari Pengaju <?= $nik_dosen; ?></h2>
                        <ul class="nav navbar-right panel_toolbox">

                            <li><button class="btn btn-success ver" type="button" data-url="<?php echo base_url('usulan/ver/' . $id_usulan) ?>"><i class="fa fa-check-circle-o"></i> Selesai Verifikasi</button></li>
                        </ul>
                        <div class="clearfix"></div>
                    </div>
                    <div class="x_content">
                        <table id="dosen" class="table table-striped table-bordered">
                            <thead>
                                <tr>
                                    <th colspan="2" style="text-align:center">
                                        <h4><b>Detail Reviewer Satu</b></h4>
                                    </th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td>NIK Reviewer</td>
                                    <td><?= $nik_reviewer_1; ?></td>
                                </tr>
                                <!-- <tr>
                                    <td>Nama</td>
                                    <td><= $listnama['nik_reviewer_1']; ?></td>
                                </tr> -->
                                <tr>
                                    <td>Profile Reviewer</td>
                                    <td><a href="<?= Base_url('dsn/profile/' . $nik_reviewer_1) ?>"><button type="button" class="btn btn-primary btn-xs"><i class="fa fa-user-circle"></i> Lihat Profile</button></a></td>
                                </tr>

                                <tr>
                                    <td>Dokumen Penilaian</td>
                                    <td><a href="<?= Base_url('verifusulan/detail/' . $id_usulan) ?>"><button type="button" class="btn btn-warning btn-xs"><i class="fa fa-pencil"></i>Lihat Nilai</button></a></td>
                                </tr>

                            </tbody>
                        </table>
                        <table id="dosen" class="table table-striped table-bordered">
                            <thead>
                                <tr>
                                    <th colspan="2" style="text-align:center">
                                        <h4><b>Detail Reviewer Dua</b></h4>
                                    </th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td>NIK Reviewer</td>
                                    <td><?= $nik_reviewer_2; ?></td>
                                </tr>
                                <!-- <tr>
                                    <td>Nama</td>
                                    <td></td>
                                </tr> -->
                                <tr>
                                    <td>Profile Reviewer</td>
                                    <td><a href="<?= Base_url('dsn/profile/' . $nik_reviewer_2) ?>"><button type="button" class="btn btn-primary btn-xs"><i class="fa fa-user-circle"></i> Lihat Profile</button></a></td>
                                </tr>
                                <tr>
                                    <td>Dokumen Penilaian</td>
                                    <td><a href="<?= Base_url('valpenelitian/index/') ?>"><button type="button" class="btn btn-warning btn-xs"><i class="fa fa-pencil"></i>Lihat Nilai</button></a></td>
                                </tr>

                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script src="https://code.jquery.com/jquery-3.5.1.min.js"></script>
<script src="https://cdn.datatables.net/1.10.22/js/jquery.dataTables.min.js"></script>