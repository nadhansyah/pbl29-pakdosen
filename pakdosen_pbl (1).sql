-- phpMyAdmin SQL Dump
-- version 5.2.0
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Dec 30, 2022 at 05:48 PM
-- Server version: 10.4.24-MariaDB
-- PHP Version: 8.1.6

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `pakdosen_pbl`
--

-- --------------------------------------------------------

--
-- Table structure for table `dosen`
--

CREATE TABLE `dosen` (
  `nik_dosen` char(20) NOT NULL,
  `nama` varchar(100) NOT NULL,
  `password` varchar(30) NOT NULL,
  `email` varchar(100) NOT NULL,
  `no_hp` varchar(15) NOT NULL,
  `role` enum('Dosen','Reviewer') NOT NULL DEFAULT 'Dosen',
  `tempat_lahir` varchar(100) NOT NULL,
  `tgl_lahir` date NOT NULL,
  `jenis_kelamin` enum('Laki-laki','Perempuan') NOT NULL,
  `jurusan` varchar(100) NOT NULL,
  `prodi` varchar(100) NOT NULL,
  `jenjang_pendidikan` varchar(100) NOT NULL,
  `jabatan_fungsional` varchar(100) NOT NULL,
  `bidang_ilmu` varchar(100) NOT NULL,
  `foto_profile` text NOT NULL,
  `nidn` char(20) NOT NULL,
  `nip` char(20) NOT NULL,
  `pangkat_golongan` varchar(100) NOT NULL,
  `masa_gol_lama` varchar(100) NOT NULL,
  `masa_gol_baru` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `dosen`
--

INSERT INTO `dosen` (`nik_dosen`, `nama`, `password`, `email`, `no_hp`, `role`, `tempat_lahir`, `tgl_lahir`, `jenis_kelamin`, `jurusan`, `prodi`, `jenjang_pendidikan`, `jabatan_fungsional`, `bidang_ilmu`, `foto_profile`, `nidn`, `nip`, `pangkat_golongan`, `masa_gol_lama`, `masa_gol_baru`) VALUES
('dosen1', 'Antono Priyantono', 'dosen01', 'antopriyantono@gmail.com', '+629832', 'Dosen', 'bdg', '2022-12-01', 'Laki-laki', 'Teknik Informatika', '', 'Antono Priyantono', 'antopriyantono@gmail.com', 'asd', 'soal uas.png', 'asd', 'asd', 'asd', 'asd', ''),
('dosen2', 'Bagas Bintaro', 'dosen02', 'bagasbintaro@gmail.com', '', 'Dosen', '', '0000-00-00', '', '', '', '', '', '', '', '', '', '', '', ''),
('dosen3', 'Handani Siri', 'dosen03', 'handanisiri@gmail.com', '', 'Dosen', '', '0000-00-00', '', '', '', '', '', '', '', '', '', '', '', ''),
('dosen4', 'Mayang Siregar', 'dosen04', 'mayangsiregar@gmail.com', '', 'Dosen', '', '0000-00-00', '', '', '', '', '', '', '', '', '', '', '', ''),
('dosen5', 'Parto artono', 'dosen05', 'parto@gmail.com', '0811', 'Dosen', '', '0000-00-00', 'Laki-laki', '', '', '', '', '', '', '', '', '', '', ''),
('dosen6', 'nama', 'password', 'email', '08', 'Dosen', '', '0000-00-00', 'Laki-laki', '', '', '', '', '', '', '', '', '', '', ''),
('reviewer1', 'Sri Ati Widyastutik', 'reviewer01', 'sriwidyastutik@gmail.com', '08111', 'Reviewer', '', '0000-00-00', '', '', '', '', '', '', '', '', '', '', '', ''),
('reviewer2', 'Puspaningsih Handani', 'reviewer02', 'puspaningsihhandani@gmail.com', '+62832284939275', 'Reviewer', '', '0000-00-00', '', '', '', '', '', '', '', '', '', '', '', ''),
('reviewer3', 'Maharani Asih', 'reviewer03', 'maharaniasih@gmail.com', '0811', 'Reviewer', '', '0000-00-00', '', '', '', '', '', '', '', '', '', '', '', ''),
('Reviewer4', 'Aminah', 'reviewer04', 'aminah@gmail.com', '08111', 'Reviewer', '', '0000-00-00', 'Laki-laki', '', '', '', '', '', '', '', '', '', '', ''),
('reviewer5', 'Maimunah Wati', 'reviewer05', 'maimunahwati@gmail.com', '081333', 'Reviewer', '', '0000-00-00', 'Laki-laki', '', '', '', '', '', '', '', '', '', '', '');

-- --------------------------------------------------------

--
-- Table structure for table `operator`
--

CREATE TABLE `operator` (
  `nik_operator` char(20) NOT NULL,
  `nama` varchar(100) NOT NULL,
  `password` varchar(30) NOT NULL,
  `email` varchar(100) NOT NULL,
  `no_hp` varchar(15) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `operator`
--

INSERT INTO `operator` (`nik_operator`, `nama`, `password`, `email`, `no_hp`) VALUES
('operator1', 'Andi Rajasarana Muda', 'operator01', 'andirajasamuda@gmail.com', '+682'),
('operator2', 'Fatma Wardani', 'operator02', 'fatmawardani@gmail.com', '08123');

-- --------------------------------------------------------

--
-- Table structure for table `pel_pendidikan`
--

CREATE TABLE `pel_pendidikan` (
  `id_pel_pendidikan` int(11) NOT NULL,
  `id_usulan` int(11) NOT NULL,
  `nik_dosen` char(20) NOT NULL,
  `jenis_pelaksanaan` text NOT NULL,
  `detail_pelaksanaan` text NOT NULL,
  `keterangan_pelaksanaan` text NOT NULL,
  `tgl_pelaksanaan` date NOT NULL,
  `semester_waktu` text NOT NULL,
  `satuan_hasil` text NOT NULL,
  `jumlah_volume_dosen` int(5) NOT NULL,
  `keterangan` text NOT NULL,
  `bukti_file` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `pel_pendidikan`
--

INSERT INTO `pel_pendidikan` (`id_pel_pendidikan`, `id_usulan`, `nik_dosen`, `jenis_pelaksanaan`, `detail_pelaksanaan`, `keterangan_pelaksanaan`, `tgl_pelaksanaan`, `semester_waktu`, `satuan_hasil`, `jumlah_volume_dosen`, `keterangan`, `bukti_file`) VALUES
(1, 1, 'dosen1', 'D - Membimbing dan ikut membimbing dalam menghasilkan disertasi, thesis, skripsi dan laporan akhir studi (maksimum 32 kum per semester)', 'D - Membimbing dan ikut membimbing dalam menghasilkan disertasi, thesis, skripsi dan laporan akhir studi (maksimum 32 kum per semester)', 'sa', '2022-12-09', 'Semester Ganjil 2021/2022', 'Mahasiswa', 150, 'Ijazah', ''),
(6, 21, 'dosen1', 'A- Melaksanakan perkulihan/ tutorial dan membimbing, menguji serta menyelenggarakan pendidikan di laboratorium, praktek keguruan bengkel/ studio/kebun percobaan/teknologi pengajaran dan praktek lapangan', 'A- Melaksanakan perkulihan/ tutorial dan membimbing, menguji serta menyelenggarakan pendidikan di laboratorium, praktek keguruan bengkel/ studio/kebun percobaan/teknologi pengajaran dan praktek lapangan', 'keterangan_pelaksanaan', '2022-12-03', 'Semester Genap 2016/2017', 'Kegiatan', 5, 'keterangan', 'soal uas.png'),
(12, 54, 'dosen1', 'A- Melaksanakan perkulihan/ tutorial dan membimbing, menguji serta menyelenggarakan pendidikan di laboratorium, praktek keguruan bengkel/ studio/kebun percobaan/teknologi pengajaran dan praktek lapangan', 'C - Membing kuliah kerja nyata, pratek kerja nyata, praktek kerja lapangan (setiap semester nilai 1 kum)', 'keterangan_pelaksanaan', '2022-12-03', 'Semester Ganjil 2016/2017', 'Mahasiswa', 5, 'keterangan', '57.png');

-- --------------------------------------------------------

--
-- Table structure for table `pel_pendidikan_reviewer`
--

CREATE TABLE `pel_pendidikan_reviewer` (
  `id_pel_pendidikan` int(11) NOT NULL DEFAULT 0,
  `nik_reviewer` char(20) NOT NULL,
  `jumlah_volume_reviewer` int(5) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `pel_pendidikan_reviewer`
--

INSERT INTO `pel_pendidikan_reviewer` (`id_pel_pendidikan`, `nik_reviewer`, `jumlah_volume_reviewer`) VALUES
(1, 'reviewer2', 0);

-- --------------------------------------------------------

--
-- Table structure for table `pendidikan`
--

CREATE TABLE `pendidikan` (
  `id_pendidikan` int(11) NOT NULL,
  `id_usulan` int(11) NOT NULL,
  `nik_dosen` char(20) NOT NULL,
  `jenis_pendidikan` text NOT NULL,
  `detail_pendidikan` text NOT NULL,
  `tgl_pendidikan` date NOT NULL,
  `satuan_hasil` text NOT NULL,
  `jumlah_volume_dosen` int(5) NOT NULL,
  `keterangan` text NOT NULL,
  `bukti_file` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `pendidikan`
--

INSERT INTO `pendidikan` (`id_pendidikan`, `id_usulan`, `nik_dosen`, `jenis_pendidikan`, `detail_pendidikan`, `tgl_pendidikan`, `satuan_hasil`, `jumlah_volume_dosen`, `keterangan`, `bukti_file`) VALUES
(1, 1, 'dosen1', 'pendidikan formal', 'ini kolom detail pendidikan formal Asisten Ahli', '0000-00-00', '', 150, 'Ijazah', ''),
(7, 1, 'dosen1', 'B - Pendidikan dan Pelatihan Prajabatan', '1. Doktor (S3)', '2022-12-01', '-', 150, 'Ijazah', 'soal uas.png'),
(8, 1, 'dosen1', 'hallow', 'detail_pendidikan', '2022-12-02', 'satuan_hasil', 1, 'keterangan', 'soal uas.png'),
(19, 1, 'dosen1', 'A - Pendidikan Formal', '2. Magister (S2)', '2022-12-01', '-', 150, 'Ijazah', 'coba.png'),
(23, 1, 'dosen1', 'A - Pendidikan Formal', '2. Magister (S2)', '2022-12-01', '-', 200, 'Ijazah', ''),
(32, 1, 'dosen1', 'B - Pendidikan dan Pelatihan Prajabatan', '1. Doktor (S3)', '2022-12-03', '-', 150, 'Ijazah', ''),
(34, 1, 'dosen1', 'A - Pendidikan Formal', '1. Doktor (S3)', '2022-12-02', '-', 150, 'Ijazah', ''),
(35, 21, 'dosen1', 'A - Pendidikan Formal', '1. Doktor (S3)', '2022-12-02', '-', 200, 'Ijazah', 'soal uas.png'),
(47, 54, 'dosen1', 'A - Pendidikan Formal', '1. Doktor (S3)', '2022-12-02', '-', 150, 'Ijazah', ''),
(48, 55, 'dosen1', 'A - Pendidikan Formal', '1. Doktor (S3)', '2022-12-02', '-', 150, 'Ijazah', ''),
(49, 56, 'dosen1', 'A - Pendidikan Formal', '1. Doktor (S3)', '2022-12-03', '-', 200, 'Ijazah', '52.png'),
(50, 57, 'dosen1', 'A - Pendidikan Formal', '1. Doktor (S3)', '2022-12-03', '-', 150, 'Ijazah', '');

-- --------------------------------------------------------

--
-- Table structure for table `pendidikan_reviewer`
--

CREATE TABLE `pendidikan_reviewer` (
  `id_pendidikan` int(11) NOT NULL,
  `nik_reviewer` char(20) NOT NULL,
  `jumlah_volume_reviewer` int(5) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `pendidikan_reviewer`
--

INSERT INTO `pendidikan_reviewer` (`id_pendidikan`, `nik_reviewer`, `jumlah_volume_reviewer`) VALUES
(1, 'reviewer1', 200),
(7, 'reviewer1', 3),
(8, 'reviewer1', 200),
(19, 'reviewer1', 3),
(1, 'reviewer1', 200),
(23, 'reviewer1', 200),
(32, 'reviewer1', 150),
(34, 'reviewer1', 200),
(48, 'reviewer1', 150);

-- --------------------------------------------------------

--
-- Table structure for table `penelitian`
--

CREATE TABLE `penelitian` (
  `id_penelitian` int(11) NOT NULL,
  `id_usulan` int(11) NOT NULL,
  `nik_dosen` char(20) NOT NULL,
  `jenis_pelaksanaan` text NOT NULL,
  `jenis_hasil_pelaksanaan` text NOT NULL,
  `detail_pelaksanaan` text NOT NULL,
  `keterangan_pelaksanaan` text NOT NULL,
  `tgl_penelitian` date NOT NULL,
  `satuan_hasil` text NOT NULL,
  `jumlah_volume_dosen` int(5) NOT NULL,
  `keterangan` text NOT NULL,
  `bukti_file` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `penelitian`
--

INSERT INTO `penelitian` (`id_penelitian`, `id_usulan`, `nik_dosen`, `jenis_pelaksanaan`, `jenis_hasil_pelaksanaan`, `detail_pelaksanaan`, `keterangan_pelaksanaan`, `tgl_penelitian`, `satuan_hasil`, `jumlah_volume_dosen`, `keterangan`, `bukti_file`) VALUES
(1, 1, 'dosen1', 'ini adalah jenis pelaksanaan penelitian Asisten Ahli', 'ini adalah jenis hasil pelaksanaan penelitian Asisten Ahli', 'ini adalah detail pelaksanaan penelitian Asisten Ahli', '', '2022-12-03', '2', 3, 'Ijazah', ''),
(6, 21, 'dosen1', 'jenis_pelaksanaan', 'jenis_hasil_pelaksanaan', 'detail_pelaksanaan', '', '2022-12-03', 'satuan_hasil', 3, 'keterangan', 'soal uas.png');

-- --------------------------------------------------------

--
-- Table structure for table `penelitian_reviewer`
--

CREATE TABLE `penelitian_reviewer` (
  `id_penelitian` int(11) NOT NULL,
  `nik_reviewer` char(20) NOT NULL,
  `jumlah_volume_reviewer` int(5) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `penelitian_reviewer`
--

INSERT INTO `penelitian_reviewer` (`id_penelitian`, `nik_reviewer`, `jumlah_volume_reviewer`) VALUES
(1, 'reviewer1', 0);

-- --------------------------------------------------------

--
-- Table structure for table `pengabdian`
--

CREATE TABLE `pengabdian` (
  `id_pengabdian` int(11) NOT NULL,
  `id_usulan` int(11) NOT NULL,
  `nik_dosen` char(20) NOT NULL,
  `jenis_pelaksanaan` text NOT NULL,
  `detail_pelaksanaan` text NOT NULL,
  `keterangan_pelaksanaan` text NOT NULL,
  `tgl_pengabdian` date NOT NULL,
  `satuan_hasil` text NOT NULL,
  `jumlah_volume_dosen` int(5) NOT NULL,
  `keterangan` text NOT NULL,
  `bukti_file` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `pengabdian`
--

INSERT INTO `pengabdian` (`id_pengabdian`, `id_usulan`, `nik_dosen`, `jenis_pelaksanaan`, `detail_pelaksanaan`, `keterangan_pelaksanaan`, `tgl_pengabdian`, `satuan_hasil`, `jumlah_volume_dosen`, `keterangan`, `bukti_file`) VALUES
(1, 1, 'dosen1', 'F - Hasil kegiatan pengabdian kepada masyarakat yang dipublikasikan di sebuah berkala/ jurnal pengabdian kepada masyarakat atau teknologi tepat  merupakan diseminasi dari luaran program kegiatan pengabdian kepada masyarakat tiap karya ', 'Menduduki jabatan pimpinan pada lembaga pemerintahan/ pejabat negara yang harus dibebaskan dari jabatan organiknya', 'a) Tingkat internasional', '0000-00-00', 'Per program/ semester', 200, 'Ijazah', ''),
(3, 1, 'dosen1', 'G - Berperan aktif dalam pengelolaan jurnal ilmiah (per tahun sebagai editor /dewan penyunting/dewan redaksi jurnal ilmiah)', '2. Jurnal ilmiah nasional', 'd) Insidental', '0000-00-00', 'Tiap tahun', 150, 'Ijazah', ''),
(6, 21, 'dosen1', 'G - Berperan aktif dalam pengelolaan jurnal ilmiah (per tahun sebagai editor /dewan penyunting/dewan redaksi jurnal ilmiah)', '2. Jurnal ilmiah nasional', 'd) Insidental', '2022-12-31', 'Tiap tahun', 10, 'keterangan', 'soal uas.png');

-- --------------------------------------------------------

--
-- Table structure for table `pengabdian_reviewer`
--

CREATE TABLE `pengabdian_reviewer` (
  `id_pengabdian` int(11) NOT NULL,
  `nik_reviewer` char(20) NOT NULL,
  `jumlah_volume_reviewer` int(5) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `pengabdian_reviewer`
--

INSERT INTO `pengabdian_reviewer` (`id_pengabdian`, `nik_reviewer`, `jumlah_volume_reviewer`) VALUES
(1, 'reviewer1', 0);

-- --------------------------------------------------------

--
-- Table structure for table `penunjang`
--

CREATE TABLE `penunjang` (
  `id_penunjang` int(11) NOT NULL,
  `id_usulan` int(11) NOT NULL,
  `nik_dosen` char(20) NOT NULL,
  `jenis_pelaksanaan` text NOT NULL,
  `detail_pelaksanaan` text NOT NULL,
  `keterangan_pelaksanaan` text NOT NULL,
  `tgl_penunjang` date NOT NULL,
  `satuan_hasil` text NOT NULL,
  `jumlah_volume_dosen` int(5) NOT NULL,
  `keterangan` text NOT NULL,
  `bukti_file` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `penunjang`
--

INSERT INTO `penunjang` (`id_penunjang`, `id_usulan`, `nik_dosen`, `jenis_pelaksanaan`, `detail_pelaksanaan`, `keterangan_pelaksanaan`, `tgl_penunjang`, `satuan_hasil`, `jumlah_volume_dosen`, `keterangan`, `bukti_file`) VALUES
(1, 1, 'dosen1', 'A - Menjadi anggota dalam suatu Panitia/Badan pada perguruan tinggi', '1. Sebagai ketua/wakil ketua merangkap anggota tiap tahun', 'a) Ketua/ Wakil Ketua', '2022-12-17', 'Per panitia', 5, 'Surat Penugasan; Surat undangan', 'coba.png'),
(6, 1, 'dosen1', 'F - Berperan serta aktif dalam pertemuan ilmiah', '1. Panitian pusat', 'b) Anggota, tiap kegiatan', '2022-12-15', 'Per periode', 10, 'SK Direktur,,,,', 'soal uas.png'),
(8, 21, 'dosen1', 'J - Keanggotaan dalam tim penilaian ', '2. Sebagai anggota delegasi', 'c) 10 (sepuluh) tahun', '2022-12-31', 'Kegiatan', 600, 'Sertifikat Asli', 'coba.png');

-- --------------------------------------------------------

--
-- Table structure for table `penunjang_reviewer`
--

CREATE TABLE `penunjang_reviewer` (
  `id_penunjang` int(11) NOT NULL,
  `nik_reviewer` char(20) NOT NULL,
  `jumlah_volume_reviewer` int(5) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `penunjang_reviewer`
--

INSERT INTO `penunjang_reviewer` (`id_penunjang`, `nik_reviewer`, `jumlah_volume_reviewer`) VALUES
(1, 'reviewer1', 0);

-- --------------------------------------------------------

--
-- Table structure for table `usulan`
--

CREATE TABLE `usulan` (
  `id_usulan` int(11) NOT NULL,
  `nik_dosen` char(20) NOT NULL,
  `jenis_usulan` enum('Asisten Ahli','Lektor') NOT NULL DEFAULT 'Asisten Ahli',
  `tgl_usulan` datetime DEFAULT current_timestamp(),
  `status_usulan` varchar(100) NOT NULL,
  `tgl_validasi` datetime DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `usulan`
--

INSERT INTO `usulan` (`id_usulan`, `nik_dosen`, `jenis_usulan`, `tgl_usulan`, `status_usulan`, `tgl_validasi`) VALUES
(1, 'dosen1', 'Asisten Ahli', '2022-12-30 06:22:26', 'Sudah dinilai', '2022-12-30 09:59:40'),
(9, 'dosen4', 'Lektor', '0000-00-00 00:00:00', 'Sudah dinilai', '2022-12-29 17:03:05'),
(10, 'dosen3', 'Lektor', '0000-00-00 00:00:00', 'Mohon tunggu sebentar', '2022-12-24 02:14:04'),
(11, 'dosen3', 'Asisten Ahli', '2022-11-28 18:59:01', 'Sudah dinilai', '2022-12-24 02:14:04'),
(12, 'dosen3', 'Lektor', '0000-00-00 00:00:00', 'coba2', '2022-12-24 02:14:04'),
(16, 'dosen3', 'Lektor', '0000-00-00 00:00:00', 'Tervalidasi', '2022-12-30 05:42:56'),
(17, 'dosen3', 'Asisten Ahli', '2022-11-28 19:29:48', 'Tervalidasi', '2022-12-30 01:24:47'),
(20, 'dosen3', 'Asisten Ahli', '2022-11-28 22:51:12', 'Tervalidasi', '2022-12-23 16:55:30'),
(21, 'dosen1', 'Lektor', '2022-12-23 16:47:27', 'Sudah dinilai', '2022-12-30 05:38:07'),
(22, 'dosen1', 'Lektor', '2022-12-30 01:18:07', 'Diajukan', '2022-12-29 16:44:33'),
(23, 'dosen2', '', NULL, 'Diajukan ke Senat', '2022-12-24 02:14:04'),
(25, 'dosen2', '', '2022-12-13 04:54:07', 'Diterima', '2022-12-24 02:14:04'),
(26, 'dosen3', 'Lektor', '2022-12-13 05:22:59', 'Tervalidasi', '2022-12-24 15:39:40'),
(27, 'dosen5', 'Asisten Ahli', '2022-12-14 05:43:23', '', '2022-12-24 02:14:04'),
(28, 'dosen1', 'Asisten Ahli', '2022-12-14 05:48:04', '', '2022-12-24 02:14:04'),
(43, 'dosen2', 'Lektor', NULL, 'Tervalidasi', '2022-12-29 12:42:10'),
(46, 'reviewer1', 'Asisten Ahli', '2022-12-30 01:34:09', 'Sudah dinilai', '2022-12-30 06:42:11'),
(54, 'dosen1', 'Asisten Ahli', '2022-12-30 09:33:54', 'Sudah dinilai', '2022-12-30 09:36:15'),
(55, 'dosen1', 'Asisten Ahli', '2022-12-30 09:56:29', 'Sudah dinilai', '2022-12-30 09:58:35'),
(56, 'dosen1', 'Asisten Ahli', '2022-12-30 10:19:04', 'Sudah dinilai', '2022-12-30 10:20:35'),
(57, 'dosen1', 'Asisten Ahli', '2022-12-30 10:30:43', 'Sudah dinilai', '2022-12-30 10:32:14');

-- --------------------------------------------------------

--
-- Table structure for table `usulan_reviewer`
--

CREATE TABLE `usulan_reviewer` (
  `id_usulan_reviewer` int(11) NOT NULL,
  `id_usulan` int(11) NOT NULL,
  `nik_reviewer_1` char(20) NOT NULL,
  `nik_reviewer_2` char(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `usulan_reviewer`
--

INSERT INTO `usulan_reviewer` (`id_usulan_reviewer`, `id_usulan`, `nik_reviewer_1`, `nik_reviewer_2`) VALUES
(35, 1, 'reviewer1', 'reviewer3'),
(28, 1, 'reviewer1', 'reviewer5'),
(1, 1, 'reviewer3', 'reviewer5'),
(11, 1, 'reviewer3', 'reviewer5'),
(19, 9, 'reviewer1', 'reviewer3'),
(20, 9, 'reviewer1', 'reviewer3'),
(21, 9, 'reviewer2', 'reviewer3'),
(22, 9, 'reviewer2', 'reviewer3'),
(23, 9, 'reviewer2', 'reviewer3'),
(12, 11, 'Reviewer4', 'reviewer1'),
(27, 16, 'reviewer5', 'reviewer3'),
(25, 21, 'reviewer1', 'reviewer3'),
(26, 21, 'reviewer1', 'reviewer3'),
(10, 21, 'reviewer3', 'reviewer2'),
(18, 22, 'reviewer2', 'reviewer3'),
(29, 46, 'reviewer1', 'reviewer2'),
(33, 54, 'reviewer1', 'Reviewer4'),
(34, 55, 'reviewer1', 'reviewer2'),
(36, 56, 'reviewer1', 'reviewer3'),
(37, 57, 'reviewer1', 'Reviewer4');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `dosen`
--
ALTER TABLE `dosen`
  ADD PRIMARY KEY (`nik_dosen`);

--
-- Indexes for table `operator`
--
ALTER TABLE `operator`
  ADD PRIMARY KEY (`nik_operator`);

--
-- Indexes for table `pel_pendidikan`
--
ALTER TABLE `pel_pendidikan`
  ADD PRIMARY KEY (`id_pel_pendidikan`),
  ADD KEY `id_usulan` (`id_usulan`,`nik_dosen`),
  ADD KEY `pel_pendidikan_ibfk_2` (`nik_dosen`);

--
-- Indexes for table `pel_pendidikan_reviewer`
--
ALTER TABLE `pel_pendidikan_reviewer`
  ADD KEY `id_pel_pendidikan` (`id_pel_pendidikan`,`nik_reviewer`),
  ADD KEY `pel_pendidikan_reviewer_ibfk_2` (`nik_reviewer`);

--
-- Indexes for table `pendidikan`
--
ALTER TABLE `pendidikan`
  ADD PRIMARY KEY (`id_pendidikan`),
  ADD KEY `id_usulan` (`id_usulan`,`nik_dosen`),
  ADD KEY `pendidikan_ibfk_1` (`nik_dosen`);

--
-- Indexes for table `pendidikan_reviewer`
--
ALTER TABLE `pendidikan_reviewer`
  ADD KEY `id_pendidikan` (`id_pendidikan`,`nik_reviewer`),
  ADD KEY `pendidikan_reviewer_ibfk_1` (`nik_reviewer`);

--
-- Indexes for table `penelitian`
--
ALTER TABLE `penelitian`
  ADD PRIMARY KEY (`id_penelitian`),
  ADD KEY `id_usulan` (`id_usulan`,`nik_dosen`),
  ADD KEY `penelitian_ibfk_2` (`nik_dosen`);

--
-- Indexes for table `penelitian_reviewer`
--
ALTER TABLE `penelitian_reviewer`
  ADD KEY `id_penelitian` (`id_penelitian`,`nik_reviewer`),
  ADD KEY `penelitian_reviewer_ibfk_2` (`nik_reviewer`);

--
-- Indexes for table `pengabdian`
--
ALTER TABLE `pengabdian`
  ADD PRIMARY KEY (`id_pengabdian`),
  ADD KEY `id_usulan` (`id_usulan`,`nik_dosen`),
  ADD KEY `pengabdian_ibfk_2` (`nik_dosen`);

--
-- Indexes for table `pengabdian_reviewer`
--
ALTER TABLE `pengabdian_reviewer`
  ADD KEY `id_pengabdian` (`id_pengabdian`,`nik_reviewer`),
  ADD KEY `pengabdian_reviewer_ibfk_2` (`nik_reviewer`);

--
-- Indexes for table `penunjang`
--
ALTER TABLE `penunjang`
  ADD PRIMARY KEY (`id_penunjang`),
  ADD KEY `id_usulan` (`id_usulan`,`nik_dosen`),
  ADD KEY `penunjang_ibfk_2` (`nik_dosen`);

--
-- Indexes for table `penunjang_reviewer`
--
ALTER TABLE `penunjang_reviewer`
  ADD KEY `id_penunjang` (`id_penunjang`,`nik_reviewer`),
  ADD KEY `penunjang_reviewer_ibfk_2` (`nik_reviewer`);

--
-- Indexes for table `usulan`
--
ALTER TABLE `usulan`
  ADD PRIMARY KEY (`id_usulan`),
  ADD KEY `nik_dosen` (`nik_dosen`);

--
-- Indexes for table `usulan_reviewer`
--
ALTER TABLE `usulan_reviewer`
  ADD PRIMARY KEY (`id_usulan_reviewer`),
  ADD KEY `id_usulan` (`id_usulan`,`nik_reviewer_1`,`nik_reviewer_2`),
  ADD KEY `nik_reviewer_1` (`nik_reviewer_1`),
  ADD KEY `nik_reviewer_2` (`nik_reviewer_2`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `pel_pendidikan`
--
ALTER TABLE `pel_pendidikan`
  MODIFY `id_pel_pendidikan` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT for table `pendidikan`
--
ALTER TABLE `pendidikan`
  MODIFY `id_pendidikan` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=51;

--
-- AUTO_INCREMENT for table `penelitian`
--
ALTER TABLE `penelitian`
  MODIFY `id_penelitian` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT for table `pengabdian`
--
ALTER TABLE `pengabdian`
  MODIFY `id_pengabdian` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT for table `penunjang`
--
ALTER TABLE `penunjang`
  MODIFY `id_penunjang` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT for table `usulan`
--
ALTER TABLE `usulan`
  MODIFY `id_usulan` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=58;

--
-- AUTO_INCREMENT for table `usulan_reviewer`
--
ALTER TABLE `usulan_reviewer`
  MODIFY `id_usulan_reviewer` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=38;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `pel_pendidikan`
--
ALTER TABLE `pel_pendidikan`
  ADD CONSTRAINT `pel_pendidikan_ibfk_1` FOREIGN KEY (`id_usulan`) REFERENCES `usulan` (`id_usulan`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `pel_pendidikan_ibfk_2` FOREIGN KEY (`nik_dosen`) REFERENCES `dosen` (`nik_dosen`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `pel_pendidikan_reviewer`
--
ALTER TABLE `pel_pendidikan_reviewer`
  ADD CONSTRAINT `pel_pendidikan_reviewer_ibfk_1` FOREIGN KEY (`id_pel_pendidikan`) REFERENCES `pel_pendidikan` (`id_pel_pendidikan`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `pel_pendidikan_reviewer_ibfk_2` FOREIGN KEY (`nik_reviewer`) REFERENCES `dosen` (`nik_dosen`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `pendidikan`
--
ALTER TABLE `pendidikan`
  ADD CONSTRAINT `pendidikan_ibfk_1` FOREIGN KEY (`nik_dosen`) REFERENCES `dosen` (`nik_dosen`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `pendidikan_ibfk_2` FOREIGN KEY (`id_usulan`) REFERENCES `usulan` (`id_usulan`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `pendidikan_reviewer`
--
ALTER TABLE `pendidikan_reviewer`
  ADD CONSTRAINT `pendidikan_reviewer_ibfk_1` FOREIGN KEY (`nik_reviewer`) REFERENCES `dosen` (`nik_dosen`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `pendidikan_reviewer_ibfk_2` FOREIGN KEY (`id_pendidikan`) REFERENCES `pendidikan` (`id_pendidikan`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `penelitian`
--
ALTER TABLE `penelitian`
  ADD CONSTRAINT `penelitian_ibfk_1` FOREIGN KEY (`id_usulan`) REFERENCES `usulan` (`id_usulan`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `penelitian_ibfk_2` FOREIGN KEY (`nik_dosen`) REFERENCES `dosen` (`nik_dosen`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `penelitian_reviewer`
--
ALTER TABLE `penelitian_reviewer`
  ADD CONSTRAINT `penelitian_reviewer_ibfk_1` FOREIGN KEY (`id_penelitian`) REFERENCES `penelitian` (`id_penelitian`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `penelitian_reviewer_ibfk_2` FOREIGN KEY (`nik_reviewer`) REFERENCES `dosen` (`nik_dosen`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `pengabdian`
--
ALTER TABLE `pengabdian`
  ADD CONSTRAINT `pengabdian_ibfk_1` FOREIGN KEY (`id_usulan`) REFERENCES `usulan` (`id_usulan`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `pengabdian_ibfk_2` FOREIGN KEY (`nik_dosen`) REFERENCES `dosen` (`nik_dosen`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `pengabdian_reviewer`
--
ALTER TABLE `pengabdian_reviewer`
  ADD CONSTRAINT `pengabdian_reviewer_ibfk_1` FOREIGN KEY (`id_pengabdian`) REFERENCES `pengabdian` (`id_pengabdian`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `pengabdian_reviewer_ibfk_2` FOREIGN KEY (`nik_reviewer`) REFERENCES `dosen` (`nik_dosen`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `penunjang`
--
ALTER TABLE `penunjang`
  ADD CONSTRAINT `penunjang_ibfk_1` FOREIGN KEY (`id_usulan`) REFERENCES `usulan` (`id_usulan`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `penunjang_ibfk_2` FOREIGN KEY (`nik_dosen`) REFERENCES `dosen` (`nik_dosen`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `penunjang_reviewer`
--
ALTER TABLE `penunjang_reviewer`
  ADD CONSTRAINT `penunjang_reviewer_ibfk_1` FOREIGN KEY (`id_penunjang`) REFERENCES `penunjang` (`id_penunjang`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `penunjang_reviewer_ibfk_2` FOREIGN KEY (`nik_reviewer`) REFERENCES `dosen` (`nik_dosen`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `usulan`
--
ALTER TABLE `usulan`
  ADD CONSTRAINT `usulan_ibfk_1` FOREIGN KEY (`nik_dosen`) REFERENCES `dosen` (`nik_dosen`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `usulan_reviewer`
--
ALTER TABLE `usulan_reviewer`
  ADD CONSTRAINT `usulan_reviewer_ibfk_1` FOREIGN KEY (`nik_reviewer_1`) REFERENCES `dosen` (`nik_dosen`) ON UPDATE CASCADE,
  ADD CONSTRAINT `usulan_reviewer_ibfk_2` FOREIGN KEY (`nik_reviewer_2`) REFERENCES `dosen` (`nik_dosen`) ON UPDATE CASCADE,
  ADD CONSTRAINT `usulan_reviewer_ibfk_3` FOREIGN KEY (`id_usulan`) REFERENCES `usulan` (`id_usulan`) ON DELETE CASCADE ON UPDATE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
