<!doctype html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title><?= $title; ?></title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-Zenh87qX5JnK2Jl0vWa8Ck2rdkQ2Bzep5IDxbcnCeuOxjzrPF/et3URy9Bv1WTRi" crossorigin="anonymous">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap-icons@1.10.2/font/bootstrap-icons.css">
</head>

<body>
    <!-- header -->
    <div class="container-fluid bg-dark text-light py-5 ">
        <div class="container">
            <div class="row align-items-center">
                <div class="col-2">
                    <img src="img/logo.png" class="img-fluid" alt="">
                </div>
                <div class="col-7">
                    <h4>Politeknik Negeri Batam</h4>
                    <h1>Sistem Penilaian Angka Kredit Dosen</h1>
                </div>
                <div class="col-1 offset-2">
                    <img src="img/logopolibatam.png" class="img-fluid" alt="">
                </div>
            </div>


        </div>
    </div>
    <!-- end header -->

    <!-- navbar -->
    <?= $this->include('layout/navbar'); ?>
    <!-- end navbar -->

    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.2.2/dist/js/bootstrap.bundle.min.js" integrity="sha384-OERcA2EqjJCMA+/3y+gxIOqMEjwtxJY7qPCqsdltbNJuaOe923+mo//f6V8Qbsw3" crossorigin="anonymous"></script>

    <!-- content -->
    <?= $this->renderSection('content'); ?>
    <!-- end content -->

    <!-- footer -->
    <div class="container-fluid bg-dark text-light py-5 ">
        <div class="container">
            <div class="row">
                <div class="col-4 text-left">
                    <h4>QUICK LINKS</h4>
                    <nav class="h-100 flex-column align-items-stretch pe-4 border-end">
                        <nav class="nav nav-pills flex-column">
                            <a class="nav-link" href="#item-1">> SELANCAR PAK</a>
                            <a class="nav-link" ms-3 my-1 href="#item-2">> KEMENDIKBUDRISTEK</a>
                            <nav class="nav nav-pills flex-column">
                                <a class="nav-link ms-3 my-1" href="#item-2-1">> DITJEN DIKTI RISTEK</a>
                                <a class="nav-link ms-3 my-1" href="#item-2-2">> DITJEN VOKASI</a>
                                <nav class="nav nav-pills flex-column">
                                    <a class="nav-link ms-3 my-1" href="#item-2-2-1">> PDDIKTI</a>
                                    <nav class="nav nav-pills flex-column">
                                        <a class="nav-link ms-3 my-1" href="#item-2-2-1-1">> SISTER</a>
                                    </nav>
                                </nav>
                            </nav>
                        </nav>
                    </nav>
                </div>
                <div class="col-5 text-left">
                    <h4>CONTACT US</h4>
                    <div class="ms-3 my-1 h-100 flex-column align-items-stretch pe-4 border-end">
                        <h6 class="py-2"><i class="bi bi-telephone"></i> Call Center : </h6>

                        <h6 class="py-2"><i class="bi bi-envelope-at"></i> pakpolibatam@gmail.com</h6>
                        <h6 class="py-2"><i class="bi bi-geo-alt"></i> Polibatam, Gedung blabla Lantai blabla Batam</h6>
                    </div>
                </div>
                <div class="col-3 text-left">
                    <h4>SOCIAL MEDIA</h4>
                    <div class="ms-3 my-1">
                        <h6 class="py-2"><i class="bi bi-google"></i> Website </h6>
                        <h6 class="py-2"><i class="bi bi-instagram"></i> Instagram </h6>
                        <h6 class="py-2"><i class="bi bi-facebook"></i> Facebook</h6>
                        <h6 class="py-2"><i class="bi bi-twitter"></i> Twitter</h6>
                        <h6 class="py-2"><i class="bi bi-youtube"></i> Youtube</h6>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- end footer -->

    <!-- copyright -->
    <div class="container-fluid bg-dark text-light py-5 ">
        <div class="container">
            <div class="row">
                <div class="col-12 text-center">
                    <h5>2022 (c) Hak Cipta Direktorat Politeknik Negeri Batam</h5>
                </div>
            </div>
        </div>
    </div>
    <!-- end copyright-->
</body>

</html>